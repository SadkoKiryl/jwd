package by.treining.ksa.oop.association.task5.entity;

public enum AmountOfDays {

    SEVEN("7", 7),
    TEN("10", 10),
    FIFTEEN("15", 15);

    private String amountOfDays;
    private int value;

    AmountOfDays(String amountOfDays, int value) {
        this.amountOfDays = amountOfDays;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getAmountOfDays() {
        return amountOfDays;
    }

    public void setAmountOfDays(String amountOfDays) {
        this.amountOfDays = amountOfDays;
    }
}
