package by.treining.ksa.oop.association.task3.model;

import java.util.ArrayList;
import java.util.List;

public class State {
    private final String stateName;
    private List<Region> regionList;
    private int regionCount;
    private int stateArea;
    private City capital;
    private List<City> regionCenterList = new ArrayList<>();

    public State(String stateName, List<Region> regionList) {
        this.stateName = stateName;
        this.regionList = regionList;
        this.regionCount = regionList.size();

        for (Region region : regionList) {
            this.stateArea += region.getRegionArea();
        }

        for (Region region : regionList) {
            for (City city : region.getCityList()) {
                if (city.isCapital()) {
                    this.capital = city;
                    break;
                }
            }
        }

        for (Region region : regionList) {
            this.regionCenterList.add(region.getRegionCenter());
        }
    }

    public String getStateName() {
        return stateName;
    }


    public List<Region> getRegionList() {
        return regionList;
    }

    public List<City> getRegionCenterList() {
        return regionCenterList;
    }

    public int getStateArea() {
        return stateArea;
    }

    public int getRegionCount() {
        return regionCount;
    }

    public City getCapital() {
        return capital;
    }
}
