package by.treining.ksa.decomposition;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/*
12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать метод(методы) вычисления его площади,
        если угол между сторонами длиной X и Y— прямой.
*/
public class Task_12 {

    public static void main(String[] args) {
        double x = 4;
        double y = 5;
        double z = 3;
        double t = 5;

        double area = calculateArea(x, y, z, t);
        System.out.println("\nThe area of quadrangle is " + area);
    }

    private static double calculateArea(double x, double y, double z, double t) {
        double hypotenuse = sqrt(pow(x, 2) + pow(y, 2));

        return calculateTriangleArea(x, y, hypotenuse) + calculateTriangleArea(hypotenuse, z, t);
    }

    private static double calculateTriangleArea(double sideAB, double sideBC, double sideCA) {
        double s = (sideAB + sideBC + sideCA) / 2;
        return sqrt(s * (s - sideAB) * (s - sideBC) * (s - sideCA));
    }
}
