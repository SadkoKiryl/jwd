package validator;

import by.training.sadko.controller.command.validator.LiteratureOptionsValidator;
import by.training.sadko.controller.command.validator.Validator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LiteratureOptionsValidatorTest {
    Validator<String[]> stringValidator;
    private String optionsLine;
    private BufferedReader reader;

    @Before
    public void setUp() throws IOException {
        stringValidator = new LiteratureOptionsValidator();
        Path path = Paths.get("src/test/resources/list.txt");
        reader = Files.newBufferedReader(path);
    }

    @Test
    public void validStringShouldHaveFiveOptions() throws IOException {

        while ((optionsLine = reader.readLine()) != null) {
            String[] literatureOptions = optionsLine.split(",");
            if (stringValidator.isValid(literatureOptions)) {
                Assert.assertEquals(5, literatureOptions.length);
            }
        }
    }

    @Test
    public void thirdOptionShouldBeNumeric() throws IOException {
        while ((optionsLine = reader.readLine()) != null) {
            String[] literatureOptions = optionsLine.split(",");
            if (stringValidator.isValid(literatureOptions)) {
                try {
                    Integer.parseInt(literatureOptions[2]);
                } catch (NumberFormatException e) {
                    Assert.fail("Should not thrown any exceptions");
                }
            }
        }
    }
}
