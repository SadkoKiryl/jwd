package by.treining.ksa.oop.association.task3.model;

import java.util.ArrayList;
import java.util.List;

public class City {
    private String cityName;
    private boolean isCapital;
    private boolean isRegionCenter;
    private List<District> districtList;
    private int cityArea;

    public City(String name, List<District> districtList, boolean isRegionCenter, boolean isCapital) {
        this.cityName = name;
        this.districtList = districtList;
        this.isRegionCenter = isRegionCenter;
        this.isCapital = isCapital;

        //initialisation area of the city
        for (District district : districtList) {
            cityArea += district.getDistrictArea();
        }
    }

    public City(String name, District district, boolean isRegionCenter, boolean isCapital) {
        this.cityName = name;
        this.districtList = new ArrayList<>();
        this.districtList.add(district);
        this.isRegionCenter = isRegionCenter;
        this.isCapital = isCapital;
        cityArea = district.getDistrictArea();
    }

    public int getCityArea() {
        return cityArea;
    }

    public boolean isRegionCenter() {
        return isRegionCenter;
    }

    public void setRegionCenter(boolean regionCenter) {
        isRegionCenter = regionCenter;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean capital) {
        isCapital = capital;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;

        cityArea = 0;
        for (District district : districtList) {
            cityArea += district.getDistrictArea();
        }
    }
}
