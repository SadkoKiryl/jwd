package by.training.sadko.entity;

public class Magazine extends Publication {

    private int subscribingIndex;
    private boolean isGlossy;

    public Magazine() {
        super.type = PublicationType.MAGAZINE.getValue();
    }

    public int getSubscriptionIndex() {
        return subscribingIndex;
    }

    public void setSubscribingIndex(int subscribingIndex) {
        this.subscribingIndex = subscribingIndex;
    }

    public boolean isGlossy() {
        return isGlossy;
    }

    public void setGlossy(boolean glossy) {
        isGlossy = glossy;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "id='" + id + '\'' +
                ", publicationType=" + type +
                ", contentType=" + contentType +
                ", title='" + title + '\'' +
                ", isMonthly=" + isMonthly +
                ", isColored=" + isColored +
                ", volume=" + volume +
                ", subscribingIndex=" + subscribingIndex +
                ", isGlossy=" + isGlossy +
                "}";
    }
}
