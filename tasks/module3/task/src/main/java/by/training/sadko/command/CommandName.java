package by.training.sadko.command;

public enum CommandName {

    SHOW_COMMAND("show"),
    UPLOAD_FILE_COMMAND("upload"),
    DOWNLOAD_TO_FILE_COMMAND("download"),
    NOT_FOUND_COMMAND("not_found");

    private final String stringName;

    CommandName(String stringName) {
        this.stringName = stringName;
    }

    public static CommandName fromString(String inputName) {
        CommandName[] commandsNameArray = CommandName.values();
        for (CommandName commandName : commandsNameArray) {
            if (commandName.name().equalsIgnoreCase(inputName)
                    || commandName.stringName.equalsIgnoreCase(inputName)) {
                return commandName;
            }
        }
        return NOT_FOUND_COMMAND;
    }

    public String getStringName() {
        return stringName;
    }
}
