package by.training.sadko.dao;

import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;

import java.util.List;

public interface LiteratureDao {

    List<Literature> getAllLiterature();

    void removeLiteratureById(int id);

    List<Literature> selectionLiteratureByGenre(Genre genre);

    void addLiterature(List<Literature> literature);

    void sort(LiteratureComparator... comparators);
}
