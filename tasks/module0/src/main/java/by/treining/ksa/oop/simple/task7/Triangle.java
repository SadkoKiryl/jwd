package by.treining.ksa.oop.simple.task7;

import static java.lang.Math.sqrt;

/*
7. Описать класс, представляющий треугольник. Предусмотреть методы для создания объектов, вычисления
площади, периметра и точки пересечения медиан.
*/
public class Triangle {
    private double sideAB;
    private double sideBC;
    private double sideCA;

    private Point pointA;
    private Point pointB;
    private Point pointC;

    public Triangle(Point pointA, Point pointB, Point pointC) {
        this.pointA = pointA;
        this.pointB = pointB;
        this.pointC = pointC;

        sideAB = Point.distanceCalculation(pointA, pointB);
        sideBC = Point.distanceCalculation(pointB, pointC);
        sideCA = Point.distanceCalculation(pointC, pointA);
    }

    public double areaCalculation() {
        double p = this.perimeterCalculation() / 2;
        double area = sqrt(p * (p - this.sideAB) * (p - this.sideBC) * (p - this.sideCA));
        return area;
    }

    public double perimeterCalculation() {
        double perimeter = this.sideAB + this.sideBC + this.sideCA;
        return perimeter;
    }

    public Point pointIntersectionMedians() {
        Point midPointAB = this.midCalculation();
        int x = (this.pointC.getX() + 2 * midPointAB.getX()) / 2;
        int y = (this.pointC.getY() + 2 * midPointAB.getY()) / 2;

        return new Point(x, y);
    }

    private Point midCalculation() {
        int middleX = (this.pointA.getX() + this.pointB.getX()) / 2;
        int middleY = (this.pointA.getY() + this.pointB.getY()) / 2;
        Point midPoint = new Point(middleX, middleY);
        return midPoint;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "sideAB=" + sideAB +
                ", sideBC=" + sideBC +
                ", sideCA=" + sideCA +
                ", pointA=" + pointA +
                ", pointB=" + pointB +
                ", pointC=" + pointC +
                ", area=" + this.areaCalculation() +
                ", perimeter=" + this.perimeterCalculation() +
                ", pointIntersectionMedians=" + this.pointIntersectionMedians() +
                '}';
    }

    public double getSideAB() {
        return sideAB;
    }

    public void setSideAB(double sideAB) {
        this.sideAB = sideAB;
    }

    public double getSideBC() {
        return sideBC;
    }

    public void setSideBC(double sideBC) {
        this.sideBC = sideBC;
    }

    public double getSideCA() {
        return sideCA;
    }

    public void setSideCA(double sideCA) {
        this.sideCA = sideCA;
    }

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }
}