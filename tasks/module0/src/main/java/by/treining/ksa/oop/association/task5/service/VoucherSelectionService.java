package by.treining.ksa.oop.association.task5.service;

import by.treining.ksa.oop.association.task5.entity.Voucher;

import java.util.List;

public class VoucherSelectionService implements VoucherSelection {

    @Override
    public Voucher getRequiredVoucher(int id, List<Voucher> voucherList) {

        for (Voucher voucher : voucherList) {
            if (voucher.getId() == id) {
                System.out.println("\nYour choice is : " + voucher.toString());
                return voucher;
            }
        }

        System.out.println("\nThe voucher with this id is not found");
        return null;
    }
}
