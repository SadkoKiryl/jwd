package by.training.sadko.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NotFoundCommand implements AppCommand {

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        return "not_found_view";
    }
}
