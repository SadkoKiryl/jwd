package by.treining.ksa.oop.association.task5.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum VoucherType {
    REST("rest", new BigDecimal(80).setScale(2, RoundingMode.HALF_UP)),
    EXCURSIONS("excursions", new BigDecimal(110).setScale(2, RoundingMode.HALF_UP)),
    TREATMENT("treatment", new BigDecimal(300).setScale(2, RoundingMode.HALF_UP)),
    SHOPPING("shopping", new BigDecimal(60).setScale(2, RoundingMode.HALF_UP)),
    CRUISE("cruise", new BigDecimal(190).setScale(2, RoundingMode.HALF_UP));

    private String type;
    private BigDecimal costPerDay;

    VoucherType(String type, BigDecimal costPerDay) {
        this.type = type;
        this.costPerDay = costPerDay;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(BigDecimal costPerDay) {
        this.costPerDay = costPerDay;
    }
}
