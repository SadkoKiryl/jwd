package by.training.sadko.parser.xml.sax;

import org.xml.sax.helpers.DefaultHandler;

import java.util.List;

public abstract class SAXHandler<T> extends DefaultHandler {

    protected List<T> entityFromXML;

    public abstract List<T> getListFromXML();
}
