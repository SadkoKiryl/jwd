<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container">
    <div class="content">
        <div class="alert alert-info" role="alert">
            <c:if test="${not empty download_result}">
                <p class="ui-state-error-text">
                    <c:out value="${download_result}"/>
                </p>
            </c:if>
        </div>
    </div>
</div>