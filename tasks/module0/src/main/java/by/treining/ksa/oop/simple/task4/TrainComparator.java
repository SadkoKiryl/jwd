package by.treining.ksa.oop.simple.task4;

import java.util.Arrays;
import java.util.Comparator;

public class TrainComparator {

    public static Train[] sortByTrainNumber(Train[] array) {
        Arrays.sort(array, Comparator.comparing(Train::getTrainNumber));
        return array;
    }

    public static Train[] sortByDestinationAndDeparture(Train[] array) {
        Arrays.sort(array, Comparator.comparing(Train::getDestination).thenComparing(Train::getDepartureTime));
        return array;
    }
}
