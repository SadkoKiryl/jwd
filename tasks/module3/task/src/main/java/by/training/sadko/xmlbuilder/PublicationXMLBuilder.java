package by.training.sadko.xmlbuilder;

import by.training.sadko.entity.Publication;

import java.util.List;
import java.util.Map;

public class PublicationXMLBuilder implements FileBuilder<Publication> {

    private final Map<String, EntityStringBuilder<Publication>> builderMap;

    public PublicationXMLBuilder(Map<String, EntityStringBuilder<Publication>> builderMap) {
        this.builderMap = builderMap;
    }

    @Override
    public String build(List<Publication> entitiesList) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
                .append("<Papers xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n")
                .append(" xmlns=\"http://ksa.com/schem\"\n")
                .append("xsi:schemaLocation=\"http://ksa.com/schem schema.xsd\">\n")
                .append("<Publications>\n");

        for (Publication publication : entitiesList) {
            String publicationType = publication.getType();
            EntityStringBuilder<Publication> stringBuilder = builderMap.get(publicationType);
            String element = stringBuilder.build(publication);
            stringBuffer.append(element);
        }

        stringBuffer.append("</Publications>\n")
                .append("</Papers>\n");

        return stringBuffer.toString();
    }
}
