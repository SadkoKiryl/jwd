package by.treining.ksa.oop.simple.task3;

/*
3. Создайте класс с именем Student, содержащий поля: фамилия и инициалы, номер группы, успеваемость
(массив из пяти элементов). Создайте массив из десяти элементов такого типа. Добавьте возможность вывода фамилий
и номеров групп студентов, имеющих оценки, равные только 9 или 10.
*/
public class Main {
    public static void main(String[] args) {
        Student[] students = new Student[10];

        students[0] = new Student("Petrov", 1, new int[]{7, 5, 10, 8, 6});
        students[1] = new Student("Vovan", 2, new int[]{9, 9, 10, 9, 10});
        students[2] = new Student("Volan", 3, new int[]{7, 8, 10, 8, 9});
        students[3] = new Student("Mytko", 2, new int[]{7, 5, 10, 8, 6});
        students[4] = new Student("Sanek", 3, new int[]{7, 10, 10, 8, 10});
        students[5] = new Student("Makarova", 1, new int[]{7, 5, 10, 8, 6});
        students[6] = new Student("Zayka", 3, new int[]{10, 10, 10, 10, 10});
        students[7] = new Student("Lodyr", 1, new int[]{8, 9, 10, 8, 8});
        students[8] = new Student("Kolobkov", 2, new int[]{9, 9, 9, 9, 9});
        students[9] = new Student("Kolbasnikov", 1, new int[]{10, 10, 10, 9, 9});

        printStudents(students, 9);
    }

    private static void printStudents(Student[] students, int mark) {

        for (Student currentStudent : students) {
            int[] marks = currentStudent.getMarks();

            boolean isExcellent = true;
            for (int tempMark : marks) {
                if (mark > tempMark) {
                    isExcellent = false;
                    break;
                }
            }

            if (isExcellent) {
                System.out.println("Student: " + currentStudent.getName());
                System.out.println("Number of group: " + currentStudent.getGroup() + "\n");
            }
        }
    }
}
