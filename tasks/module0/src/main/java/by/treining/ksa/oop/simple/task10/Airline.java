package by.treining.ksa.oop.simple.task10;

import java.util.Calendar;

/*
10. Создать класс Airline, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы
и метод toString(). Создать второй класс, агрегирующий массив типа Airline, с подходящими конструкторами и
методами. Задать критерии выбора данных и вывести эти данные на консоль.
*/
/*
Airline: пункт назначения, номер рейса, тип самолета, время вылета, дни недели.
        Найти и вывести:
        a) список рейсов для заданного пункта назначения;
        b) список рейсов для заданного дня недели;
        c) список рейсов для заданного дня недели, время вылета для которых больше заданного.
*/
public class Airline {
    private String destination;
    private int flightNumber;
    private AirPlane airPlaneType;
    private Calendar departureTime;
    private DayOfWeek dayOfWeek;

    @Override
    public String toString() {
        return "Airline{" +
                "destination='" + destination + '\'' +
                ", flightNumber=" + flightNumber +
                ", airPlaneType=" + airPlaneType +
                ", departureTime=" + departureTime.getTime() +
                ", dayOfWeek=" + dayOfWeek +
                '}';
    }

    public Airline(String destination, int flightNumber, AirPlane airPlaneType, Calendar departureTime,
                   DayOfWeek dayOfWeek) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.airPlaneType = airPlaneType;
        this.departureTime = departureTime;
        this.dayOfWeek = dayOfWeek;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public AirPlane getAirPlaneType() {
        return airPlaneType;
    }

    public void setAirPlaneType(AirPlane airPlaneType) {
        this.airPlaneType = airPlaneType;
    }

    public Calendar getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Calendar departureTime) {
        this.departureTime = departureTime;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
