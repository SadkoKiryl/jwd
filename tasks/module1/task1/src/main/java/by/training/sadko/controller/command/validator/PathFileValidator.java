package by.training.sadko.controller.command.validator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathFileValidator implements Validator<String> {

    @Override
    public boolean isValid(String pathToFile) {
        Path path = Paths.get(pathToFile);
        return Files.exists(path) && (path.toString().endsWith(".txt") || path.toString().endsWith(".csv"));
    }
}
