package by.treining.ksa.oop.association.task1.model;

import by.treining.ksa.oop.association.task1.service.TextService;

import java.util.List;

public class Text implements TextService {
    private String heading;
    private List<Sentence> sentenceList;

    public Text(List<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
        heading = "Unknown author";
    }

    public Text(List<Sentence> sentenceList, String heading) {
        this.heading = heading;
        this.sentenceList = sentenceList;
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    public void setSentenceList(List<Sentence> sentenceList) {
        this.sentenceList = sentenceList;
    }

    @Override
    public void addingText(Sentence sentence) {
        this.sentenceList.add(sentence);

    }

    @Override
    public void printingText() {
        System.out.println(heading);
        for (Sentence sentence : this.sentenceList) {
            for (Word word : sentence.getWordList()) {
                System.out.print(word.getWord());
            }
        }

    }

    @Override
    public void headingText(String heading) {
        this.heading = heading;
    }
}
