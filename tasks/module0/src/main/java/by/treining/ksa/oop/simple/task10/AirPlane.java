package by.treining.ksa.oop.simple.task10;

public enum AirPlane {
    CIVILIAN_AIRCRAFT,
    WAR_PLANE,
    CARGO_AIRPLANE
}
