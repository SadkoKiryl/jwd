package by.training.sadko.parser;

public interface ParserFactory<T> {

    FileParser<T> getParser(String inputName);
}
