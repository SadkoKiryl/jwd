package by.treining.ksa.oop.simple.task7;

/*
7. Описать класс, представляющий треугольник. Предусмотреть методы для создания объектов, вычисления
площади, периметра и точки пересечения медиан.
*/
public class Main {
    public static void main(String[] args) {
        Point pointA = new Point(2, -5);
        Point pointB = new Point(5, 6);
        Point pointC = new Point(4, 4);

        Triangle triangle = new Triangle(pointA, pointB, pointC);

        System.out.println(triangle.toString());
    }
}
