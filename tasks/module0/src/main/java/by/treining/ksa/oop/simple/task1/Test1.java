package by.treining.ksa.oop.simple.task1;

/*
1. Создайте класс Test1 двумя переменными. Добавьте метод вывода на экран и методы изменения этих переменных.
Добавьте метод, который находит сумму значений этих переменных, и метод, который находит наибольшее значение
из этих двух переменных.
*/
public class Test1 {
    int number1;
    int number2;

    void printFields() {
        System.out.println(number1);
        System.out.println(number2);
    }

    void setFields(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    int sumOfFields() {
        int sum = number1 + number2;
        return sum;
    }

    int compareFields() {
        if (number1 > number2) {
            return number1;
        } else {
            return number2;
        }
    }
}
