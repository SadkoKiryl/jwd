package by.training.sadko.parser;

public enum ParserName {
    SAX("sax"),
    STAX("stax"),
    DOM("dom");

    private final String stringName;

    ParserName(String title) {
        this.stringName = title;
    }

    public static ParserName fromString(String inputName) {
        ParserName[] parsersArray = ParserName.values();
        for (ParserName parserName : parsersArray) {
            if (parserName.name().equalsIgnoreCase(inputName)
                    || parserName.stringName.equalsIgnoreCase(inputName)) {
                return parserName;
            }
        }
        return STAX;
    }

    public String getStringName() {
        return stringName;
    }
}
