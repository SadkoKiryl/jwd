package by.training.sadko.parser.xml.stax;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import java.util.List;

public abstract class StAXHandler<T> {

    protected List<T> entityFromXML;

    public abstract List<T> getListFromXML(XMLEventReader xmlEventReader) throws XMLStreamException;
}
