package by.training.sadko.xmlbuilder;

import by.training.sadko.entity.Newspaper;
import by.training.sadko.entity.Publication;

public class NewspaperXMLBuilder implements EntityStringBuilder<Publication> {

    @Override
    public String build(Publication publication) {
        Newspaper newspaper = (Newspaper) publication;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<Newspaper id=\"").append(newspaper.getId()).append("\">\n")
                .append("<title>").append(newspaper.getTitle()).append("</title>\n")
                .append("<monthly>").append(newspaper.isMonthly()).append("</monthly>\n")
                .append("<colored>").append(newspaper.isColored()).append("</colored>\n")
                .append("<content>").append(newspaper.getContentType()).append("</content>\n")
                .append("<volume>").append(newspaper.getVolume()).append("</volume>\n")
                .append("<type>").append(newspaper.getType()).append("</type>\n")
                .append("<subscriptionIndex>").append(newspaper.getSubscriptionIndex()).append("</subscriptionIndex>\n")
                .append("</Newspaper>\n");

        return stringBuffer.toString();
    }
}
