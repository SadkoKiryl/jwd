package by.treining.ksa.oop.simple.task7;

import static java.lang.Math.*;

class Point {

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    static double distanceCalculation(Point pointA, Point pointB) {
        double deltaX = abs(pointA.getX()) - abs(pointB.getX());
        double deltaY = abs(pointA.getY()) - abs(pointB.getY());
        double distance = sqrt(pow(deltaX, 2) + pow(deltaY, 2));
        return distance;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
