package by.treining.ksa.basics;

import static java.lang.Math.*;

public class SolutionLinear {

    public static void main(String[] args) {

        task_1();
        task_2();
        task_3();
        task_4();
        task_5();
        task_6();
        task_7();
        task_8();
        task_9();
        task_10();
        task_11();
        task_12();
        task_13();
        task_14();
        task_15();
        task_16();
        task_17();
        task_18();
        task_19();
        task_20();
        task_21();
        task_22();
        task_23();
        task_24();
        task_25();
        task_26();
        task_27();
        task_28();
        task_29();
        task_30();
        task_31();
        task_33();
        task_34();
        task_35();
        task_36();
        task_39();
        task_40();
    }

    private static void task_40() {

        System.out.println("\n\nДано значение х. Получить значения -2х + 3х2 - 4х3 и 1 + 2х + 3х2 + 4х3 . Позаботьтесь об экономии операций.");
        double x = 10;

        double temp1 = 2 * x + 4 * x * x * x;
        double temp2 = 3 * x * x;

        double result1 = - temp1 + temp2;
        double result2 = temp1 + temp2 + 1;

        System.out.println("х = " + x + ". The answers are " + result1 + " & " + result2);
    }

    private static void task_39() {

        System.out.println("\n\n39. Дано действительное число х. Не пользуясь никакими другими арифметическими операциями, кроме умножения, сложения и вычитания, вычислите за минимальное число операций:");
        double x = 10;

        double result = x * x * x * (2 * x - 3) + x * (4 * x - 5) + 6;

        System.out.println("The answer is " + result);
    }

    private static void task_36 () {

        System.out.println("\n\n36. Найти частное произведений четных и нечетных цифр четырехзначного числа.");
        int number = 1234;

        double quotient = 0.0;

        int composition1 = 1;
        int composition2 = 1;

        int newNumber = number;

        int count = 0;

        while (newNumber != 0){
            composition1 = composition1 * (newNumber % 10);
            newNumber = (int) newNumber / 100;
            count ++;
            System.out.println(count + " " + newNumber + " " + composition1);
        }

        newNumber = number / 10;
        while (newNumber != 0){
            composition2 = composition2 * (newNumber % 10);
            newNumber = (int) newNumber / 100;
            count++;
            System.out.println(count + " " + newNumber + " " + composition2);
        }

        if (count % 2 != 0) {
            System.out.println("The product of odd numbers = " + composition1);
            System.out.println("The product of even numbers = " + composition2);
            quotient = (double) composition2 / composition1;
            System.out.println(composition2 + " / " + composition1 + " = " + quotient);
        }
        else {
            System.out.println("The product of odd numbers = " + composition2);
            System.out.println("The product of even numbers = " + composition1);
            quotient = (double) composition1 / composition2;
            System.out.println(composition1 + " / " + composition2 + " = " + quotient);
        }
    }

    private static void task_35() {

        System.out.println("\n\n35. Даны натуральные числа М и N. Вывести старшую цифру дробной части и младшую цифру целой части числа M/N.");
        int m = 245;
        int n = 6;

        double result = (double) m / n;

        int min = (int) (result % 10);
        int max = (int) ((result * 10) % 10);

        System.out.println(m + " / " + n + " = " + result);
        System.out.println("The max digit of the factorial part: " + max);
        System.out.println("The min digit of the factorial part: " + min);
    }

    private static void task_34() {

        System.out.println("\n\n34. Дана величина А, выражающая объем информации в байтах. Перевести А в более крупные единицы измерения информации.");
        long initBytes = 9999999999L;

        double kBytes = initBytes / Math.pow(2, 10);
        double mBytes = initBytes / Math.pow(2, 20);
        double gBytes = initBytes / Math.pow(2, 30);
        double tBytes = initBytes / Math.pow(2, 40);
        double pBytes = initBytes / Math.pow(2, 50);

        System.out.println(initBytes + " byte");

        System.out.println(kBytes + " kilo bytes");
        System.out.println(mBytes + " mega bytes");
        System.out.println(gBytes + " giga bytes");
        System.out.println(tBytes + " tera bytes");
        System.out.println(pBytes + " peta bytes");
    }

    private static void task_33() {

        System.out.println("\n\n33. Ввести любой символ и определить его порядковый номер, а также указать предыдущий и последующий символы.");
        char currentCharacter = 'd';
        int asciiCurrent = (int) currentCharacter;

        int asciiPrev = asciiCurrent - 1;
        char prevCharacter = (char) asciiPrev;

        int asciiNext = asciiCurrent + 1;
        char nextCharacter = (char) asciiNext;

        System.out.println("The current symbol: " + currentCharacter + ", his number: " + asciiCurrent);
        System.out.println("Previous symbol: " + prevCharacter + ", his number: " + asciiPrev);
        System.out.println("Next symbol: " + nextCharacter + ", his number: " + asciiNext);
    }

    private static void task_31() {

        System.out.println("\n\n31. Составить программу для вычисления пути, пройденного лодкой, если ее скорость в стоячей воде v км/ч, скорость течения реки v1 км/ч, время движения по озеру t1 ч, а против течения реки — t2 ч.");
        double v = 10.0;
        double v1 = 2.0;
        double t1 = 2;
        double t2 = 1;
        double s = t1 * v + t2 * (v - v1);

        System.out.println("v = " + v + " v1 = " + v1 + " t1 = " + t1 + " t2 = " + t2);
        System.out.println("Track = " + s);
    }

    private static void task_30() {

        System.out.println("\n\n30. Три сопротивления R1 R2, и R3 соединены параллельно. Найдите сопротивление соединения.");
        double r1 = 0.8;
        double r2 = 0.6;
        double r3 = 0.6;

        double r = 1 / (1 / r1 + 1 / r2 + 1 / r3);

        System.out.println("R1 = " + r1 + " R2 = " + r2 + " R3 = " + r3);
        System.out.println("Resistance = " + r);
    }

    private static void task_29() {

        System.out.println("\n\n29. Найти (в радианах в градусах) все углы треугольника со сторонами а, b, с.");
        double a = 2;
        double b = 6;
        double c = 7;


        double angleARad = acos((b * b + c * c - a * a) / (2 * b * c));
        double angleADeg = angleARad * 180 / PI;

        double angleBRad = acos((a * a + c * c - b * b) / (2 * a * c));
        double angleBDegr = angleBRad * 180 / PI;

        double angleCRad = acos((a * a + b * b - c * c) / (2 * a * b));
        double angleCDegr = angleCRad * 180 / PI;

        System.out.println("The triangle with sides: " + a + " " + b + " " + c);
        System.out.println("The angle 1 = " + angleARad + " rad " + angleADeg + " degrees");
        System.out.println("The angle 2 = " + angleBRad + " rad " + angleBDegr + " degrees");
        System.out.println("The angle 3 = " + angleCRad + " rad " + angleCDegr + " degrees");
    }

    private static void task_28() {

        System.out.println("\n\n28. Составить программу перевода радианной меры угла в градусы, минуты и секунды.");
        double initRad = 5;
        double initDeg = initRad * 180 / PI;

        int degrees = (int) initDeg;

        double temp = (initDeg - degrees) * 60;
        int minutes = (int) temp;

        double seconds = (temp - minutes) * 60;

        System.out.println("In radians " + initRad);
        System.out.println(degrees + " degrees " + minutes + " minutes " + seconds + " c.");
    }

    private static void task_27() {

        System.out.println("\n\n27. Дано значение a. Не используя никаких функций и никаких операций, кроме умножения, получить значение а8 за три операции и а10 за четыре операции.");
        double a = 2.0;
        double a2 = a * a;
        double a4 = a2 * a2;
        double a8 = a4 * a4;
        double a10 = a8 * a2;
        System.out.println("a = " + a + "\nа8 = " + a8 + " и а10 = " + a10);
    }

    private static void task_26() {

        System.out.println("\n\n26. Найти площадь треугольника, две стороны которого равны а и b, а угол между этими сторонами у.");
        double a = 2.0;
        double b = 1.0;
        double angle = 30;

        double cornerRad = angle * PI / 180.0;

        double area = 0.5 * a * b * sin(cornerRad);
        System.out.println("The area of the triangle is " + area);
    }

    private static void task_25() {

        System.out.println("\n\n25. Вычислить корни квадратного уравнения ах2+ bх + с = 0 с заданными коэффициентами a, b и с (предполагается, что а≠0 и что дискриминант уравнения неотрицателен).\n");
        double a = 12.6;
        double b = 42.8;
        double c = 3.1;

        double discriminant = pow(b, 2) - 4 * a * c;
        double root = (-b + sqrt(discriminant)) / (2 * a);

        if (discriminant == 0) {
            double x = root;
            System.out.println("Root of the function is " + x);
        } else if (discriminant > 0) {
            double x1 = root;
            double x2 = (-b - sqrt(discriminant)) / (2 * a);
            System.out.println("Roots of the function are x1 = " + x1 + ", x2 = " + x2);
        } else {
            System.out.println("Function has no roots.");
        }
    }

    private static void task_24() {

        System.out.println("\n\n24. Найти площадь равнобедренной трапеции с основаниями а и b и углом α при большем основании а.\n");
        double minBase = 8;
        double maxBase = 12;
        double angle = 30; //degrees
        double area = calculatorOfArea_24(minBase, maxBase, angle);
    }

    private static double calculatorOfArea_24(double a, double b, double angle) {

        if (a == b || angle >= 90 || a <= 0 || b <= 0) {
            System.out.println("Invalid parameter values entered");
            return -1;
        }

        angle = toRadians(angle);
        double temp = (b - a) / 2;
        double h = temp * tan(angle);

        double area = a * h + h * temp;
        System.out.println("The area of the trapeze is " + area);

        return area;
    }


    private static void task_23() {

        System.out.println("\n\n23. Найти площадь кольца, внутренний радиус которого равен r, а внешний — R (R> r).\n");
        double radiusInner = 24;
        double radiusOuter = 56;
        System.out.println("Area of the ring is " + calculatorOfArea_23(24, 56));
    }

    private static double calculatorOfArea_23(double rInn, double rOut) {
        double area = PI * pow(rOut, 2) - PI * pow(rInn, 2);
        return area;
    }


    private static void task_22() {

        System.out.println("\n\n22. Дано натуральное число Т, которое представляет длительность прошедшего времени в секундах. Вывести данное значение длительности в часах, минутах и секундах в следующей форме: ННч ММмин SSc.\n");
        int time = 15745; //4h 22m 25s
        int hours = (time / 3600);
        int temp = time - (hours * 3600);
        int minutes = temp / 60;
        int seconds = temp - (minutes * 60);
        System.out.println("Исходное время в секундах: " + time);
        System.out.println(hours + " ч. " + minutes + " мин. " + seconds + " c.");
    }

    private static void task_21() {

        System.out.println("\n\n21. Дано действительное число R вида nnn.ddd (три цифровых разряда в дробной и целой частях). Поменять местами дробную и целую части числа и вывести полученное значение числа.\n");
        double r = 123.789;
        double number = r * 1000 % 1000 + (int) r / 1000.0;
        System.out.println("Changed number is " + number);
    }

    private static void task_20() {

        System.out.println("\n\n20. Известна длина окружности. Найти площадь круга, ограниченного этой окружностью.\n");
        double lengthOfCircle = 9;
        double radius = lengthOfCircle / (2 * PI);
        double area = PI * pow(radius, 2);
        System.out.println("The area of the circle is " + area);
    }


    private static void task_19() {

        System.out.println("\n\n19. Дана сторона равностороннего треугольника. Найти площадь этого треугольника, его высоту, радиусы вписанной и описанной окружностей.\n");
        double a = 12;
        double areaOfTriangle = calculatorOfArea_19(a);
        double heightOfTriangle = calculatorOfHeight_19(a);
        double radiusDescribed = calculatorOfDiscRad_19(a);
        double radiusInscribed = calculatorOfInscRad_19(a);

        System.out.println("The area of the triangle is " + areaOfTriangle);
        System.out.println("The height of the triangle is " + heightOfTriangle);
        System.out.println("The described radius of the triangle is " + radiusDescribed);
        System.out.println("The inscribed radius of the triangle is " + radiusInscribed);
    }

    private static double calculatorOfInscRad_19(double a) {
        double radius = a / (sqrt(3) * 2);
        return radius;
    }

    private static double calculatorOfDiscRad_19(double a) {
        double radius = a / sqrt(3);
        return radius;
    }

    private static double calculatorOfHeight_19(double a) {
        double result = sqrt(3) / 2 * a;
        return result;
    }

    private static double calculatorOfArea_19(double a) {
        double result = pow(a, 2) * sqrt(3) / 4;
        return result;
    }


    private static void task_18() {

        System.out.println("\n\n18. Дана длина ребра куба. Найти площадь грани, площадь полной поверхности и объем этого куба.\n");
        double sideOfCube = 22;
        double areaOfFace = pow(sideOfCube, 2);
        double areaOfSurface = areaOfFace * 6;
        double volumeOfCube = pow(sideOfCube, 3);
        System.out.println("The area of the cube's face is " + areaOfFace + "\nThe area of the cube is " + areaOfSurface + "\nThe volume of the cube is " + volumeOfCube);
    }


    private static void task_17() {

        System.out.println("\n\n17. Даны два числа. Найти среднее арифметическое кубов этих чисел и среднее геометрическое модулей этих чисел.\n");
        double x = 12;
        double y = 24;
        double arithmeticMean = calculatorOfArithmeticMean_17(x, y);
        double geometricMean = calculatorOfGeometricMean_17(x, y);
        System.out.println("The arithmetic mean is " + arithmeticMean + "\nThe geometric mean is " + geometricMean);
    }

    private static double calculatorOfGeometricMean_17(double x, double y) {
        double result = sqrt(abs(x) * abs(y));
        return result;
    }

    private static double calculatorOfArithmeticMean_17(double x, double y) {
        double result = (pow(x, 3) + pow(y, 3)) / 2;
        return result;
    }


    private static void task_16() {

        System.out.println("\n\n16. Найти произведение цифр заданного четырехзначного числа.\n");
        int number = 3589;
        String[] arrayOfNumbers = String.valueOf(number).split(""); //array with each digit

        int multiply = 1;
        for (String s : arrayOfNumbers) {
            multiply *= Integer.valueOf(s);
        }
        System.out.println(multiply);
    }

    private static void task_15() {
        System.out.println("\n\n15. Написать программу, которая выводит на экран первые четыре степени числа π.\n");

        for (int i = 1; i < 5; i++) {
            double powerOfPi = pow(PI, i);
            System.out.println(powerOfPi);
        }
    }


    private static void task_14() {

        System.out.println("\n\n14. Вычислить длину окружности и площадь круга одного и того же заданного радиуса R.\n");
        double r = 14;
        double lengthOfCircle = 2 * PI * r;
        double areaOfCircle = PI * pow(r, 2);
        System.out.println("Length of circle is " + lengthOfCircle + ".\n" + "Area of circle is " + areaOfCircle + ".");
    }


    private static void task_13() {

        System.out.println("\n\n13. Заданы координаты трех вершин треугольника (х1 у2),(х2, у2) ),(х3, у3). Найти его периметр и площадь.\n");
        double x1 = 18.6;
        double y1 = 7.2;
        double x2 = -5;
        double y2 = 2.1;
        double x3 = 5.1;
        double y3 = 2;

        double firstSide = calculatorOfSide_13(x1, y1, x2, y2);
        double secondSide = calculatorOfSide_13(x2, y2, x3, y3);
        double thirdSide = calculatorOfSide_13(x1, y1, x3, y3);

        double perimeterOfTriangle = firstSide + secondSide + thirdSide;
        double areaOfTriangle = calculatorOfArea_13(firstSide, secondSide, thirdSide);

        System.out.println("Perimeter of triangle is " + perimeterOfTriangle +
                ". \nArea of triangle is " + areaOfTriangle);
    }

    private static double calculatorOfArea_13(double a, double b, double c) {
        if (a + b >= c && a + c >= b && b + c >= a) {
            double p = (a + b + c) / 2; //half of triangle's perimeter
            double area = sqrt(p * (p - a) * (p - b) * (p - c));
            return area;
        } else {
            System.out.println("One side of the triangle is greater than the sum of the other two sides. " +
                    "Such a triangle cannot exist.\n");
            return -1;
        }
    }

    private static double calculatorOfSide_13(double x1, double y1, double x2, double y2) {
        double deltaX = abs(x1) - abs(x2);
        double deltaY = abs(y1) - abs(y2);
        double sideOfTriangle = sqrt(pow(deltaX, 2) + pow(deltaY, 2));
        return sideOfTriangle;
    }


    private static void task_12() {

        System.out.println("\n\n12. Вычислить расстояние между двумя точками с данными координатами (х1, у1)и (x2, у2).\n");
        double x1 = 18.6;
        double y1 = 7.2;
        double x2 = -5;
        double y2 = 2.1;
        double a = abs(x1) - abs(x2);
        double b = abs(y1) - abs(y2);
        double distance = sqrt(pow(a, 2) + pow(b, 2));
        System.out.println("Distance between two point's is " + distance);
    }

    private static void task_11() {

        System.out.println("\n\n11. Вычислить периметр и площадь прямоугольного треугольника по длинам а и b двух катетов.\n");
        double a = 14.0;
        double b = 7.2;
        double c = sqrt(pow(a, 2) + pow(b, 2)); //The hypotenuse of this triangle
        double perimeter = a + b + c;
        double area = 0.5 * a * b;
        System.out.println("The area of the triangle is " + area + ", the perimeter is " + perimeter);
    }

    private static void task_10() {

        System.out.println("\n\n10. Вычислить значение выражения по формуле (все переменные принимают действительные значения): см. задание.\n");
        double x = 14.0;
        double y = 7.2;
        double result = (sin(x) + cos(y)) / (cos(x) - sin(y)) * tan(x) * y;
        System.out.println("Result is " + result);
    }

    private static void task_9() {

        System.out.println("\n\n9. Вычислить значение выражения по формуле (все переменные принимают действительные значения): см. задание.\n");
        double a = 14.0;
        double b = 7.2;
        double c = 2.1;
        double d = 8.0;
        double result = a / c * b / d - ((a * b - c) / (c * d));
        System.out.println("Result is " + result);
    }

    private static void task_8() {

        System.out.println("\n\n8. Вычислить значение выражения по формуле (все переменные принимают действительные значения): см. задание.\n");
        double a = 14;
        double b = 7.2;
        double c = 2.1;
        double result = (b + sqrt(pow(b, 2) + 4 * a * c)) / (2 * a) - pow(a, 3) * c + pow(b, -2);
        System.out.println("Result is " + result);
    }

    private static void task_7() {

        System.out.println("\n\n7. Дан прямоугольник, ширина которого в два раза меньше длины. Найти площадь прямоугольника\n");
        int lengthOfRec = 10;
        int widthOfRec = lengthOfRec / 2;
        int areaOfRec = lengthOfRec * widthOfRec;
        System.out.println("The area of the rectangle is " + areaOfRec);
    }

    private static void task_6() {

        System.out.println("\n\n6. Написать код для решения задачи. В n малых бидонах 80 л молока. Сколько литров молока в m больших бидонах, если в каждом большом бидоне на 12 л. больше, чем в малом?\n");
        int n = 13; //Count of small cans
        int m = 26; //Count of big cans
        int valueOfSmallCan = 80 / n;
        int valueOfBigCan = valueOfSmallCan + 12;
        int capacityOfBigCans = valueOfBigCan * m;
        System.out.println("Count of small can's is " + n + ", value of small can is " + valueOfSmallCan + " liters. Whole milk in small can's is 80 liters.\n\n" + "Count of large can's is " + m + ", value of large can is " + valueOfBigCan + " liters. Whole milk in large can's is " + capacityOfBigCans + " liters.");
    }

    private static void task_5() {

        System.out.println("\n\n5. Составить алгоритм нахождения среднего арифметического двух чисел\n");
        double a = 5;
        double b = 8;
        double average = (a + b) / 2;
        System.out.println("Arithmetical mean of " + a + " and " + b + " is " + average);
    }

    private static void task_4() {

        System.out.println("\n\n4. Найдите значение функции: z = ( (a – 3 ) * b / 2) + c.\n");
        double a = 13.45;
        double b = 24.1;
        double c = -1;
        double z = (a - 3) * b / 2 + c;
        System.out.println("Function value is " + z);
    }

    private static void task_3() {

        System.out.println("\n\n3. Найдите значение функции: z = 2 * x + ( y – 2 ) * 5.\n");
        double x = 2.3;
        double y = 2.14;
        double z = 2 * x + (y - 2) * 5;
        System.out.println("Function value is " + z);
    }

    private static void task_2() {

        System.out.println("\n\n2. Найдите значение функции: с = 3 + а.\n");
        double a = 9.3;
        double c = 3 + a;
        System.out.println("Function value is " + c);
    }


    public static void task_1() {

        System.out.println("\n1. Даны два действительных числа х и у. Вычислить их сумму, разность, произведение и частное.\n");
        double x = 8.2;
        double y = 13;
        double sum = x + y;
        double sub = x - y;
        double mult = x * y;
        double div = x / y;
        System.out.println("Sum is " + sum + "\nSubtraction is " + sub + "\nMultiply is " + mult + "\nDivision is " + div);
    }
}
