package by.training.sadko.entity;

public enum PublicationType {

    MAGAZINE("Magazine"),
    NEWSPAPER("Newspaper"),
    BOOKLET("Booklet");

    private final String value;

    PublicationType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
