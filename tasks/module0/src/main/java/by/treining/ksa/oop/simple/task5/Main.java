package by.treining.ksa.oop.simple.task5;

/*
5. Опишите класс, реализующий десятичный счетчик, который может увеличивать или уменьшать свое значение
на единицу в заданном диапазоне. Предусмотрите инициализацию счетчика значениями по умолчанию и
произвольными значениями. Счетчик имеет методы увеличения и уменьшения состояния, и метод позволяющее
получить его текущее состояние. Написать код, демонстрирующий все возможности класса.
*/

public class Main {
    public static void main(String[] args) {
        DecimalCounter counter1 = new DecimalCounter();
        counter1.counterDisplay();

        DecimalCounter counter2 = new DecimalCounter(100, 0, 99);
        counter2.counterDisplay();
    }
}
