<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Library</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
          crossorigin="anonymous">
</head>
<body class="p-3 mb-2 bg-light text-dark">

<section class="card jumbotron">
    <div class="card-header">

    </div>
    <div class="card-body">
        <div class="container">
            <h1 class="display-4">
                Simple Servlet App
            </h1>
            <h2 class="lead">
                It is a simple servlet applications witch supports uploading and downloading xml files to data base
            </h2>
        </div>
    </div>
</section>

<section>
    <div class="columns">
        <div class="column is-one-quarter">
            <jsp:include page="nav_bar.jsp"/>
        </div>
    </div>
</section>

<section>
    <div class="columns">
        <div class="column is-full">
            <jsp:include page="views/${viewName}.jsp"/>
        </div>
    </div>
</section>

<section class="card-footer ">
    <div class="text-sm-center">
        Simple Servlet App, 2020
    </div>
</section>

</body>
</html>
