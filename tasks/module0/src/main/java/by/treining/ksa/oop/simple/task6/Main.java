package by.treining.ksa.oop.simple.task6;

public class Main {
    public static void main(String[] args) {
        Clock clock = new Clock();
        clock.setHours(12);
        clock.setMinutes(45);
        clock.setSeconds(50);
        System.out.println(clock.toString());

        clock.setTime(13, 60, 23);
        System.out.println(clock.toString());

        clock.timeChanging(5, 8, 23);
        System.out.println(clock.toString());
    }
}
