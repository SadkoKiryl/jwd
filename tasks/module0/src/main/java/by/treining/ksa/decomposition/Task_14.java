package by.treining.ksa.decomposition;

/*
14. Написать метод(методы), определяющий, в каком из данных двух чисел больше цифр.
*/
public class Task_14 {

    public static void main(String[] args) {
        int num1 = 2351;
        int num2 = 2349;

        compareNumberOfDigits(num1, num2);

    }

    private static void compareNumberOfDigits(int num1, int num2) {
        String[] arrayNum1 = String.valueOf(num1).split("");
        String[] arrayNum2 = String.valueOf(num2).split("");

        if (arrayNum1.length > arrayNum2.length) {
            System.out.println("\nThe number " + num1 + " contains more digits.");
        } else if (arrayNum1.length < arrayNum2.length) {
            System.out.println("\nThe number " + num2 + " contains more digits.");
        } else {
            System.out.println("\nThe numbers " + num1 + " and " + num2 + " contain the same number of digits.");
        }
    }
}
