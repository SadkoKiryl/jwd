package by.treining.ksa.decomposition;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/*
7. На плоскости заданы своими координатами n точек. Написать метод(методы), определяющие, между какими из
        пар точек самое большое расстояние. Указание. Координаты точек занести в массив.
*/
public class Task_7 {

    public static void main(String[] args) {
        int n = 4;

        System.out.println("\n\nThe largest distance between points is " + largestDistance(n));
    }

    private static double largestDistance(int n) {
        int arrayOfPoints[][] = createArrayOfPoints(n);
        double largestDistance = calculateDistance(arrayOfPoints);

        return largestDistance;
    }

    private static double calculateDistance(int[][] arrayOfPoints) {

        double largestDistance = Double.MIN_VALUE;

        for (int i = 0; i < arrayOfPoints.length; i++) {
            int x1 = arrayOfPoints[i][0];
            int y1 = arrayOfPoints[i][1];

            int x2;
            int y2;
            for (int k = i + 1; k < arrayOfPoints.length - 1; k++) {

                x2 = arrayOfPoints[k][0];
                y2 = arrayOfPoints[k][1];

                double deltaX = x2 - x1;
                double deltaY = y2 - y1;
                double distance = sqrt(pow(deltaX, 2) + pow(deltaY, 2));

                if (distance > largestDistance) {
                    largestDistance = distance;
                }
            }
        }

        return largestDistance;
    }

    private static int[][] createArrayOfPoints(int n) {
        int[][] arrayOfPoints = new int[n][2]; //each row contains x and y of point

        System.out.print("The coordinates of points:");
        for (int i = 0; i < arrayOfPoints.length; i++) {
            System.out.print("\nThe point " + (i + 1) + ": ");
            for (int j = 0; j < arrayOfPoints[i].length; j++) {
                arrayOfPoints[i][j] = (int) (Math.random() * 10) - 5;
                System.out.print(arrayOfPoints[i][j] + " ");
            }
        }
        return arrayOfPoints;
    }
}
