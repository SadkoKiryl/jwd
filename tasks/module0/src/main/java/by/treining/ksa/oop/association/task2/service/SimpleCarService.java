package by.treining.ksa.oop.association.task2.service;

import by.treining.ksa.oop.association.task2.model.Car;
import by.treining.ksa.oop.association.task2.model.GasEngine;
import by.treining.ksa.oop.association.task2.model.Wheel;

public class SimpleCarService implements CarService {

    @Override
    public void go(Car car) {
        GasEngine engine = car.getEngine();
        Wheel wheel = car.getWheel();

        if (engine.isRefueled() && !wheel.isPierced() && !engine.isRunning()) {
            engine.setRunning(true);
            System.out.println("The engine is running, the car is ready to go!");
        } else {
            if (!engine.isRefueled()) {
                this.fillingCar(car);
                this.go(car);
            } else {
                if (wheel.isPierced()) {
                    this.wheelRepayment(car);
                    this.go(car);
                } else {
                    if (engine.isRunning()) {
                        System.out.println("The engine is already running, the car is ready to go!");
                    }
                }
            }
        }
    }

    @Override
    public void fillingCar(Car car) {
        int currentVolume = car.getEngine().getCurrentVolume();
        int tankVolume = car.getEngine().getTankVolume();

        if (currentVolume < tankVolume) {
            car.getEngine().setCurrentVolume(tankVolume);
            car.getEngine().setRefueled(true);
            System.out.println("The car is refueled");
        }
    }

    @Override
    public void wheelRepayment(Car car) {
        Wheel wheel = car.getWheel();
        if (wheel.isPierced()) {
            wheel.setPierced(false);
            System.out.println("Wheel is repaired!");
        }
    }

    @Override
    public void getModelCar(Car car) {
        System.out.println("The model of the car is " + car.getModel());
    }
}
