package by.treining.ksa.oop.simple.task4;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static by.treining.ksa.oop.simple.task4.Train.getInfo;

/*
4. Создайте класс Train, содержащий поля: название пункта назначения, номер поезда, время отправления.
Создайте данные в массив из пяти элементов типа Train, добавьте возможность сортировки элементов массива по
номерам поездов. Добавьте возможность вывода информации о поезде, номер которого введен пользователем.
Добавьте возможность сортировки массива по пункту назначения, причем поезда с одинаковыми пунктами назначения
должны быть упорядочены по времени отправления.
*/

public class Main {
    public static void main(String[] args) {
        Train train1 = new Train(1, "Minsk",
                new GregorianCalendar(2020, Calendar.MARCH, 10, 6, 30));

        Train train2 = new Train(2, "Brest",
                new GregorianCalendar(2020, Calendar.MARCH, 9, 6, 30));

        Train train3 = new Train(3, "Minsk",
                new GregorianCalendar(2020, Calendar.MARCH, 10, 6, 35));

        Train train4 = new Train(4, "Minsk",
                new GregorianCalendar(2020, Calendar.MARCH, 12, 6, 40));

        Train train5 = new Train(5, "Minsk",
                new GregorianCalendar(2020, Calendar.MARCH, 1, 6, 45));

        Train[] trains = {train1, train2, train3, train4, train5};

        getInfo(2, trains);
        System.out.println("--------------------------------------");

        Train[] sortedByNumber = TrainComparator.sortByTrainNumber(trains);
        for (Train train : sortedByNumber) {
            System.out.println(train.toString());
        }

        System.out.println("--------------------------------------");

        Train[] sortedByDestination = TrainComparator.sortByDestinationAndDeparture(trains);
        for (Train train : sortedByDestination) {
            System.out.println(train.toString());
        }


    }
}
