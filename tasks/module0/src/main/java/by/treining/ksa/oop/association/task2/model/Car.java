package by.treining.ksa.oop.association.task2.model;

public class Car {
    private Wheel wheel;
    private GasEngine engine;
    private String model;

    public Car(Wheel wheel, GasEngine engine, String model) {
        this.wheel = wheel;
        this.engine = engine;
        this.model = model;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public GasEngine getEngine() {
        return engine;
    }

    public void setEngine(GasEngine engine) {
        this.engine = engine;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
