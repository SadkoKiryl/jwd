package by.training.sadko.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AppCommand {

    String execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException;
}
