package by.training.sadko.controller;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.controller.command.AppCommandRegistry;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.controller.command.impl.AppCommandName.DEFAULT;

public class SimpleHttpHandler implements HttpHandler {

    private static final Logger LOGGER = LogManager.getLogger(SimpleHttpHandler.class.getName());
    protected final AppCommandRegistry commandRegistry;


    public SimpleHttpHandler(AppCommandRegistry commandRegistry) {
        this.commandRegistry = commandRegistry;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String query = getQueryString(exchange);
        Map<String, String> requestMap = getRequestMap(query);
        AppCommand command = getCommand(requestMap);

        String responseView = commandExecute(command, requestMap);
        LOGGER.info("ResponseView is {}", responseView);
        String templateView = getTemplateFromResources();

        String view = MessageFormat.format(templateView, responseView);
        OutputStream responseBody = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, view.length());
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
    }

    private String getQueryString(HttpExchange exchange) {
        String requestMethod = exchange.getRequestMethod();
        URI requestURI = exchange.getRequestURI();
        LOGGER.info("Received incoming request: {} URI {}", requestMethod, requestURI);

        String query = requestURI.getQuery();
        LOGGER.info("Queried: {}", query);

        if ("POST".equalsIgnoreCase(requestMethod)) {
            StringBuilder requestBuilder = new StringBuilder();

            try (InputStream requestBody = exchange.getRequestBody();
                 InputStreamReader streamReader = new InputStreamReader(requestBody, Charset.forName
                         (StandardCharsets.UTF_8.name()));
                 Reader reader = new BufferedReader(streamReader)) {

                int c;
                while ((c = reader.read()) != -1) {
                    requestBuilder.append((char) c);
                }
                query = URLDecoder.decode(String.valueOf(requestBuilder), StandardCharsets.UTF_8);

            } catch (IOException ioException) {
                LOGGER.error("Unable to process requestBody");
            }
        }
        return query;
    }

    private Map<String, String> getRequestMap(String query) {
        Map<String, String> requestMap = new HashMap<>();
        if (query != null) {
            try {
                String[] paramsArray = query.split("&");
                for (String param : paramsArray) {
                    String paramName = param.split("=")[0];
                    String paramValue = param.split("=")[1];
                    requestMap.put(paramName, paramValue);
                }
            } catch (IndexOutOfBoundsException e) {
                LOGGER.error("Request parameters not found");
                return requestMap;
            }
        }
        return requestMap;
    }

    private AppCommand getCommand(Map<String, String> paramsMap) {
        String commandName = paramsMap.get(COMMAND_NAME);
        return commandRegistry.getCommand(commandName);
    }

    private String commandExecute(AppCommand command, Map<String, String> paramsMap) {
        String result = command.execute(paramsMap);
        if (result == null) {
            command = commandRegistry.getCommand(DEFAULT.getShortCommand());
            result = command.execute(paramsMap);
        }
        return result;
    }

    private String getTemplateFromResources() throws IOException {
        try (InputStream resourceAsStream = this.getClass().getResourceAsStream("/defaultTemplate.html");
             InputStreamReader streamReader = new InputStreamReader(resourceAsStream);
             BufferedReader bufferedReader = new BufferedReader(streamReader)) {

            return bufferedReader.lines().collect(Collectors.joining());

        } catch (IOException ioException) {
            LOGGER.error("Unable to get template from resources");
            throw ioException;
        }
    }
}
