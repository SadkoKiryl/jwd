package by.training.sadko.command;

import by.training.sadko.service.EntitiesService;
import by.training.sadko.xmlbuilder.FileBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class DownLoadFileCommand<T> implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(DownLoadFileCommand.class);
    private final EntitiesService<T> entitiesService;
    private final FileBuilder<T> fileBuilder;

    public DownLoadFileCommand(EntitiesService<T> entitiesService, FileBuilder<T> fileBuilder) {
        this.entitiesService = entitiesService;
        this.fileBuilder = fileBuilder;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) throws AppCommandException {
        List<T> entitiesList = entitiesService.getEntitiesList();
        if (!entitiesList.isEmpty()) {
            String responseString = fileBuilder.build(entitiesList);

            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    resp.getOutputStream()))) {

                resp.setHeader("Content-Disposition", "attachment;filename=publications.xml");
                resp.setContentType("text/xml");
                writer.write(responseString);
                writer.flush();

            } catch (IOException e) {
                LOGGER.error("File downloading exception");
                throw new AppCommandException("File downloading exception");
            }
            LOGGER.info("Download successful");
        } else {
            req.setAttribute("download_result", "Repository is empty");
            LOGGER.info("Repository is empty");
        }
        return "download_view";
    }
}
