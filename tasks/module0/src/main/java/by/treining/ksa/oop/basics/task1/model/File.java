package by.treining.ksa.oop.basics.task1.model;

public abstract class File {
    private String fileName;
    private Directory thisDirectory;

    public File(String fileName, Directory thisDirectory) {
        this.fileName = fileName;
        this.thisDirectory = thisDirectory;
    }

    public Directory getThisDirectory() {
        return thisDirectory;
    }

    public void setThisDirectory(Directory thisDirectory) {
        this.thisDirectory = thisDirectory;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
