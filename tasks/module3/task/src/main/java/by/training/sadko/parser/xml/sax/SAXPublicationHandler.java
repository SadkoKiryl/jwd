package by.training.sadko.parser.xml.sax;

import by.training.sadko.builder.PublicationBuilder;
import by.training.sadko.entity.Publication;
import by.training.sadko.entity.PublicationType;
import org.xml.sax.Attributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.training.sadko.AppConstants.*;

public class SAXPublicationHandler extends SAXHandler<Publication> {

    private final Map<PublicationType, PublicationBuilder> builderMap;
    private String elementValue;
    private PublicationBuilder publicationBuilder;

    public SAXPublicationHandler(Map<PublicationType, PublicationBuilder> builderMap) {
        this.builderMap = builderMap;
        this.entityFromXML = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        PublicationType[] publicationType = PublicationType.values();
        for (PublicationType type : publicationType) {
            String publicationName = type.getValue();
            if (qName.equalsIgnoreCase(publicationName)) {
                publicationBuilder = builderMap.get(type);
                publicationBuilder.setID(attributes.getValue(FIELD_ID));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case FIELD_CONTENT_TYPE:
                publicationBuilder.setContentType(elementValue);
                break;
            case FIELD_TITLE:
                publicationBuilder.setTitle(elementValue);
                break;
            case FIELD_MONTHLY:
                publicationBuilder.setIsMonthly(elementValue);
                break;
            case FIELD_COLORED:
                publicationBuilder.setIsColored(elementValue);
                break;
            case FIELD_VOLUME:
                publicationBuilder.setVolume(elementValue);
                break;
            case FIELD_GLOSSY:
                publicationBuilder.setIsGlossy(elementValue);
                break;
            case FIELD_SUBSCRIPTION_INDEX:
                publicationBuilder.setSubscriptionIndex(elementValue);
                break;

            case TAG_BOOKLET:
            case TAG_MAGAZINE:
            case TAG_NEWSPAPER:
                entityFromXML.add(publicationBuilder.getEntity());
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        elementValue = new String(ch, start, length);
    }

    @Override
    public List<Publication> getListFromXML() {
        return entityFromXML;
    }
}
