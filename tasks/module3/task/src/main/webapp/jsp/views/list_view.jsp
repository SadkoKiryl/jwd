<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<aside class="ui-menu">
    <ul class="ui-menu">
        <p class="menu-label">
            <b>Publication repository</b>
        </p>
    </ul>
</aside>
<div class="container">
    <div class="content">
        <c:if test="${not empty requestScope._list}">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="row">#</th>
                    <th scope="row"> ID</th>
                    <th scope="row">PUBLICATION TYPE</th>
                    <th scope="row"> TITLE</th>
                    <th scope="row">CONTENT TYPE</th>
                    <th scope="row">VOLUME</th>
                </tr>
                </thead>
                <tbody>

                <c:set var="i" value="1"/>
                <c:forEach items="${requestScope._list}" var="publication">
                    <tr>
                        <td><c:out value="${i}"/></td>
                        <td><c:out value="${publication.id}"/></td>
                        <td><c:out value="${publication.type}"/></td>
                        <td><c:out value="${publication.title}"/></td>
                        <td><c:out value="${publication.contentType.name}"/></td>
                        <td><c:out value="${publication.volume}"/></td>
                    </tr>
                    <c:set var="i" value="${i+1}"/>
                </c:forEach>

                </tbody>
            </table>
        </c:if>
        <div class="alert alert-info" role="alert">
            <c:if test="${not empty list_result}">
                <p class="ui-state-error-text">
                    <c:out value="${list_result}"/>
                </p>
            </c:if>
        </div>
    </div>
</div>