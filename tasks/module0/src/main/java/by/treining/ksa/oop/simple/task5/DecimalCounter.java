package by.treining.ksa.oop.simple.task5;

/*
5. Опишите класс, реализующий десятичный счетчик, который может увеличивать или уменьшать свое значение
на единицу в заданном диапазоне. Предусмотрите инициализацию счетчика значениями по умолчанию и
произвольными значениями. Счетчик имеет методы увеличения и уменьшения состояния, и метод позволяющее
получить его текущее состояние. Написать код, демонстрирующий все возможности класса.
*/

public class DecimalCounter {
    private int currentValue;
    private int maxValue;
    private int minValue;

    public DecimalCounter() {
        this.currentValue = 0;
        this.maxValue = 50;
        this.minValue = -50;
    }

    public DecimalCounter(int maxValue, int minValue, int currentValue) {
        this.maxValue = maxValue;
        this.minValue = minValue;
        this.currentValue = currentValue;
    }

    public void increaseValue() {
        if (this.currentValue < maxValue) {
            this.currentValue++;
        }
    }

    public void decreaseValue() {
        if (this.currentValue > minValue) {
            this.currentValue--;
        }
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public void counterDisplay() {
        System.out.println("\nCurrent value of the counter1 is " + this.getCurrentValue());
        System.out.println("Max value of the counter1 is " + this.getMaxValue());
        System.out.println("Min value of the counter1 is " + this.getMinValue());

        this.increaseValue();
        System.out.println("Current value of the counter1 after increasing: " + this.getCurrentValue());

        this.increaseValue();
        System.out.println("Current value of the counter1 after second increasing: " + this.getCurrentValue());

        this.decreaseValue();
        System.out.println("Current value of the counter1 after decreasing: " + this.getCurrentValue());

    }
}
