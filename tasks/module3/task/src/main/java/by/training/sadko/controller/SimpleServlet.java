package by.training.sadko.controller;

import by.training.sadko.AppConstants;
import by.training.sadko.ApplicationContext;
import by.training.sadko.command.AppCommand;
import by.training.sadko.command.AppCommandException;
import by.training.sadko.command.AppCommandProvider;
import by.training.sadko.command.SimpleAppCommandProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/", loadOnStartup = 1)
@MultipartConfig(fileSizeThreshold = 6291456,
        maxFileSize = 10485760L,
        maxRequestSize = 20971520L
)
public class SimpleServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(SimpleServlet.class);
    private static volatile AppCommandProvider commandProvider;

    @Override
    public void init() {
        commandProvider = ApplicationContext.getInstance().getBean(SimpleAppCommandProvider.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (req.getParameter(AppConstants.PARAM_COMMAND) == null) {

                req.setAttribute("viewName", "upload_view");
                req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
            } else {

                String viewName = executeCommandFromRequest(req, resp);
                if (viewName.startsWith("redirect:")) {
                    String redirect = viewName.replace("redirect:", "");
                    resp.sendRedirect(redirect);
                } else {
                    req.setAttribute("viewName", viewName);
                    req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
                }
            }

        } catch (ServletException e) {
            LOGGER.error("Servlet exception", e);
        } catch (IOException ioException) {
            LOGGER.error("JSP not found", ioException);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        doGet(req, resp);
    }

    private String executeCommandFromRequest(HttpServletRequest req, HttpServletResponse resp) {
        String commandFromRequest = req.getParameter(AppConstants.PARAM_COMMAND);
        LOGGER.info("Command from request is {}", commandFromRequest);

        AppCommand appCommand = commandProvider.getCommand(commandFromRequest);
        String viewName;
        try {
            viewName = appCommand.execute(req, resp);
            LOGGER.info("Result string from command is \"{}\"", viewName);

        } catch (AppCommandException e) {
            LOGGER.error("Unable to execute command \"{}\"", commandFromRequest);
            viewName = "not_found_view";
        } catch (Exception exception) {
            LOGGER.error("Unknown exception");
            viewName = "not_found_view";
        }
        return viewName;
    }
}
