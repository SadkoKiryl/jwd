package by.treining.ksa.oop.simple.task9;

/*
9. Создать класс Book, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и
метод toString(). Создать второй класс, агрегирующий массив типа Book, с подходящими конструкторами и
методами. Задать критерии выбора данных и вывести эти данные на консоль.
*/
/*
Book: id, название, автор(ы), издательство, год издания, количество страниц, цена, тип переплета.
        Найти и вывести:
        a) список книг заданного автора;
        b) список книг, выпущенных заданным издательством;
        c) список книг, выпущенных после заданного года.
*/

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book(1, "Treasure Island", "Stivenson",
                "Publish&Co", 1883, 40.25);
        Book book2 = new Book(2, "Island of the dead ships", "Belyaev",
                "Publish&Coooo", 1926, 45.00);
        Book book3 = new Book(3, "Twenty Thousand Leagues Under the Sea", "Jul Vern",
                "Publish&Co", 1869, 50.25);
        Book book4 = new Book(4, "The Children of Captain Grant", "Jul Vern",
                "Publish&Co", 1867, 40.25);
        Book book5 = new Book(5, "The Adventures of Tom Sawyer", "Mark Twain",
                "Publish&Co", 1876, 49.25);

        Book[] books = {book1, book2, book3, book4, book5};

        Library library = new Library(books);

        library.searchByAuthor("Jul Vern");
        System.out.println("--------------------------------");
        library.searchByPublishing("Publish&Co");
        System.out.println("--------------------------------");
        library.searchByYear(1869);
    }
}
