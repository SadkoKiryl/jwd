package by.treining.ksa.decomposition;

/*
3. Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел.
*/
public class Task_3 {

    public static void main(String[] args) {
        int num1 = 400;
        int num2 = 6;
        int num3 = 26;
        int num4 = 48;

        System.out.println("\nThe greatestCommonDivisor of numbers " + num1 + ", " + num2 + ", " + num3 + ", " + num4 +
                " is: " + greatestCommonDivisorForNumbers(num1, num2, num3, num4));
    }

    private static int greatestCommonDivisor(int num1, int num2) {
        return num2 == 0 ? num1 : greatestCommonDivisor(num2, num1 % num2);
    }

    private static int greatestCommonDivisorForNumbers(int num1, int num2, int num3, int num4) {
        int greatest1 = greatestCommonDivisor(num1, num2);
        int greatest2 = greatestCommonDivisor(num3, num4);
        return greatest2 == 0 ? greatest1 : greatestCommonDivisor(greatest2, greatest1 % greatest2);
    }
}
