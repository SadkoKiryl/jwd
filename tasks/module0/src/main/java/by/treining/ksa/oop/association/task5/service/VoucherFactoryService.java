package by.treining.ksa.oop.association.task5.service;

import by.treining.ksa.oop.association.task5.entity.*;

import java.util.ArrayList;
import java.util.List;

public class VoucherFactoryService implements VoucherFactory {

    private final List<Voucher> voucherList;

    public VoucherFactoryService() {
        this.voucherList = new ArrayList<>();
    }

    public VoucherFactoryService(List<Voucher> voucherList) {
        this.voucherList = voucherList;
    }

    @Override
    public List<Voucher> getVoucherList(VoucherType voucherType) {
        Direction[] directions = Direction.values();
        Transport[] transports = Transport.values();
        Food[] foods = Food.values();
        AmountOfDays[] amountOfDays = AmountOfDays.values();

        for (Direction direction : directions) {
            for (Transport transport : transports) {
                for (Food food : foods) {
                    for (AmountOfDays days : amountOfDays) {
                        voucherList.add(new Voucher(voucherType, direction, transport, food, days));
                    }
                }
            }
        }
        return voucherList;
    }

    @Override
    public List<Voucher> getVoucherList(VoucherType voucherType, Transport transportType) {
        Direction[] directions = Direction.values();
        Food[] foods = Food.values();
        AmountOfDays[] amountOfDays = AmountOfDays.values();

        for (Direction direction : directions) {
            for (Food food : foods) {
                for (AmountOfDays days : amountOfDays) {
                    voucherList.add(new Voucher(voucherType, direction, transportType, food, days));
                }
            }
        }
        return voucherList;
    }

    @Override
    public List<Voucher> getVoucherList(VoucherType voucherType, Transport transportType, Food foodType) {
        Direction[] directions = Direction.values();
        AmountOfDays[] amountOfDays = AmountOfDays.values();

        for (Direction direction : directions) {
            for (AmountOfDays days : amountOfDays) {
                voucherList.add(new Voucher(voucherType, direction, transportType, foodType, days));
            }
        }
        return voucherList;
    }

    @Override
    public List<Voucher> getVoucherList(VoucherType voucherType, Transport transportType, Food foodType, AmountOfDays amountOfDays) {
        Direction[] directions = Direction.values();

        for (Direction direction : directions) {
            voucherList.add(new Voucher(voucherType, direction, transportType, foodType, amountOfDays));
        }
        return voucherList;
    }

    public List<Voucher> getVoucherList() {
        return voucherList;
    }
}
