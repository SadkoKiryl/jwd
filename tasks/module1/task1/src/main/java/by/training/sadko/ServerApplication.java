package by.training.sadko;

import by.training.sadko.controller.SimpleHttpHandler;
import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.controller.command.AppCommandRegistry;
import by.training.sadko.controller.command.impl.*;
import by.training.sadko.dao.InMemoryLiteratureDao;
import by.training.sadko.dao.LiteratureDao;
import by.training.sadko.service.LiteratureService;
import by.training.sadko.service.SimpleLiteratureService;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static by.training.sadko.controller.command.impl.AppCommandName.*;

public class ServerApplication {

    private static final Logger LOGGER = LogManager.getLogger(ServerApplication.class.getName());

    public static void main(String[] args) throws IOException {
        //init server
        InetSocketAddress localhost = new InetSocketAddress(AppConstants.URL, AppConstants.PORT);
        HttpServer server = HttpServer.create(localhost, 0);

        //init DAO
        LiteratureDao literatureDao = new InMemoryLiteratureDao();

        //init service
        LiteratureService literatureService = new SimpleLiteratureService(literatureDao);

        //init commands
        AppCommand defaultCommand = new DefaultCommand(literatureService);
        AppCommand calculatePagesByGenreCommand = new CalculatePagesByGenreCommand(literatureService);
        AppCommand deleteLiteratureByIdCommand = new DeleteByIdCommand(literatureService);
        AppCommand addLiteratureCommand = new AddLiteratureCommand(literatureService);
        AppCommand sortCommand = new SortCommand(literatureService);
        AppCommand showCalculatorCommand = new ShowCalculatorCommand();
        AppCommand showInputFieldCommand = new ShowInputFieldCommand();
        AppCommand showSortFieldCommand = new ShowSortFieldCommand();

        //init commandRegistry
        Map<AppCommandName, AppCommand> commandsMap = new EnumMap<>(AppCommandName.class);
        commandsMap.put(CALCULATE_PAGES_BY_GENRE, calculatePagesByGenreCommand);
        commandsMap.put(DEFAULT, defaultCommand);
        commandsMap.put(DELETE_BY_ID, deleteLiteratureByIdCommand);
        commandsMap.put(SHOW_CALCULATOR, showCalculatorCommand);
        commandsMap.put(SHOW_INPUT_FIELD, showInputFieldCommand);
        commandsMap.put(ADD_LITERATURE, addLiteratureCommand);
        commandsMap.put(SHOW_SORT_FIELD, showSortFieldCommand);
        commandsMap.put(SORT_LITERATURE, sortCommand);
        AppCommandRegistry commandRegistry = new SimpleAppCommandRegistry(commandsMap);

        //init controllerLayer
        HttpHandler httpHandler = new SimpleHttpHandler(commandRegistry);

        server.createContext("/home", httpHandler);
        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();

        String serverAddress = server.getAddress().toString();
        LOGGER.info("Server started on {}", serverAddress);
    }
}
