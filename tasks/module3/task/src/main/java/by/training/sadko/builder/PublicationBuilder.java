package by.training.sadko.builder;

import by.training.sadko.entity.Publication;

public interface PublicationBuilder extends EntityBuilder<Publication> {

    void setID(String value);

    void setContentType(String value);

    void setTitle(String value);

    void setIsMonthly(String value);

    void setIsColored(String value);

    void setVolume(String value);

    void setSubscriptionIndex(String value);

    void setIsGlossy(String value);
}
