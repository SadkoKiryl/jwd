package by.treining.ksa.oop.basics.task2;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Payment {
    private Bill bill;

    public Payment(Product... products) {
        this.bill = new Bill(products);
    }

    public Payment() {
        this.bill = new Bill();
    }

    public void addProduct(Product... products) {
        for (Product product : products) {
            bill.productMap.put(product.getName(), product.getPrice());
            bill.totalCost = bill.totalCost.add(product.getPrice());
        }
    }

    public void pay() {
        System.out.println("Paid " + bill.totalCost + " BYN");
    }

    public void getBill() {
        int id = 1;
        for (Map.Entry<String, BigDecimal> set : bill.getProductMap().entrySet()) {
            System.out.println(id + ". " + set.getKey() + " - " + set.getValue() + " BYN");
            id++;
        }
        System.out.println("Total cost: " + bill.getTotalCost() + " BYN");
    }

    private class Bill {

        private BigDecimal totalCost;
        private Map<String, BigDecimal> productMap;

        private Bill() {
            this.productMap = new HashMap<>();
            this.totalCost = new BigDecimal(0);
        }

        private Bill(Product... products) {
            productMap = new HashMap<>();

            for (Product product : products) {
                productMap.put(product.getName(), product.getPrice());
                totalCost = totalCost.add(product.getPrice());
            }
        }

        public Map<String, BigDecimal> getProductMap() {
            return productMap;
        }

        public BigDecimal getTotalCost() {
            return totalCost;
        }
    }
}
