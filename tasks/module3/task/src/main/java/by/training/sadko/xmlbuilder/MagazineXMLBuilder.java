package by.training.sadko.xmlbuilder;

import by.training.sadko.entity.Magazine;
import by.training.sadko.entity.Publication;

public class MagazineXMLBuilder implements EntityStringBuilder<Publication> {

    @Override
    public String build(Publication publication) {
        Magazine magazine = (Magazine) publication;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<Magazine id=\"").append(magazine.getId()).append("\">\n")
                .append("<title>").append(magazine.getTitle()).append("</title>\n")
                .append("<monthly>").append(magazine.isMonthly()).append("</monthly>\n")
                .append("<colored>").append(magazine.isColored()).append("</colored>\n")
                .append("<content>").append(magazine.getContentType()).append("</content>\n")
                .append("<volume>").append(magazine.getVolume()).append("</volume>\n")
                .append("<type>").append(magazine.getType()).append("</type>\n")
                .append("<subscriptionIndex>").append(magazine.getSubscriptionIndex()).append("</subscriptionIndex>\n")
                .append("<glossy>").append(magazine.isGlossy()).append("</glossy>\n")
                .append("</Magazine>\n");

        return stringBuffer.toString();
    }
}
