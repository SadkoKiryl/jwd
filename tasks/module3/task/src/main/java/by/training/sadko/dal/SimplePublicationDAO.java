package by.training.sadko.dal;

import by.training.sadko.entity.Publication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class SimplePublicationDAO implements EntitiesDAO<Publication> {

    private static final Logger LOGGER = LogManager.getLogger(SimplePublicationDAO.class);
    private static volatile SimplePublicationDAO instance;
    private final List<Publication> publicationDAO = new ArrayList<>();

    private SimplePublicationDAO() {
    }

    public static SimplePublicationDAO getInstance() {
        SimplePublicationDAO result = instance;
        if (result != null) {
            return result;
        }
        synchronized (SimplePublicationDAO.class) {
            if (instance == null) {
                instance = new SimplePublicationDAO();
                LOGGER.info("Dao instance created");
            }
            return instance;
        }
    }

    public List<Publication> getEntitiesDAO() {
        return this.publicationDAO;
    }

    @Override
    public void addEntities(List<Publication> entities) {
        int daoSize = publicationDAO.size();
        entities.forEach(entity -> {
            if (!publicationDAO.contains(entity)) {
                publicationDAO.add(entity);
            }
        });
        if (publicationDAO.size() > daoSize) {
            LOGGER.info("Publications added to the DAO");
        } else {
            LOGGER.info("DAO already contains these publications");
        }
    }
}
