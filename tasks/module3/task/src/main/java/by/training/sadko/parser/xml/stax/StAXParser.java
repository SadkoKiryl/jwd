package by.training.sadko.parser.xml.stax;

import by.training.sadko.parser.xml.XMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StAXParser<T> extends XMLParser<T> {

    private static final Logger LOGGER = LogManager.getLogger(StAXParser.class);
    private final StAXHandler<T> staxHandler;

    public StAXParser(StAXHandler<T> staxHandler) {
        publicationsFromXML = new ArrayList<>();
        this.staxHandler = staxHandler;
    }

    @Override
    public List<T> parse(InputStream inputStream) {
        XMLInputFactory inputFactory = XMLInputFactory.newFactory();
        try {
            XMLEventReader xmlEventReader = inputFactory.createXMLEventReader(inputStream);
            publicationsFromXML = staxHandler.getListFromXML(xmlEventReader);

        } catch (XMLStreamException e) {
            LOGGER.error("Error of parser", e);
            return publicationsFromXML;
        }
        return publicationsFromXML;
    }
}
