package by.training.sadko.controller.command;

public interface AppCommandRegistry {

    AppCommand getCommand(String commandName);
}
