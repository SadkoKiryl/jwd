package by.treining.ksa.oop.association.task5.service;

import by.treining.ksa.oop.association.task5.entity.Voucher;

import java.util.Comparator;
import java.util.List;

public class VoucherSortingService implements VoucherSorting {

    @Override
    public List<Voucher> sortByCost(List<Voucher> voucherList) {

        voucherList.sort(Comparator.comparing(Voucher::getTotalPrice));
        return voucherList;
    }

    @Override
    public List<Voucher> sortByAmountOfDays(List<Voucher> voucherList) {

        voucherList.sort(Comparator.comparing(Voucher::getAmountOfDays));
        return voucherList;
    }
}
