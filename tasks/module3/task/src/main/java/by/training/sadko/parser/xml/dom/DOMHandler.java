package by.training.sadko.parser.xml.dom;

import org.w3c.dom.Document;

import java.util.List;

public abstract class DOMHandler<T> {

    protected List<T> entityFromXML;

    abstract List<T> getListFromXML(Document document);
}
