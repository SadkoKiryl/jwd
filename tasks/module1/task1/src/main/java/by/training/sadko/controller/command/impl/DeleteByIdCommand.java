package by.training.sadko.controller.command.impl;

import by.training.sadko.AppConstants;
import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.service.LiteratureService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class DeleteByIdCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(DeleteByIdCommand.class.getName());
    private final LiteratureService service;

    public DeleteByIdCommand(LiteratureService service) {
        this.service = service;
    }

    @Override
    public String execute(Map<String, String> requestMap) {
        String idString = requestMap.get(AppConstants.PARAM_ID);
        int id = Integer.parseInt(idString);
        service.deleteLiteratureById(id);
        LOGGER.info("Literature with id {} has been deleted", id);
        return null;
    }
}
