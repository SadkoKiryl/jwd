package by.training.sadko.parser.xml.dom;

import by.training.sadko.builder.PublicationBuilder;
import by.training.sadko.entity.Publication;
import by.training.sadko.entity.PublicationType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static by.training.sadko.AppConstants.*;

public class DOMPublicationHandler extends DOMHandler<Publication> {

    private static final Logger LOGGER = LogManager.getLogger(DOMPublicationHandler.class);
    private final Map<PublicationType, PublicationBuilder> builderMap;

    public DOMPublicationHandler(Map<PublicationType, PublicationBuilder> builderMap) {
        this.entityFromXML = new ArrayList<>();
        this.builderMap = builderMap;
    }

    @Override
    List<Publication> getListFromXML(Document document) {
        PublicationType[] values = PublicationType.values();
        for (PublicationType type : values) {
            Map<String, String> parametersMap;
            NodeList publicationElement = document.getDocumentElement().getElementsByTagName(type.getValue());

            if (publicationElement.getLength() != 0) {
                parametersMap = new HashMap<>();
                for (int i = 0; i < publicationElement.getLength(); i++) {
                    Node publicationNode = publicationElement.item(i);
                    if (publicationNode.getNodeType() == Node.ELEMENT_NODE) {
                        fillingMap(parametersMap, publicationNode);

                        PublicationBuilder publicationBuilder = builderMap.get(type);
                        Publication publication = constructPublication(parametersMap, publicationBuilder);
                        entityFromXML.add(publication);
                    }
                }
            } else {
                LOGGER.info("Elements {} not found in the XML", type.getValue());
            }
        }
        return entityFromXML;
    }

    private void fillingMap(Map<String, String> parametersMap, Node publicationNode) {
        NamedNodeMap attributes = publicationNode.getAttributes();
        String id = attributes.getNamedItem(FIELD_ID).getNodeValue();
        parametersMap.put(FIELD_ID, id);

        NodeList childNodes = publicationNode.getChildNodes();
        for (int j = 0; j < childNodes.getLength(); j++) {
            Node publicationField = childNodes.item(j);
            if (publicationField.getNodeType() == Node.ELEMENT_NODE) {
                String nodeValue = publicationField.getChildNodes().item(0).getNodeValue();
                parametersMap.put(publicationField.getNodeName(), nodeValue);
            }
        }
    }

    private Publication constructPublication(Map<String, String> parameterMap, PublicationBuilder publicationBuilder) {
        publicationBuilder.setID(parameterMap.get(FIELD_ID));
        publicationBuilder.setContentType(parameterMap.get(FIELD_CONTENT_TYPE));
        publicationBuilder.setTitle(parameterMap.get(FIELD_TITLE));
        publicationBuilder.setIsMonthly(parameterMap.get(FIELD_MONTHLY));
        publicationBuilder.setIsColored(parameterMap.get(FIELD_COLORED));
        publicationBuilder.setVolume(parameterMap.get(FIELD_VOLUME));
        publicationBuilder.setIsGlossy(parameterMap.get(FIELD_GLOSSY));
        publicationBuilder.setSubscriptionIndex(parameterMap.getOrDefault(FIELD_SUBSCRIPTION_INDEX, "0"));
        return publicationBuilder.getEntity();
    }
}
