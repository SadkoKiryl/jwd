package by.treining.ksa.decomposition;


import static java.lang.Math.*;

/*
1. Треугольник задан координатами своих вершин. Написать метод для вычисления его площади.
    1)поля: ax, ay, bx, by, cx, cy
    2)вычислить стороны треугольника
    3)вычислить площадь треугольника
*/
public class Task_1 {

    public static void main(String[] args) {
        int ax = -7;
        int ay = 1;
        int bx = -2;
        int by = 7;
        int cx = -3;
        int cy = 1;

        double sideAB = calculateSide(ax, ay, bx, by);
        double sideBC = calculateSide(bx, by, cx, cy);
        double sideCA = calculateSide(cx, cy, ax, ay);

        double area = calculateArea(sideAB, sideBC, sideCA);

        System.out.println("\nThe area of this triangle is: " + area);
    }

    private static double calculateSide(int x1, int y1, int x2, int y2) {
        double deltaX = abs(x1) - abs(x2);
        double deltaY = abs(y1) - abs(y2);
        double side = sqrt(pow(deltaX, 2) + pow(deltaY, 2));
        return side;
    }

    private static double calculateArea(double sideAB, double sideBC, double sideCA) {
        double s = (sideAB + sideBC + sideCA) / 2;
        return sqrt(s * (s - sideAB) * (s - sideBC) * (s - sideCA));
    }
}
