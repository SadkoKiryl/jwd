package by.treining.ksa.oop.basics.task2;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Product {

    BREAD("bread", new BigDecimal(1.25).setScale(2, RoundingMode.HALF_UP)),
    MEAT("pork", new BigDecimal(13.50).setScale(2, RoundingMode.HALF_UP)),
    FISH("dorada", new BigDecimal(18.30).setScale(2, RoundingMode.HALF_UP)),
    SOUR_CREAM("smetana", new BigDecimal(2.30).setScale(2, RoundingMode.HALF_UP)),
    ICE_CREAM("kashtan", new BigDecimal(1.90).setScale(2, RoundingMode.HALF_UP));

    private String name;
    private BigDecimal price;

    Product(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
