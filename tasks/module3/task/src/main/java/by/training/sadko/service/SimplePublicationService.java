package by.training.sadko.service;

import by.training.sadko.dal.EntitiesDAO;
import by.training.sadko.entity.Publication;

import java.util.List;

public class SimplePublicationService implements EntitiesService<Publication> {

    private final EntitiesDAO<Publication> entitiesDAO;

    public SimplePublicationService(EntitiesDAO<Publication> entitiesDAO) {
        this.entitiesDAO = entitiesDAO;
    }

    @Override
    public List<Publication> getEntitiesList() {
        return entitiesDAO.getEntitiesDAO();
    }

    @Override
    public void addEntities(List<Publication> entities) {
        entitiesDAO.addEntities(entities);
    }
}
