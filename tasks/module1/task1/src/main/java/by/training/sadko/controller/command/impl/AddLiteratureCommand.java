package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.controller.command.validator.LiteratureOptionsValidator;
import by.training.sadko.controller.command.validator.PathFileValidator;
import by.training.sadko.controller.command.validator.Validator;
import by.training.sadko.entity.Author;
import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;
import by.training.sadko.entity.PublicationType;
import by.training.sadko.service.LiteratureService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.AppConstants.PATH;

public class AddLiteratureCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(AddLiteratureCommand.class.getName());
    private final LiteratureService service;
    private final Validator<String> pathFileValidator = new PathFileValidator();
    private final Validator<String[]> stringValidator = new LiteratureOptionsValidator();

    public AddLiteratureCommand(LiteratureService service) {
        this.service = service;
    }

    @Override
    public String execute(Map<String, String> requestMap) {
        StringBuilder responseString = new StringBuilder("<p align=\"center\">");

        String pathToFile = requestMap.get(PATH);
        if (pathFileValidator.isValid(pathToFile)) {
            LOGGER.info("The file located by path: {}, exists", pathToFile);

            List<String> wrongStrings = new ArrayList<>();
            List<Literature> literatureForAddition = new ArrayList<>();

            Path path = Paths.get(pathToFile);
            try (BufferedReader reader = Files.newBufferedReader(path)) {
                String optionsLine;
                while ((optionsLine = reader.readLine()) != null) {
                    String[] literatureOptions = optionsLine.split(",");
                    if (stringValidator.isValid(literatureOptions)) {
                        Literature literature = getNewLiterature(literatureOptions);
                        literatureForAddition.add(literature);

                    } else {
                        wrongStrings.add(optionsLine);
                    }
                }
            } catch (IOException e) {
                LOGGER.error("Wrong path to file");
            }

            service.addLiterature(literatureForAddition);
            responseString
                    .append("<b>")
                    .append(literatureForAddition.size())
                    .append(" new literature added to the library</b>")
                    .append("</p>");

            if (!wrongStrings.isEmpty()) {
                appendWrongStringResponse(responseString, wrongStrings);
            }

        } else {
            LOGGER.info("The file does not exist or it's extension is not supported");
            responseString
                    .append("The file does not exist or it's extension is not supported. Try again")
                    .append("</p>")
                    .append("<form action=\"/home\" method=\"POST\" align=\"center\">\n")
                    .append("  <label for=" + PATH + ">Enter file path:" + "</label><br>\n")
                    .append("  <input type=\"text\"  name=" + PATH + " value=\"\">\n")
                    .append("  <input type=\"hidden\"  name=" + COMMAND_NAME + " value=\"add\">\n")
                    .append("  <input type=\"submit\" value=\"Add\">\n")
                    .append("</form> ");
        }

        return responseString.toString();
    }

    private Literature getNewLiterature(String[] literatureOptions) {
        Genre genre = Genre.fromString(literatureOptions[0]);
        String name = literatureOptions[1];
        int numberOfPages = Integer.parseInt(literatureOptions[2]);
        PublicationType publication = PublicationType.fromString(literatureOptions[3]);
        Author author = new Author(literatureOptions[4]);
        return new Literature(genre, name, numberOfPages, publication, author);
    }

    private void appendWrongStringResponse(StringBuilder responseString, List<String> wrongStrings) {
        responseString.append("<p align=\"center\"><b>Next lines contains error:</b></p>");
        for (String tempStr : wrongStrings) {
            responseString
                    .append("<li>")
                    .append(tempStr)
                    .append("</li>");
        }
        responseString.append("</p>");
    }
}
