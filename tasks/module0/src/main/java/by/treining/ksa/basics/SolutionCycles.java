package by.treining.ksa.basics;

import java.util.Scanner;

import static java.lang.Math.pow;

public class SolutionCycles {

    public static void main(String[] args) {

        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
        task11();
        task12();
        task13();
        task14();
        task15();
        task16();
        task17();
        task25();
        task26();
        task27();
        task29();
        task33();
    }

    private static void task1() {

        System.out.println("\n1. Необходимо вывести на экран числа от 1 до 5.");

        for (int i = 1; i < 6; i++) {
            System.out.println(i);
        }
    }

    private static void task2() {

        System.out.println("\n\n2. Необходимо вывести на экран числа от 5 до 1.");

        for (int i = 5; i > 0; i--) {
            System.out.println(i);
        }
    }

    private static void task3() {

        System.out.println("\n\n3. Необходимо вывести на экран таблицу умножения на 3:\n");

        for (int i = 1; i < 11; i++) {
            System.out.println(i + " x 3 = " + i * 3);
        }
    }

    private static void task4() {

        System.out.println("\n\n4. С помощью оператора while напишите программу вывода всех четных чисел в диапазоне от 2 до 100 включительно.");
        int i = 2;

        while (i <= 100) {
            System.out.print("\t" + i);
            i += 2;
        }
    }

    private static void task5() {

        System.out.println("\n\n5. С помощью оператора while напишите программу определения суммы всех нечетных чисел в диапазоне от 1 до 99 включительно.");
        int i = 1;
        int count = 0;

        while (i <= 99) {
            count += i;
            i += 2;
        }
        System.out.println("The sum of odd numbers is " + count);
    }

    private static void task6() {

        System.out.println("\n\n6. Напишите программу, где пользователь вводит любое целое положительное число. А программа суммирует все числа от 1 до введенного пользователем числа.");
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a positive integer: ");
        int number = scan.nextInt();
        int i = 1;
        int count = 0;

        while (number >= i) {
            count += i;
            i++;
        }
        System.out.println("The sum of numbers is " + count);
    }

    private static void task7() {

        System.out.println("\n\n7. Вычислить значения функции на отрезке [а,b] c шагом h: см. задание.");
        int a = 14;
        int b = 2;
        int h = 6;
        int y;

        for (int i = a; i <= b; i += h) {
            if (i > 2) {
                y = i;
            } else {
                y = i * (-1);
            }
            System.out.println("The function's value is " + y);
        }
    }

    private static void task8() {

        System.out.println("\n\nВычислить значения функции на отрезке [а,b] c шагом h:");
        int a = 14;
        int b = 2;
        int h = 6;
        int c = -7;
        int y;

        for (int i = a; i <= b; i += h) {
            if (i == 15) {
                y = (i + c) * 2;
            } else {
                y = (i - c) + 6;
            }
            System.out.println("The function's value is " + y);
        }
    }

    private static void task9() {

        System.out.println("\n\nНайти сумму квадратов первых ста чисел.");
        int count = 0;

        for (int i = 1; i <= 100; i++) {
            count += pow(i, 2);
        }
        System.out.println("The sum of numbers squares is " + count);
    }

    private static void task10() {

        System.out.println("\n\n10. Составить программу нахождения произведения квадратов первых двухсот чисел.");
        int count = 0;

        for (int i = 1; i <= 200; i++) {
            count *= pow(i, 2);
        }
        System.out.println("The multiply of numbers squares is " + count);
    }

    private static void task11() {

        System.out.println("\n\n11. Составить программу нахождения разности кубов первых двухсот чисел");
        int count = 0;

        for (int i = 1; i <= 200; i++) {
            count -= pow(i, 3);
        }
        System.out.println("The difference of numbers cubes is " + count);
    }

    private static void task12() {

        System.out.println("\n\n12. Последовательность аn строится так: а1 = 1, an =6+ аn-1 , для каждого n >1 Составьте программу нахождения произведения первых 10 членов этой последовательности.");
        int multiply = 1;
        long an = 1L;

        for (int n = 2; n < 11; n++) {
            an = 6 + an;
            multiply *= an;
        }
        System.out.println("The result is " + multiply);
    }

    private static void task13() {

        System.out.println("\n\n13. Составить таблицу значений функции y = 5 - x2/2 на отрезке [-5; 5] с шагом 0.5.");
        int a = -5;
        int b = 5;
        double h = 0.5;
        double y;

        for (double i = a; i <= b; i += h) {
            y = 5 - (i * i) / 2.0;
            System.out.println("x = " + i + " y = " + y);
        }
    }

    private static void task14() {

        System.out.println("\n\n14. Дано натуральное n. вычислить: 1 + 1/2 + 1/3 + 1/4 + ... + 1/n.");
        int n = 4;
        double sum = 0;

        for (double i = 1; i <= n; i++) {
            sum += 1 / i;
        }

        System.out.println("When n = " + n + " the sum is " + sum);
    }

    private static void task15() {

        System.out.println("\n\n15. Вычислить : 1+2+4+8+...+ 2 в 10 степени.");
        long sum = 0L;

        for (int i = 0; i < 11; i++) {
            sum += pow(2, i);
        }
        System.out.println("The sum is " + sum);
    }

    private static void task16() {

        System.out.println("\n\n16. Вычислить: (1+2)*(1+2+3)*...*(1+2+...+10).");
        int sum = 3;
        long composition = sum;

        for (int i = 3; i <= 10; i++) {
            sum += i;
            composition *= sum;
        }
        System.out.println("The composition is " + composition);
    }

    private static void task17() {

        System.out.println("\n\n17. Даны действительное (а) и натуральное (n). вычислить: a(a+1)...(a+n-1)");
        int n = 5;
        double a = 12.7;
        double composition = 1;

        for (int i = 0; i < n; i++) {
            composition *= a + i;
        }
        System.out.println("The composition is " + composition);
    }

    private static void task25() {

        System.out.println("\n\n18. Даны числовой ряд и некоторое число е. Найти сумму тех членов ряда, модуль которых больше или равен заданному е. Общий член ряда имеет вид: см. задание");
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter a number to calculate the factorial: ");
        int number = scan.nextInt();
        long factorial = 1;

        if (number == 0) {
            factorial = 1;
        } else {
            for (int i = 1; i <= number; i++) {
                factorial *= i;
            }
        }
        System.out.println("The factorial of number " + number + " is " + factorial);
    }

    private static void task26() {

        System.out.println("\n\n26. Вывести на экран соответствий между символами и их численными обозначениями в памяти компьютера (Таблицу ASCII).");
        for (int i = 32; i <= 126; i++) {
            System.out.println(i + " " + (char) i);
        }
    }

    private static void task27() {

        System.out.println("\n\n27. Для каждого натурального числа в промежутке от m до n вывести все делители, кроме единицы и самого числа. m и n вводятся с клавиатуры.");
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the m:");
        int m = scan.nextInt();
        System.out.println("Enter the n:");
        int n = scan.nextInt();
        int divisor;

        for (int i = m; i <= n; i++) {

            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    divisor = j;
                    System.out.println("The divisor of " + i + " is " + divisor);
                }
            }
        }
    }

    private static void task29() {

        System.out.println("\n\n29. Даны два числа. Определить цифры, входящие в запись как первого так и второго числа.");
        int a = 123;
        int b = -341;

        char[] charsA = Integer.toString(a).toCharArray();
        char[] charsB = Integer.toString(b).toCharArray();

        for (char c : charsA) {
            for (char d : charsB) {
                if (c == d) {
                    System.out.print(d + " ");
                }
            }
        }
    }

    private static void task33() {

        System.out.println("\n\n33. Найдите наибольшую цифру данного натурального числа.");
        int a = 129450;
        char[] numbers = String.valueOf(a).toCharArray();

        char maxDigit = 0;
        for (int i = 0; i < numbers.length - 1; i++) {
            int x = Character.getNumericValue(numbers[i]);

            if (numbers[i] > maxDigit) {
                maxDigit = numbers[i];
            }
        }
        System.out.println(maxDigit);
    }
}
