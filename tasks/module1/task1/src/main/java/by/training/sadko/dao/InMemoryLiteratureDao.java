package by.training.sadko.dao;

import by.training.sadko.entity.Author;
import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;
import by.training.sadko.entity.PublicationType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InMemoryLiteratureDao implements LiteratureDao {

    private static final Logger LOGGER = LogManager.getLogger(InMemoryLiteratureDao.class.getName());
    private final List<Literature> libraryRepository;

    public InMemoryLiteratureDao() {
        LOGGER.info("Initializing InMemoryDao...");

        Literature lit1 = new Literature(Genre.FANTASY, "Hobbit", 1300, PublicationType.BOOK,
                new Author("Tolkien"));
        Literature lit2 = new Literature(Genre.FANTASY, "Air seller", 700, PublicationType.BOOK,
                new Author("Belyaev"));
        Literature lit3 = new Literature(Genre.BUSINESS, "Theory of Constraints", 239, PublicationType.BOOK,
                new Author("Goldrat"));
        Literature lit4 = new Literature(Genre.BUSINESS, "Forbes 02.2019", 35, PublicationType.MAGAZINE,
                new Author("The forbes"));
        Literature lit5 = new Literature(Genre.FANTASY, "A Wizard of Earthsea", 300, PublicationType.BOOK,
                new Author("Le Guin"));
        Literature lit6 = new Literature(Genre.NOVEL, "Alice in Wonderland", 700, PublicationType.BOOK,
                new Author("Lewis Carroll"));
        Literature lit7 = new Literature(Genre.NOVEL, "Atlas Shrugged", 1260, PublicationType.BOOK,
                new Author("Ayn Rand"));
        Literature lit8 = new Literature(Genre.SCIENTIFIC, "Feynman Lectures on Physics", 2670, PublicationType.BOOK,
                new Author("Feynman"));
        Literature lit9 = new Literature(Genre.SCIENTIFIC, "Popular Mechanics", 93, PublicationType.MAGAZINE,
                new Author("Hearst Communications"));
        Literature lit10 = new Literature(Genre.FANTASY, "Whither", 1800, PublicationType.BOOK,
                new Author("Sapkowsky"));

        List<Literature> tempList = Arrays.asList(lit1, lit2, lit3, lit4, lit5, lit6, lit7, lit8, lit9, lit10);
        this.libraryRepository = new ArrayList<>(tempList);
        LOGGER.info("InMemoryDao initialized");
    }

    @Override
    public List<Literature> getAllLiterature() {
        return libraryRepository;
    }

    @Override
    public void removeLiteratureById(int id) {
        for (Literature tempLiterature : libraryRepository) {
            if (tempLiterature.getId() == id) {
                libraryRepository.remove(tempLiterature);
                LOGGER.debug("Literature removed by id={}", id);
                break;
            }
        }
    }

    @Override
    public List<Literature> selectionLiteratureByGenre(Genre genre) {
        List<Literature> literatureByGenre = new ArrayList<>();

        for (Literature tempLiterature : libraryRepository) {
            if (tempLiterature.getGenre().equals(genre)) {
                literatureByGenre.add(tempLiterature);
            }
        }
        LOGGER.debug("Selection by genre completed");
        return literatureByGenre;
    }

    @Override
    public void addLiterature(List<Literature> literature) {
        for (Literature tempLit : literature) {
            if (!libraryRepository.contains(tempLit)) {
                libraryRepository.add(tempLit);
                LOGGER.debug("Literature with id={} added to the repository", tempLit.getId());
            }
        }
    }

    @Override
    public void sort(LiteratureComparator... comparators) {
        libraryRepository.sort(LiteratureComparator.getComparator(comparators));
        LOGGER.debug("The repository sorted");
    }
}
