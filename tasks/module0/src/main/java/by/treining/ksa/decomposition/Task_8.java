package by.treining.ksa.decomposition;


/*
8. Составить программу, которая в массиве A[N] находит второе по величине число (вывести на печать число,
        которое меньше максимального элемента массива, но больше всех других элементов).
*/
public class Task_8 {

    static int[] array;

    public static void main(String[] args) {

        createArray(5);
        System.out.println("\nThe second maximum element in the array is: " + findSecondLargestNumber(array));

    }

    private static int findSecondLargestNumber(int[] array) {

        boolean isSorted = false;

        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    isSorted = false;
                }
            }

        }


        return array[1];
    }

    private static void createArray(int n) {
        array = new int[n];

        System.out.print("The array is: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
            System.out.print(array[i] + " ");
        }
    }
}
