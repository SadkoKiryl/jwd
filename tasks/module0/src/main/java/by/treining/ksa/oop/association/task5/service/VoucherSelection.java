package by.treining.ksa.oop.association.task5.service;

import by.treining.ksa.oop.association.task5.entity.Voucher;

import java.util.List;

public interface VoucherSelection {
    Voucher getRequiredVoucher(int id, List<Voucher> voucherList);
}
