package by.training.sadko.xmlbuilder;

public interface EntityStringBuilder<T> {

    String build(T t);
}
