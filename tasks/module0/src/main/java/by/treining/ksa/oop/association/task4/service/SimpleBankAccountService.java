package by.treining.ksa.oop.association.task4.service;

import by.treining.ksa.oop.association.task4.model.BankAccount;
import by.treining.ksa.oop.association.task4.model.Customer;

import java.util.Comparator;
import java.util.List;

public class SimpleBankAccountService implements BankAccountService {
    @Override
    public void accountLock(Customer customer, int accountNumber) {
        for (BankAccount bankAccount : customer.getBankAccountList()) {
            if (bankAccount.getAccountNumber() == accountNumber) {
                bankAccount.setBlocked(true);
                System.out.println("The bank account number " + accountNumber + " locked.");
                break;
            }
        }
    }

    @Override
    public void accountUnlock(Customer customer, int accountNumber) {
        for (BankAccount bankAccount : customer.getBankAccountList()) {
            if (bankAccount.getAccountNumber() == accountNumber) {
                bankAccount.setBlocked(false);
                System.out.println("The bank account number " + accountNumber + " unlocked.");
                break;
            }
        }
    }

    @Override
    public void searchAccount(Customer customer, int accountNumber) {
        for (BankAccount bankAccount : customer.getBankAccountList()) {
            if (bankAccount.getAccountNumber() == accountNumber) {
                System.out.println("The bank account number " + accountNumber + " found");
                System.out.println("The balance is " + bankAccount.getAccountBalance());
            }
        }
    }

    @Override
    public List<BankAccount> sortingAccount(Customer customer) {
        List<BankAccount> bankAccountsList = customer.getBankAccountList();
        bankAccountsList.sort(Comparator.comparing(BankAccount::getAccountBalance).thenComparing(BankAccount::getAccountNumber));
        return bankAccountsList;
    }

    @Override
    public double overallBalance(List<Customer> customerList) {
        double overallBalance = 0;
        for (Customer customer : customerList) {

            for (BankAccount bankAccount : customer.getBankAccountList()) {
                overallBalance += bankAccount.getAccountBalance();
            }
        }
        return overallBalance;
    }

    @Override
    public double positiveBalance(List<Customer> customerList) {
        double positiveBalance = 0;
        for (Customer customer : customerList) {

            for (BankAccount bankAccount : customer.getBankAccountList()) {
                if (bankAccount.getAccountBalance() > 0) {
                    positiveBalance += bankAccount.getAccountBalance();
                }
            }
        }
        return positiveBalance;
    }

    @Override
    public double negativeBalance(List<Customer> customerList) {
        double negativeBalance = 0;
        for (Customer customer : customerList) {

            for (BankAccount bankAccount : customer.getBankAccountList()) {
                if (bankAccount.getAccountBalance() < 0) {
                    negativeBalance += bankAccount.getAccountBalance();
                }
            }
        }
        return negativeBalance;
    }
}
