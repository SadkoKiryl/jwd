package by.treining.ksa.oop.basics.task1.model;

import java.util.ArrayList;
import java.util.List;

public class Directory {
    private String directoryName;
    private List<File> fileList;

    public Directory(String directoryName, ArrayList<File> fileList) {
        this.directoryName = directoryName;
        this.fileList = fileList;
    }

    public Directory(String directoryName) {
        this.directoryName = directoryName;
        this.fileList = new ArrayList<>();
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public List<File> getFileList() {
        return fileList;
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
    }
}
