package by.treining.ksa.oop.association.task5.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Food {

    RO("room only", new BigDecimal(0).setScale(2, RoundingMode.HALF_UP)),
    BB("bed & breakfast", new BigDecimal(5).setScale(2, RoundingMode.HALF_UP)),
    HB("half board", new BigDecimal(12).setScale(2, RoundingMode.HALF_UP)),
    FB("full board", new BigDecimal(17).setScale(2, RoundingMode.HALF_UP));

    private String foodType;
    private BigDecimal costPerDay;

    Food(String foodType, BigDecimal costPerDay) {
        this.foodType = foodType;
        this.costPerDay = costPerDay;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public BigDecimal getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(BigDecimal costPerDay) {
        this.costPerDay = costPerDay;
    }
}
