package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.dao.LiteratureComparator;
import by.training.sadko.service.LiteratureService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.AppConstants.SORTING_OPTIONS;

public class SortCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(SortCommand.class.getName());
    private final LiteratureService service;

    public SortCommand(LiteratureService service) {
        this.service = service;
    }

    @Override
    public String execute(Map<String, String> requestMap) {
        String stringOfOptions = requestMap.get(SORTING_OPTIONS);
        String[] inputOptions = stringOfOptions.split(",");
        LiteratureComparator[] values = LiteratureComparator.values();

        List<LiteratureComparator> currentOptions = new ArrayList<>();
        for (String inputData : inputOptions) {
            for (LiteratureComparator comparator : values) {
                if (comparator.getName().equalsIgnoreCase(inputData)
                        || comparator.name().equalsIgnoreCase(inputData)) {
                    currentOptions.add(comparator);
                    break;
                }
            }
        }

        return getResponseString(currentOptions);
    }

    private String getResponseString(List<LiteratureComparator> currentOptions) {
        String responseView;
        if (currentOptions.isEmpty()) {
            responseView = "<p align=\"center\">There are no such sorting options. Try again:</p>" +
                    "<form action=\"/home\" method=\"GET\" align=\"center\" accept-charset=\"utf-8\">\n" +
                    "<label for=" + SORTING_OPTIONS + ">Enter comma-separated sort options. " +
                    "For example: id,genre,pages,author,name,publication." + "</label><br>\n" +
                    "<input type=\"text\"  name=" + SORTING_OPTIONS + " value=\"\">\n" +
                    "<input type=\"hidden\"  name=" + COMMAND_NAME + " value=\"sort\">\n" +
                    "<br>\n\n" +
                    "<input type=\"submit\" value=\"Sort\">\n" +
                    "</form> ";
        } else {
            LiteratureComparator[] optionsArray = currentOptions
                    .toArray(new LiteratureComparator[currentOptions.size()]);

            service.sortLiterature(optionsArray);

            StringBuilder comparatorsString = new StringBuilder();
            currentOptions.forEach(literatureComparator -> comparatorsString
                    .append(literatureComparator.getName())
                    .append(" "));
            LOGGER.info("Library repository sorted by {}", comparatorsString);

            responseView = "<p align=\"center\"><b>Library repository sorted by " +
                    comparatorsString.toString() +
                    "</b></p>";
        }
        return responseView;
    }
}
