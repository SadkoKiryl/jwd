package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;

import java.util.Map;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.AppConstants.PARAM_GENRE;

public class ShowCalculatorCommand implements AppCommand {

    @Override
    public String execute(Map<String, String> requestMap) {
        return "<form action=\"/home\" align=\"center\">\n" +
                "<label for=" + PARAM_GENRE + ">Enter the genre to calculate number of pages by genre:" +
                "</label><br>\n" +
                "<input type=\"text\"  name=" + PARAM_GENRE + " value=\"\">\n" +
                "<input type=\"hidden\"  name=" + COMMAND_NAME + " value=\"calc\">\n" +
                "<br>\n\n" + "  <input type=\"submit\" value=\"Calculate\">\n" +
                "</form> ";
    }
}
