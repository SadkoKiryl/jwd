package by.treining.ksa.decomposition;

/*
11. Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].
        Пояснение. Составить метод(методы) для вычисления суммы трех последовательно расположенных элементов
        массива с номерами от k до m.
*/
public class Task_11 {

    private static int[] array;

    public static void main(String[] args) {
        createArray(7);
        int sum = findSum(array, 2);

        System.out.println("\n\nThe sum from k to m is " + sum);
    }

    private static int findSum(int[] array, int k) {
        int m = k + 3;
        int sum = 0;
        for (int i = k; i < m; i++) {
            sum += array[i];
        }
        return sum;
    }

    private static void createArray(int n) {
        array = new int[n];

        System.out.print("The array is: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 10);
            System.out.print(array[i] + " ");
        }
    }
}
