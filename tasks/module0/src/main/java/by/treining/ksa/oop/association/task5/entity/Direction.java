package by.treining.ksa.oop.association.task5.entity;

public enum Direction {
    ITALY("Italy", 2336),
    SPAIN("Spain", 3453),
    FRANCE("France", 2386),
    AUSTRIA("Austria", 1428),
    INDONESIA("Indonesia", 9834),
    INDIA("India", 6763);

    private String directionType;
    private int distance;

    Direction(String directionType, int distance) {
        this.directionType = directionType;
        this.distance = distance;
    }

    public String getDirectionType() {
        return directionType;
    }

    public void setDirectionType(String directionType) {
        this.directionType = directionType;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

}
