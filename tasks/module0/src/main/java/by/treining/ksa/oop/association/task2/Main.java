package by.treining.ksa.oop.association.task2;

/*
2. Создать объект класса Автомобиль, используя классы Колесо, Двигатель. Методы: ехать, заправляться,
менять колесо, вывести на консоль марку автомобиля.
*/

import by.treining.ksa.oop.association.task2.model.Car;
import by.treining.ksa.oop.association.task2.model.GasEngine;
import by.treining.ksa.oop.association.task2.model.Wheel;
import by.treining.ksa.oop.association.task2.service.CarService;
import by.treining.ksa.oop.association.task2.service.SimpleCarService;

public class Main {
    public static void main(String[] args) {
        Wheel wheel = new Wheel();
        GasEngine gasEngine = new GasEngine(50, 5);
        String modelCar = "Skoda";
        Car car = new Car(wheel, gasEngine, modelCar);

        CarService carService = new SimpleCarService();
        carService.getModelCar(car);
        carService.go(car);
    }
}
