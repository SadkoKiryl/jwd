package by.training.sadko.entity;

public class Booklet extends Publication {

    private boolean isGlossy;

    public Booklet() {
        super.type = PublicationType.BOOKLET.getValue();
    }

    public boolean isGlossy() {
        return isGlossy;
    }

    public void setGlossy(boolean glossy) {
        isGlossy = glossy;
    }

    @Override
    public String toString() {
        return "Booklet{" +
                "id='" + id + '\'' +
                ", publicationType=" + type +
                ", contentType=" + contentType +
                ", title='" + title + '\'' +
                ", isMonthly=" + isMonthly +
                ", isColored=" + isColored +
                ", volume=" + volume +
                ", isGlossy=" + isGlossy +
                "}";
    }
}
