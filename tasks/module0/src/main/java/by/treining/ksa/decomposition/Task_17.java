package by.treining.ksa.decomposition;

import static java.lang.Math.pow;

/*
17. Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его цифр, возведенная
        в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. Для решения задачи использовать
        декомпозицию.
*/
public class Task_17 {

    public static void main(String[] args) {
        int k = 1000;
        findArmstrongNumbers(k);
    }

    private static void findArmstrongNumbers(int k) {
        System.out.println("\nThe numbers of Armstrong in this interval [1, " + k + "]:");

        for (int i = 1; i <= k; i++) {
            String temp = String.valueOf(i);
            String[] tempArray = temp.split("");

            int[] arrayOfIntegers = new int[tempArray.length];
            int sumOfDigitsInPower = 0;

            for (int j = 0; j < arrayOfIntegers.length; j++) {
                arrayOfIntegers[j] = Integer.parseInt(tempArray[j]);

                sumOfDigitsInPower += pow(arrayOfIntegers[j], arrayOfIntegers.length);

                if (i == sumOfDigitsInPower) {
                    System.out.print(i + " ");
                }
            }
        }
    }
}
