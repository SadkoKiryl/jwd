package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;

import java.util.Map;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.AppConstants.SORTING_OPTIONS;

public class ShowSortFieldCommand implements AppCommand {

    @Override
    public String execute(Map<String, String> requestMap) {
        return "<form action=\"/home\" method=\"GET\" align=\"center\" accept-charset=\"utf-8\">\n" +
                "<label for=" + SORTING_OPTIONS + ">Enter comma-separated sort options. " +
                "For example: id,genre,pages,author,name,publication." + "</label><br>\n" +
                "<input type=\"text\"  name=" + SORTING_OPTIONS + " value=\"\">\n" +
                "<input type=\"hidden\"  name=" + COMMAND_NAME + " value=\"sort\">\n" +
                "<br>\n\n" +
                "<input type=\"submit\" value=\"Sort\">\n" +
                "</form> ";
    }
}
