package by.treining.ksa.oop.association.task1.service;

import by.treining.ksa.oop.association.task1.model.Sentence;

public interface TextService {
    public void addingText(Sentence sentence);

    public void printingText();

    public void headingText(String heading);
}
