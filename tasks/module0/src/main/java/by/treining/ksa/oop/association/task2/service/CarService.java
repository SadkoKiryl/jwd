package by.treining.ksa.oop.association.task2.service;

import by.treining.ksa.oop.association.task2.model.Car;

public interface CarService {
    void go(Car car);

    void fillingCar(Car car);

    void wheelRepayment(Car car);

    void getModelCar(Car car);
}
