package by.training.sadko.service;

import by.training.sadko.dao.LiteratureComparator;
import by.training.sadko.dao.LiteratureDao;
import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class SimpleLiteratureService implements by.training.sadko.service.LiteratureService {

    private static final Logger LOGGER = LogManager.getLogger(SimpleLiteratureService.class.getName());
    private final LiteratureDao literatureDao;

    public SimpleLiteratureService(LiteratureDao literatureDao) {
        this.literatureDao = literatureDao;
    }

    @Override
    public long calculatingPagesByGenre(Genre genre) {
        List<Literature> literatureByGenre = literatureDao.selectionLiteratureByGenre(genre);

        long sumOfPages = 0L;
        for (Literature tempLiterature : literatureByGenre) {
            sumOfPages += tempLiterature.getNumberOfPages();
        }
        LOGGER.debug("Sum of pages by genre calculated");
        return sumOfPages;
    }

    @Override
    public void deleteLiteratureById(int id) {
        literatureDao.removeLiteratureById(id);
        LOGGER.debug("Literature with id={} deleted", id);
    }

    @Override
    public List<Literature> getAllLiterature() {
        return literatureDao.getAllLiterature();
    }

    @Override
    public List<Literature> selectionLiteratureByGenre(Genre genre) {
        return literatureDao.selectionLiteratureByGenre(genre);
    }

    @Override
    public void addLiterature(List<Literature> literature) {
        literatureDao.addLiterature(literature);
    }

    @Override
    public void sortLiterature(LiteratureComparator... comparators) {
        literatureDao.sort(comparators);
    }
}
