package by.treining.ksa.oop.association.task3.service;

/*
Методы: вывести на консоль столицу, количество областей, площадь, областные центры.
*/

import by.treining.ksa.oop.association.task3.model.State;

public interface StateService {
    void printingCapital(State state);

    void printingRegionCount(State state);

    void printingStateArea(State state);

    void printingRegionCenterList(State state);
}
