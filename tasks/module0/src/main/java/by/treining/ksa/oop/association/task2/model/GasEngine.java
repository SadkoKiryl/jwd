package by.treining.ksa.oop.association.task2.model;

public class GasEngine {
    private boolean isRunning;
    private int tankVolume;
    private int currentVolume;
    private boolean isRefueled;

    public GasEngine(int tankVolume, int currentVolume) {
        this.tankVolume = tankVolume;
        this.currentVolume = currentVolume;
        this.isRunning = false;
        this.isRefueled = tankVolume <= currentVolume;
    }

    public boolean isRefueled() {
        return isRefueled;
    }

    public void setRefueled(boolean refueled) {
        isRefueled = refueled;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public int getTankVolume() {
        return tankVolume;
    }

    public void setTankVolume(int tankVolume) {
        this.tankVolume = tankVolume;
    }

    public int getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(int currentVolume) {
        this.currentVolume = currentVolume;
    }
}
