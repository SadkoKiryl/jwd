package by.treining.ksa.decomposition;

/*
4. Написать метод(методы) для нахождения наименьшего общего кратного трех натуральных чисел.
*/
public class Task_4 {

    public static void main(String[] args) {
        int num1 = 24;
        int num2 = 28;
        int num3 = 16;

        System.out.println("\nThe least common multiple of numbers " + num1 + ", " + num2 + ", " + num3 + " is " +
                leastCommonMultipleThree(num1, num2, num3));

    }

    private static int greatestCommonDivisor(int num1, int num2) {
        return num2 == 0 ? num1 : greatestCommonDivisor(num2, num1 % num2);
    }

    private static int leastCommonMultiple(int num1, int num2) {
        return num1 * num2 / greatestCommonDivisor(num1, num2);
    }

    private static int leastCommonMultipleThree(int num1, int num2, int num3) {
        int lCM1 = leastCommonMultiple(num1, num2);
        int lCM2 = leastCommonMultiple(lCM1, num3);
        return lCM2;
    }
}
