package by.treining.ksa.oop.association.task1.model;

import java.util.List;

public class Sentence {

    private List<Word> wordList;

    public Sentence(List<Word> words) {
        this.wordList = words;
    }

    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }
}
