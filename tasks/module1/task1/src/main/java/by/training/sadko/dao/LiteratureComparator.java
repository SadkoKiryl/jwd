package by.training.sadko.dao;

import by.training.sadko.entity.Literature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public enum LiteratureComparator implements Comparator<Literature> {
    ID_SORT("id") {
        @Override
        public int compare(Literature o1, Literature o2) {
            return o1.getId() - o2.getId();
        }
    },
    NAME_SORT("name") {
        @Override
        public int compare(Literature o1, Literature o2) {
            return o1.getName().compareToIgnoreCase(o2.getName());
        }
    },
    AUTHOR_SORT("author") {
        @Override
        public int compare(Literature o1, Literature o2) {
            return o1.getAuthor().compareTo(o2.getAuthor());
        }
    },
    PAGES_SORT("pages") {
        @Override
        public int compare(Literature o1, Literature o2) {
            return o1.getNumberOfPages() - o2.getNumberOfPages();
        }
    },
    GENRE_SORT("genre") {
        @Override
        public int compare(Literature o1, Literature o2) {
            return o1.getGenre().getName().compareTo(o2.getGenre().getName());
        }
    },
    PUBLICATION_TYPE_SORT("publication") {
        @Override
        public int compare(Literature o1, Literature o2) {
            return o1.getPublicationType().getName().compareTo(o2.getPublicationType().getName());
        }
    };

    private static final Logger LOGGER = LogManager.getLogger(LiteratureComparator.class);
    private final String name;

    LiteratureComparator(String name) {
        this.name = name;
    }

    public static Comparator<Literature> getComparator(LiteratureComparator... sortingOptions) {
        return (o1, o2) -> {
            List<LiteratureComparator> literatureComparators = Arrays.asList(sortingOptions);
            Iterator<LiteratureComparator> iterator = literatureComparators.iterator();

            LiteratureComparator comparator;
            while (iterator.hasNext()) {
                comparator = iterator.next();
                int res = comparator.compare(o1, o2);
                if (res != 0) {
                    return res;
                }
            }
            return 0;
        };
    }

    public static Comparator<Literature> descending(final Comparator<Literature> other) {
        return (o1, o2) -> -1 * other.compare(o1, o2);
    }

    public String getName() {
        return name;
    }
}
