package by.training.sadko.parser.xml.dom;

import by.training.sadko.parser.xml.XMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DomParser<T> extends XMLParser<T> {

    private static final Logger LOGGER = LogManager.getLogger(DomParser.class);
    private final DOMHandler<T> domHandler;

    public DomParser(DOMHandler<T> domHandler) {
        this.publicationsFromXML = new ArrayList<>();
        this.domHandler = domHandler;
    }

    @Override
    public List<T> parse(InputStream inputStream) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();

        } catch (ParserConfigurationException e) {
            LOGGER.error("There is DocumentBuilder initialization exception", e);
            return publicationsFromXML;
        }

        Document document;
        try {
            document = builder.parse(inputStream);
            publicationsFromXML = domHandler.getListFromXML(document);

        } catch (SAXException e) {
            LOGGER.error("There is Document initialization exception", e);
            return publicationsFromXML;
        } catch (IOException ioException) {
            LOGGER.error("Path to file is invalid", ioException);
            return publicationsFromXML;
        }
        return publicationsFromXML;
    }
}
