<%@ page import="by.training.sadko.AppConstants" %>
<%@ page import="by.training.sadko.command.CommandName" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<aside class="ui-menu">
    <ul class="ui-menu">
        <p class="menu-label">
            <b>Commands</b>
        </p>
        <form>
            <a href="/"></a>
            <button class="btn btn-sm btn-outline-dark">UPLOAD</button>
        </form>
        <form>
            <input type="hidden" name="${AppConstants.PARAM_COMMAND}" value="${CommandName.SHOW_COMMAND}"/>
            <input type="submit" value="LIST" class="btn btn-sm btn-outline-dark"/>
        </form>
        <form>
            <input type="hidden" name="${AppConstants.PARAM_COMMAND}" value="${CommandName.DOWNLOAD_TO_FILE_COMMAND}"/>
            <input type="submit" value="DOWNLOAD" class="btn btn-sm btn-outline-dark"/>
        </form>
    </ul>
</aside>