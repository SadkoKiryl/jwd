package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.controller.command.AppCommandRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class SimpleAppCommandRegistry implements AppCommandRegistry {

    private static final Logger LOGGER = LogManager.getLogger(SimpleAppCommandRegistry.class.getName());
    private final Map<AppCommandName, AppCommand> commandMap;

    public SimpleAppCommandRegistry(Map<AppCommandName, AppCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public AppCommand getCommand(String commandName) {
        AppCommandName appCommandName = AppCommandName.fromString(commandName);

        LOGGER.info("Command name is {}", appCommandName);
        return commandMap.getOrDefault(appCommandName, commandMap.get(AppCommandName.DEFAULT));
    }
}
