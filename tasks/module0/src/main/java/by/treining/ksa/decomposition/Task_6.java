package by.treining.ksa.decomposition;

/*
6. Вычислить площадь правильного шестиугольника со стороной а, используя метод вычисления площади
треугольника.
*/

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task_6 {

    public static void main(String[] args) {
        int side = 5;

        System.out.println("\nThe hexagon area is " + hexagonArea(side));
    }

    private static double hexagonArea(int side) {
        double area = triangleAre(side) * 6;
        return area;
    }

    private static double triangleAre(int side) {
        double area = pow(side, 2) * sqrt(3) / 4;
        return area;
    }
}
