package by.training.sadko.service;

import by.training.sadko.dao.LiteratureComparator;
import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;

import java.util.List;

public interface LiteratureService {

    long calculatingPagesByGenre(Genre genre);

    void deleteLiteratureById(int id);

    List<Literature> getAllLiterature();

    List<Literature> selectionLiteratureByGenre(Genre genre);

    void addLiterature(List<Literature> literatureList);

    void sortLiterature(LiteratureComparator... paramsForSorting);
}
