package by.training.sadko.entity;

public enum ContentType {
    NEWS("news"),
    ENTERTAINING("entertaining"),
    SCIENTIFIC("scientific"),
    ART("art"),
    LITERATURE("literature"),
    BUSINESS("business");

    private final String name;

    ContentType(String name) {
        this.name = name;
    }

    public static ContentType fromString(String userData) {
        ContentType[] contentArray = ContentType.values();
        for (ContentType content : contentArray) {
            if (content.name.equals(userData) || content.name().equals(userData)) {
                return content;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
