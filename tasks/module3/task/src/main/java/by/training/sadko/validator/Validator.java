package by.training.sadko.validator;

public interface Validator<T> {

    boolean validate(T t);
}
