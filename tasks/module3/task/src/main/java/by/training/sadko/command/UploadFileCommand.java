package by.training.sadko.command;

import by.training.sadko.AppConstants;
import by.training.sadko.parser.FileParser;
import by.training.sadko.parser.ParserFactory;
import by.training.sadko.service.EntitiesService;
import by.training.sadko.validator.Validator;
import by.training.sadko.validator.XSDValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static by.training.sadko.AppConstants.PARAM_PARSER;

public class UploadFileCommand<T> implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(UploadFileCommand.class);
    private final EntitiesService<T> service;
    private final ParserFactory<T> parserFactory;
    private final Validator<InputStream> xsdValidator = new XSDValidator();

    public UploadFileCommand(EntitiesService<T> service, ParserFactory<T> parserFactory) {
        this.service = service;
        this.parserFactory = parserFactory;
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse resp) throws AppCommandException {
        String parserName = request.getParameter(PARAM_PARSER);
        LOGGER.info("The selected parser is {}", parserName);


        InputStream fileContent;
        InputStream cloneForValidation;
        ByteArrayOutputStream baos;
        try(InputStream inputStream = getInputStream(request);) {

            baos = new ByteArrayOutputStream();
            inputStream.transferTo(baos);
            fileContent = new ByteArrayInputStream(baos.toByteArray());
            cloneForValidation = new ByteArrayInputStream(baos.toByteArray());
        } catch (IOException ioException) {
            LOGGER.error("InputStream is not exists");
            return "not_found_view";
        }

        List<T> publicationsFromFile;
        if (xsdValidator.validate(cloneForValidation)) {
            FileParser<T> xmlParser = parserFactory.getParser(parserName);

            publicationsFromFile = xmlParser.parse(fileContent);
            service.addEntities(publicationsFromFile);

            try {
                cloneForValidation.close();
                fileContent.close();
                baos.close();
            } catch (IOException ioException) {
                LOGGER.error("IOException");
                throw new AppCommandException("IOException");
            }
            request.setAttribute("upload_result", "Upload successful");

        } else {
            request.setAttribute("upload_result", "The file does not match XSD");
            LOGGER.info("Path to file is invalid");
            return "not_found_view";
        }
        return "upload_view";
    }

    private InputStream getInputStream(HttpServletRequest request) throws AppCommandException, IOException {
        InputStream fileContent = null;
        try {
            Part filePart = request.getPart(AppConstants.DEFAULT_FILE_NAME);
            fileContent = filePart.getInputStream();
        } catch (ServletException e) {
            LOGGER.error("Servlet exception", e);
            throw new AppCommandException("Servlet exception");
        }
        return fileContent;
    }
}
