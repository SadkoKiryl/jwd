package by.treining.ksa.oop.simple.task4;

import java.util.Calendar;
import java.util.Objects;

/*
4. Создайте класс Train, содержащий поля: название пункта назначения, номер поезда, время отправления.
Создайте данные в массив из пяти элементов типа Train, добавьте возможность сортировки элементов массива по
номерам поездов. Добавьте возможность вывода информации о поезде, номер которого введен пользователем.
Добавьте возможность сортировки массива по пункту назначения, причем поезда с одинаковыми пунктами назначения
должны быть упорядочены по времени отправления.
*/
public class Train {
    private final int trainNumber;
    private final String destination;
    private Calendar departureTime;

    public Train(int trainNumber, String destination, Calendar departureTime) {
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }

    public static void getInfo(int trainNumber, Train[] trains) {

        for (Train train : trains) {
            if (trainNumber == train.trainNumber) {
                System.out.println(train.toString());
            }
        }
    }

    @Override
    public String toString() {
        return "Train{" +
                "trainNumber=" + trainNumber +
                ", destination='" + destination + '\'' +
                ", departureTime=" + departureTime.getTime() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Train train = (Train) o;
        return trainNumber == train.trainNumber &&
                destination.equals(train.destination) &&
                departureTime.equals(train.departureTime);
    }

    public String getDestination() {
        return destination;
    }

    public Calendar getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Calendar departureTime) {
        this.departureTime = departureTime;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, trainNumber, departureTime);
    }
}
