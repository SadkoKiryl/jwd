package by.training.sadko.entity;

import java.util.Objects;

public abstract class Publication {

    protected String id;
    protected String type;
    protected ContentType contentType;
    protected String title;
    protected boolean isMonthly;
    protected boolean isColored;
    protected int volume;

    public Publication() {
    }

    public Publication(String id, ContentType contentType, String title, boolean isMonthly, boolean isColored, int volume) {
        this.id = id;
        this.contentType = contentType;
        this.title = title;
        this.isMonthly = isMonthly;
        this.isColored = isColored;
        this.volume = volume;
    }

    public boolean isMonthly() {
        return isMonthly;
    }

    public void setMonthly(boolean monthly) {
        isMonthly = monthly;
    }

    public boolean isColored() {
        return isColored;
    }

    public void setColored(boolean colored) {
        isColored = colored;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }


    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
