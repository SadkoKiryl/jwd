package by.treining.ksa.oop.association.task3;

import by.treining.ksa.oop.association.task3.model.City;
import by.treining.ksa.oop.association.task3.model.District;
import by.treining.ksa.oop.association.task3.model.Region;
import by.treining.ksa.oop.association.task3.model.State;
import by.treining.ksa.oop.association.task3.service.SimpleStateService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        District district1 = new District("FirstDistrict", 50);
        District district2 = new District("SecondDistrict", 39);
        District district3 = new District("ThirdDistrict", 58);

        List<District> districtList = new ArrayList<>();
        districtList.add(district1);
        districtList.add(district2);
        districtList.add(district3);

        City city1 = new City("Minsk", districtList, true, true);
        City city2 = new City("Bobruysk", districtList, false, false);
        City city3 = new City("London", districtList, true, false);
        City city4 = new City("Berlin", districtList, true, false);
        List<City> cityList1 = new ArrayList<>();
        cityList1.add(city1);
        cityList1.add(city2);

        Region region1 = new Region("MinskRegion", cityList1);
        Region region2 = new Region("London Region", city3);
        Region region3 = new Region("BerlinRegion", city4);
        List<Region> regionList = new ArrayList<>();
        regionList.add(region1);
        regionList.add(region2);
        regionList.add(region3);

        State state = new State("Hybrid State", regionList);

        SimpleStateService stateService = new SimpleStateService();

        stateService.printingCapital(state);
        stateService.printingRegionCount(state);
        stateService.printingRegionCenterList(state);
        stateService.printingStateArea(state);

    }
}
