package by.training.sadko.builder;

public interface EntityBuilder<T> {

    T getEntity();
}
