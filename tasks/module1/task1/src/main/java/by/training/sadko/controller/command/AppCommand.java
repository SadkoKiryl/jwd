package by.training.sadko.controller.command;

import java.util.Map;

public interface AppCommand {

    String execute(Map<String, String> requestMap);
}
