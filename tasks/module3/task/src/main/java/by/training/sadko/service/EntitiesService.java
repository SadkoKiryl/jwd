package by.training.sadko.service;

import java.util.List;

public interface EntitiesService<T> {

    List<T> getEntitiesList();

    void addEntities(List<T> entities);
}
