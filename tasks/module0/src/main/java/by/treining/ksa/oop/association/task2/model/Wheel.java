package by.treining.ksa.oop.association.task2.model;

public class Wheel {
    private boolean isPierced;

    public Wheel() {
        isPierced = true;
    }

    public boolean isPierced() {
        return isPierced;
    }

    public void setPierced(boolean isPierced) {
        this.isPierced = isPierced;
    }
}
