package by.treining.ksa.oop.association.task4;

import by.treining.ksa.oop.association.task4.model.BankAccount;
import by.treining.ksa.oop.association.task4.model.Customer;
import by.treining.ksa.oop.association.task4.service.SimpleBankAccountService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        BankAccount bankAccount1 = new BankAccount(1, -250);
        BankAccount bankAccount2 = new BankAccount(2, 570);
        BankAccount bankAccount3 = new BankAccount(3, 3200);
        BankAccount bankAccount4 = new BankAccount(4, 4800);
        BankAccount bankAccount5 = new BankAccount(5, -2400);

        List<BankAccount> bankAccountList1 = new ArrayList<>();
        bankAccountList1.add(bankAccount1);
        bankAccountList1.add(bankAccount2);
        bankAccountList1.add(bankAccount3);

        List<BankAccount> bankAccountList2 = new ArrayList<>();
        bankAccountList2.add(bankAccount4);
        bankAccountList2.add(bankAccount5);

        Customer customer1 = new Customer("Vasya", bankAccountList1);
        Customer customer2 = new Customer("Valera", bankAccountList2);
        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);

        SimpleBankAccountService accountService = new SimpleBankAccountService();
        accountService.accountLock(customer1, 4);
        accountService.accountLock(customer1, 3);
        accountService.accountUnlock(customer1, 3);

        accountService.searchAccount(customer1, 1);

        System.out.println("The overall balance is " + accountService.overallBalance(customerList));
        System.out.println("The positive balance is " + accountService.positiveBalance(customerList));
        System.out.println("The negative balance is" + accountService.negativeBalance(customerList));
    }
}
