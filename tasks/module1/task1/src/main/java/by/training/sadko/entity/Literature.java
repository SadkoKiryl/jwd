package by.training.sadko.entity;

import java.util.Objects;

public class Literature {

    private static int countOfLiterature = 0;
    private final int id;
    private Genre genre;
    private String name;
    private Author author;
    private int numberOfPages;
    private PublicationType publicationType;

    public Literature(Genre genre, String name, int numberOfPages, PublicationType publicationType, Author author) {

        this.id = ++countOfLiterature;
        this.genre = genre;
        this.name = name;
        this.numberOfPages = numberOfPages;
        this.publicationType = publicationType;
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Literature that = (Literature) o;
        return numberOfPages == that.numberOfPages &&
                genre == that.genre &&
                Objects.equals(name, that.name) &&
                Objects.equals(author, that.author) &&
                publicationType == that.publicationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, genre, name, author, numberOfPages, publicationType);
    }

    @Override
    public String toString() {
        return "Literature{" +
                "id=" + id +
                ", genre=" + genre +
                ", name='" + name + '\'' +
                ", author=" + author +
                ", numberOfPages=" + numberOfPages +
                ", publicationType=" + publicationType +
                '}';
    }

    public int getId() {
        return id;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public PublicationType getPublicationType() {
        return publicationType;
    }

    public void setPublicationType(PublicationType publicationType) {
        this.publicationType = publicationType;
    }
}
