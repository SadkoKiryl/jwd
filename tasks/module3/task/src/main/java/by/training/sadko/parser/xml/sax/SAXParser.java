package by.training.sadko.parser.xml.sax;

import by.training.sadko.parser.xml.XMLParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SAXParser<T> extends XMLParser<T> {

    private static final Logger LOGGER = LogManager.getLogger(SAXParser.class);
    private final SAXHandler<T> saxHandler;

    public SAXParser(SAXHandler<T> saxHandler) {
        this.saxHandler = saxHandler;
        this.publicationsFromXML = new ArrayList<>();
    }

    @Override
    public List<T> parse(InputStream inputStream) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        javax.xml.parsers.SAXParser parser;

        try {
            parser = factory.newSAXParser();

        } catch (ParserConfigurationException e) {
            LOGGER.error("ParserConfiguration error", e);
            return publicationsFromXML;

        } catch (SAXException e) {
            LOGGER.error("SAX Exception", e);
            return publicationsFromXML;
        }

        try {
            parser.parse(inputStream, saxHandler);
            publicationsFromXML = saxHandler.getListFromXML();

        } catch (SAXException e) {
            LOGGER.error("There is SAXException", e);
            return publicationsFromXML;
        } catch (IOException ioException) {
            LOGGER.error("File not found", ioException);
            return publicationsFromXML;
        }
        return publicationsFromXML;
    }
}
