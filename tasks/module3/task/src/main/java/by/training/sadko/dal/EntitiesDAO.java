package by.training.sadko.dal;

import java.util.List;

public interface EntitiesDAO<T> {

    List<T> getEntitiesDAO();

    void addEntities(List<T> entities);
}
