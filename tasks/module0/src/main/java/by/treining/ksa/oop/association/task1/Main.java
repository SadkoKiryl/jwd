package by.treining.ksa.oop.association.task1;

import by.treining.ksa.oop.association.task1.model.Sentence;
import by.treining.ksa.oop.association.task1.model.Text;
import by.treining.ksa.oop.association.task1.model.Word;

import java.util.ArrayList;
import java.util.List;

/*
1. Создать объект класса Текст, используя классы Предложение, Слово. Методы: дополнить текст,
вывести на консоль текст, заголовок текста.
*/

public class Main {
    public static void main(String[] args) {
        List<Word> wordList1 = new ArrayList<>();
        wordList1.add(new Word("Peter "));
        wordList1.add(new Word("Piper "));
        wordList1.add(new Word("picked "));
        wordList1.add(new Word("a "));
        wordList1.add(new Word("peck "));
        wordList1.add(new Word("of "));
        wordList1.add(new Word("pickled "));
        wordList1.add(new Word("peppers. "));

        List<Word> wordList2 = new ArrayList<>();
        wordList2.add(new Word("A "));
        wordList2.add(new Word("peck "));
        wordList2.add(new Word("pickled "));
        wordList2.add(new Word("peppers "));
        wordList2.add(new Word("Peter "));
        wordList2.add(new Word("Piper "));
        wordList2.add(new Word("picked. "));

        Sentence sentence1 = new Sentence(wordList1);
        Sentence sentence2 = new Sentence(wordList2);

        List<Sentence> sentenceList = new ArrayList<>();
        sentenceList.add(sentence1);
        sentenceList.add(sentence2);

        Text text = new Text(sentenceList);
        text.printingText();
        System.out.println("\n----------------");

        text.addingText(sentence2);
        text.printingText();
        System.out.println("\n----------------");


        text.headingText("Peter Piper");
        text.printingText();
    }
}
