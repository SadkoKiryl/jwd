package by.treining.ksa.oop.association.task5;

import by.treining.ksa.oop.association.task5.entity.Transport;
import by.treining.ksa.oop.association.task5.entity.Voucher;
import by.treining.ksa.oop.association.task5.entity.VoucherType;
import by.treining.ksa.oop.association.task5.service.*;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        VoucherFactory factoryService = new VoucherFactoryService();

        //Preview vouchers
        List<Voucher> voucherList = factoryService.getVoucherList(VoucherType.REST, Transport.AIRPLANE);

        for (Voucher voucher : voucherList) {
            System.out.println(voucher);
        }

        System.out.println("\n----------------------------\n");


        //Sorting vouchers
        VoucherSorting voucherSorting = new VoucherSortingService();
        voucherSorting.sortByCost(voucherList);

        for (Voucher voucher : voucherList) {
            System.out.println(voucher);
        }

        //Selection voucher
        VoucherSelection voucherSelection = new VoucherSelectionService();
        Voucher requestVoucher = voucherSelection.getRequiredVoucher(47, voucherList);
    }
}
