package by.treining.ksa.decomposition;

/*
18. Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую последовательность
        (например, 1234, 5789). Для решения задачи использовать декомпозицию.
*/
public class Task_18 {

    public static void main(String[] args) {

        int n = 3;

        findIncreasingSequence(n);

    }

    private static void findIncreasingSequence(int n) {

        int[] increasingSequence = new int[n];
        for (int i = 1; i <= 10 - n; i++) {
            int temp = i;

            for (int j = 0; j < n; j++) {
                increasingSequence[j] = temp;
                temp++;
            }

            int number = convertSequenceToNum(increasingSequence);

            System.out.println(number);
        }
    }

    private static int convertSequenceToNum(int[] sequence) {
        String tempNum = "";

        for (int i = 0; i < sequence.length; i++) {
            tempNum += String.valueOf(sequence[i]);
        }

        return Integer.parseInt(tempNum);
    }
}
