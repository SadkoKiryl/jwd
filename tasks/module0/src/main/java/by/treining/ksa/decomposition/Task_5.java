package by.treining.ksa.decomposition;

/*
5. Написать метод(методы) для нахождения суммы большего и меньшего из трех чисел.
*/

public class Task_5 {

    public static void main(String[] args) {
        int num1 = -4;
        int num2 = 23;
        int num3 = 0;

        System.out.println("\nThe sum of the largest and smallest of the three numbers is " + sumOfNumbers(num1, num2, num3));
    }

    private static int sumOfNumbers(int num1, int num2, int num3) {
        return max(num1, num2, num3) + min(num1, num2, num3);
    }


    private static int max(int num1, int num2, int num3) {
        int max = num1;

        if (num2 > max) {
            max = num2;
        }

        if (num3 > max) {
            max = num3;
        }

        return max;
    }

    private static int min(int num1, int num2, int num3) {
        int min = num1;

        if (min > num2) {
            min = num2;
        }

        if (min > num3) {
            min = num3;
        }

        return min;
    }
}
