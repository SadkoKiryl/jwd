package by.treining.ksa.decomposition;

/*
10. Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9.
*/

public class Task_10 {

    public static void main(String[] args) {
        int sum = sumOfFactorials(9);
        System.out.println("\nThe sum of factorials is " + sum);
    }

    private static int sumOfFactorials(int n) {

        int sum = 0;
        for (int i = 1; i <= n; i++) {
            int factorial = 1;
            if (i % 2 != 0) {
                for (int j = 2; j <= i; j++) {
                    factorial *= j;
                }
                sum += factorial;
            }
        }
        return sum;
    }
}
