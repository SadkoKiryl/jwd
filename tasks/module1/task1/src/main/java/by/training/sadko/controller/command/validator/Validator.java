package by.training.sadko.controller.command.validator;

public interface Validator<T> {

    boolean isValid(T t);
}
