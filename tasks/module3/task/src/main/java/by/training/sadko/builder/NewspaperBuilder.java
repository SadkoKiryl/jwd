package by.training.sadko.builder;

import by.training.sadko.entity.ContentType;
import by.training.sadko.entity.Newspaper;

public class NewspaperBuilder implements PublicationBuilder {

    private Newspaper newspaper;

    public NewspaperBuilder() {
        newspaper = new Newspaper();
    }

    @Override
    public void setID(String value) {
        newspaper.setId(value);
    }

    @Override
    public void setContentType(String value) {
        newspaper.setContentType(ContentType.fromString(value));
    }

    @Override
    public void setTitle(String value) {
        newspaper.setTitle(value);
    }

    @Override
    public void setIsMonthly(String isMonthly) {
        newspaper.setMonthly(Boolean.parseBoolean(isMonthly));
    }

    @Override
    public void setIsColored(String isColored) {
        newspaper.setColored(Boolean.parseBoolean(isColored));
    }

    @Override
    public void setVolume(String volume) {
        newspaper.setVolume(Integer.parseInt(volume));
    }

    @Override
    public void setSubscriptionIndex(String subscriptionIndex) {
        newspaper.setSubscriptionIndex(Integer.parseInt(subscriptionIndex));
    }

    @Override
    public void setIsGlossy(String isGlossy) {
        //not exists
    }

    @Override
    public Newspaper getEntity() {
        Newspaper copy = newspaper;
        newspaper = new Newspaper();
        return copy;
    }
}
