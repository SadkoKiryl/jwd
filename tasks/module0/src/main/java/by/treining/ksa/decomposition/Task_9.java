package by.treining.ksa.decomposition;

/*
9. Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми.
*/
public class Task_9 {

    public static void main(String[] args) {

        int num1 = 1;
        int num2 = 2;
        int num3 = 3;

        System.out.println("The numbers " + num1 + ", " + num2 + ", " + num3 + " are coprime - " +
                checkNumbers(num1, num2, num3));

    }

    private static int greatestCommonDivisor(int num1, int num2) {
        return num2 == 0 ? num1 : greatestCommonDivisor(num2, num1 % num2);
    }


    private static boolean checkNumbers(int num1, int num2, int num3) {
        int gCD1 = greatestCommonDivisor(num1, num2);
        int gCD2 = greatestCommonDivisor(gCD1, num3);
        return gCD2 == 1;
    }
}
