package by.treining.ksa.decomposition;

/*
15. Даны натуральные числа К и N. Написать метод(методы) формирования массива А, элементами которого
        являются числа, сумма цифр которых равна К и которые не большее N.
*/
public class Task_15 {

    public static void main(String[] args) {
        int k = 6;
        int n = 30;
        System.out.println("\nThe number k is " + k + ", the number n is " + n);

        createArray(k, n);
    }

    private static void createArray(int k, int n) {
        int[] array = new int[5];

        System.out.print("The array is: ");
        for (int i = 0; i < array.length; i++) {

            array[i] = createNumber(k, n);
            System.out.print(array[i] + " ");
        }
    }

    private static int createNumber(int k, int n) {
        int number = 0;
        int sum = 0;

        while (sum != k) {
            sum = 0;
            number = (int) (Math.random() * n);

            String tempNumber = String.valueOf(number);
            String[] tempArray = tempNumber.split("");
            int[] arrayOfIntegers = new int[tempArray.length];

            for (int i = 0; i < arrayOfIntegers.length; i++) {
                arrayOfIntegers[i] = Integer.parseInt(tempArray[i]);
                sum += arrayOfIntegers[i];
            }
        }

        return number;
    }
}
