package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.entity.Literature;
import by.training.sadko.service.LiteratureService;

import java.util.List;
import java.util.Map;

import static by.training.sadko.AppConstants.*;

public class DefaultCommand implements AppCommand {

    private final LiteratureService service;

    public DefaultCommand(LiteratureService service) {
        this.service = service;
    }

    @Override
    public String execute(Map<String, String> requestMap) {
        List<Literature> literatureList = service.getAllLiterature();
        StringBuilder responseString = new StringBuilder();
        for (Literature tempLiterature : literatureList) {
            responseString
                    .append("<li>")
                    .append(tempLiterature.toString())
                    .append("<form action=\"/home\" method=\"POST\">\n" + "\t<input type=\"hidden\" name=\"")
                    .append(COMMAND_NAME + "\" value=\"delete\">\n")
                    .append("\t<input type=\"hidden\" name=\"" + PARAM_ID + "\" value=\"")
                    .append(tempLiterature.getId())
                    .append("\">\n")
                    .append("\t<input type=\"submit\" value=\"Delete\" />\n")
                    .append("</form>")
                    .append("</li>");
        }
        responseString
                .append("<form action=\"/home\" align=\"center\">\n")
                .append("<br/>")
                .append("<label for=\"" + PARAM_GENRE + "\">")
                .append("Enter the genre to calculate the number of pages:</label><br>\n")
                .append("<input type=\"text\"  name=" + PARAM_GENRE + " value=\"\">\n")
                .append("<input type=\"hidden\"  name=" + COMMAND_NAME + " value=\"calc\">\n")
                .append("<br>\n\n")
                .append("<input type=\"submit\" value=\"Calculate\">\n")
                .append("</form> ");

        return responseString.toString();
    }
}
