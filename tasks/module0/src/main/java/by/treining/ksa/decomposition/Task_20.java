package by.treining.ksa.decomposition;

/*
20. Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д. Сколько таких
        действий надо произвести, чтобы получился нуль? Для решения задачи использовать декомпозицию.
*/
public class Task_20 {

    public static void main(String[] args) {
        int num = 28;

        int amountOfManipulation = calculateAmountToZero(num);

        System.out.println("\nThe amount of manipulations is " + amountOfManipulation);
    }

    private static int calculateAmountToZero(int num) {
        int amount = 0;

        while (num > 0) {
            num = num - calculateSumOfDigits(num);
            amount++;
        }
        return amount;
    }

    private static int calculateSumOfDigits(int num) {
        String tempNum = String.valueOf(num);
        String[] tempArray = tempNum.split("");
        int[] arrayOfDigits = new int[tempArray.length];

        int sum = 0;
        for (int i = 0; i < tempArray.length; i++) {
            arrayOfDigits[i] = Integer.parseInt(tempArray[i]);
            sum += arrayOfDigits[i];
        }
        return sum;
    }
}