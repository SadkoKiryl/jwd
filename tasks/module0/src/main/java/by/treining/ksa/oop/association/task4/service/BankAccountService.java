package by.treining.ksa.oop.association.task4.service;

import by.treining.ksa.oop.association.task4.model.BankAccount;
import by.treining.ksa.oop.association.task4.model.Customer;

import java.util.List;

public interface BankAccountService {
    void accountLock(Customer customer, int accountNumber);

    void accountUnlock(Customer customer, int accountNumber);

    void searchAccount(Customer customer, int accountNumber);

    List<BankAccount> sortingAccount(Customer customer);

    double overallBalance(List<Customer> customerList);

    double positiveBalance(List<Customer> customerList);

    double negativeBalance(List<Customer> customerList);
}
