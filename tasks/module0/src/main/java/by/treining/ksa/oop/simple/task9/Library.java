package by.treining.ksa.oop.simple.task9;

/*
Создать второй класс, агрегирующий массив типа Book, с подходящими конструкторами и методами.
Задать критерии выбора данных и вывести эти данные на консоль.
Найти и вывести:
        a) список книг заданного автора;
        b) список книг, выпущенных заданным издательством;
        c) список книг, выпущенных после заданного года.
*/

public class Library {
    private Book[] books;

    public Library(Book[] books) {
        this.books = books;
    }

    public void searchByAuthor(String author) {
        for (Book book : this.books) {
            if (author.equals(book.getAuthor())) {
                System.out.println(book.toString());
            }
        }
    }

    public void searchByPublishing(String publishingName) {
        for (Book book : this.books) {
            if (publishingName.equals(book.getPublishingName())) {
                System.out.println(book.toString());
            }
        }
    }

    public void searchByYear(int year) {
        for (Book book : books) {
            if (year <= book.getYear()) {
                System.out.println(book.toString());
            }
        }
    }

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }
}
