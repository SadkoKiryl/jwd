package by.training.sadko.dal;

import by.training.sadko.entity.Booklet;
import by.training.sadko.entity.Magazine;
import by.training.sadko.entity.Newspaper;
import by.training.sadko.entity.Publication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SimplePublicationDAOTest {

    private SimplePublicationDAO dao;
    private List<Publication> publications;
    private int daoSize;
    private int daoSizeAfterAdding;


    @Before
    public void setUp() {
        dao = SimplePublicationDAO.getInstance();
        daoSize = dao.getEntitiesDAO().size();
        publications = new ArrayList<>();
        publications.add(new Magazine());
        publications.add(new Booklet());
        publications.add(new Newspaper());
    }

    @Test
    public void daoShouldAddNewPublications() {
        dao.addEntities(publications);
        daoSizeAfterAdding = dao.getEntitiesDAO().size();
        Assert.assertNotEquals(daoSize, daoSizeAfterAdding);
    }

    @Test
    public void daoShouldAddUniqueValues() {
        dao.addEntities(publications);
        daoSizeAfterAdding = dao.getEntitiesDAO().size();
        dao.addEntities(publications);
        int currentSize = dao.getEntitiesDAO().size();
        Assert.assertEquals(currentSize, daoSizeAfterAdding);
    }
}
