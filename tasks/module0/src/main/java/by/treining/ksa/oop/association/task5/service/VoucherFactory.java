package by.treining.ksa.oop.association.task5.service;

import by.treining.ksa.oop.association.task5.entity.*;

import java.util.List;

public interface VoucherFactory {
    List<Voucher> getVoucherList(VoucherType voucherType);

    List<Voucher> getVoucherList(VoucherType voucherType, Transport transportType);

    List<Voucher> getVoucherList(VoucherType voucherType, Transport transportType, Food foodType);

    List<Voucher> getVoucherList(VoucherType voucherType, Transport transportType, Food foodType, AmountOfDays amountOfDays);

}

