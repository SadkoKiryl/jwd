package by.treining.ksa.arrays;

import java.util.Scanner;

public class MultiArray {
    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
        task11();
        task12();
        task13();
        task14();
        task15();
        task16();
        task17();
        task18();
        task19();
        task27();
        task32();
        task33();
    }

    private static void task1() {
        System.out.println("\n\n1. Cоздать матрицу 3 x 4, заполнить ее числами 0 и 1 так, чтобы в одной строке была ровно одна единица, и вывести на экран.");
        int[][] multiArray = new int[3][4];

        multiArray[0][0] = 1;
        multiArray[1][1] = 1;
        multiArray[2][2] = 1;

        for (int[] ints : multiArray) {
            System.out.println();
            for (int anInt : ints) {  // Можно ли так упрощать код или делать его более читабельным?
                System.out.print(anInt + " ");
            }
        }
    }

    private static void task2() {
        System.out.println("\n\n2. Создать и вывести на экран матрицу 2 x 3, заполненную случайными числами из [0, 9].");
        int[][] arr = new int[2][3];
        for (int i = 0; i < arr.length; i++) {
            System.out.println();
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }
    }

    private static void task3() {
        System.out.println("\n\n3. Дана матрица. Вывести на экран первый и последний столбцы.");
        int[][] arr = new int[3][5];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.println("\nFirst and last column: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][0] + " " + arr[i][4]);
        }
    }

    private static void task4() {
        System.out.println("\n\n4. Дана матрица. Вывести на экран первую и последнюю строки.");
        int[][] arr = new int[3][5];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }


        System.out.println("\n\nFirst and last rows: ");
        for (int i = 0; i < arr[0].length; i++) {
            System.out.print(arr[0][i] + " ");
        }

        System.out.println();
        for (int i = 0; i < arr[0].length; i++) {
            System.out.print(arr[2][i] + " ");
        }
    }

    private static void task5() {
        System.out.println("\n\n5. Дана матрица. Вывести на экран все четные строки, то есть с четными номерами.");
        int[][] arr = new int[6][5];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.println("\n\nThe even rows are: ");
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 != 0) {
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j] + " ");
                }
                System.out.println();
            }
        }
    }

    private static void task6() {
        System.out.println("\n\n6. Дана матрица. Вывести на экран все нечетные столбцы, у которых первый элемент больше последнего.");
        int[][] arr = new int[6][5];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.println("\n\nThe columns: ");
        for (int j = 1; j < arr[0].length; j += 2) {
            if (arr[0][j] > arr[arr.length - 1][j]) {
                for (int i = 0; i < arr.length; i++) {
                    System.out.println(arr[i][j]);
                }
                System.out.println();
            }
        }
    }

    private static void task7() {
        System.out.println("\n\n7. Дан двухмерный массив 5×5. Найти сумму модулей отрицательных нечетных элементов.");
        int[][] arr = new int[5][5];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10) - 3;
                System.out.print(arr[i][j] + " ");
            }
        }

        int sum = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] < 0 && arr[i][j] % 2 == 0) {
                    sum += arr[i][j];
                }
            }
        }
        System.out.println("\nThe sum is: " + sum);
    }

    private static void task8() {
        System.out.println("\n\n8. Дан двухмерный массив n×m элементов. Определить, сколько раз встречается число 7 среди элементов массива.");
        int[][] arr = new int[6][5];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 8);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.println();
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == 7) {
                    count++;
                }
            }
        }
        System.out.println("The number seven occurs " + count + " times");
    }

    private static void task9() {
        System.out.println("\n\n9. Дана квадратная матрица. Вывести на экран элементы, стоящие на диагонали.");
        int[][] arr = new int[4][4];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.println("\n");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i][i] + " ");
        }
    }

    private static void task10() {
        System.out.println("\n\n10. Дана матрица. Вывести k-ю строку и p-й столбец матрицы.");
        int k = 2;
        int p = 4;

        int[][] arr = new int[6][8];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.print("\nThe row number " + k + ": ");
        for (int j = 0; j < arr[k - 1].length; j++) {
            System.out.print(arr[k - 1][j] + " ");
        }

        System.out.println("\nThe column number " + p + ": ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i][p - 1]);
        }
    }

    private static void task11() {
        System.out.println("\n\n11. Дана матрица размера m x n. Вывести ее элементы в следующем порядке: первая строка справа налево, вторая строка слева направо, третья строка справа налево и так далее.");
        int[][] arr = new int[6][8];

        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        System.out.println("\n");
        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            if (i % 2 == 0) {
                for (int j = arr[i].length - 1; j >= 0; j--) {
                    System.out.print(arr[i][j] + " ");
                }
            } else {
                for (int j = 0; j < arr[i].length; j++) {
                    System.out.print(arr[i][j] + " ");
                }
            }
        }
    }

    private static void task12() {
        System.out.println("\n\n12. Получить квадратную матрицу порядка n: см. задание");
        int n = 5;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = 1;
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
        }
    }

    private static void task13() {
        System.out.println("\n\n13. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 6;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            System.out.println();
            if (i % 2 == 0) {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = j + 1;
                    System.out.print(arr[i][j] + " ");
                }
            } else {
                for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = n - j;
                    System.out.print(arr[i][j] + " ");
                }
            }
        }
    }

    private static void task14() {
        System.out.println("\n\n14. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 6;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = arr[i].length - 1; j >= 0; j--) {
                arr[i][i] = j + 1;
                System.out.print(arr[i][j] + " ");
            }
        }
    }

    private static void task15() {
        System.out.println("\n\n15. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 6;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = n--;
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
        }
    }

    private static void task16() {
        System.out.println("\n\n16. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 8;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            arr[i][i] = (i + 1) - (i + 2);
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
        }
    }

    private static void task17() {
        System.out.println("\n\n17. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 8;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            arr[i][0] = 1;
            arr[i][n - 1] = 1;
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[0][j] = 1;
                arr[n - 1][j] = 1;
                System.out.print(arr[i][j] + " ");
            }
        }
    }

    private static void task18() {
        System.out.println("\n\n18. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 8;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < arr[i].length - i; j++) {
                arr[i][j] = i + 1;
            }
        }

        for (int[] a : arr) {
            System.out.println();
            for (int i : a) {
                System.out.print(i + " ");
            }
        }
    }

    private static void task19() {
        System.out.println("\n\n19. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное): см. задание");
        int n = 10;
        int[][] arr = new int[n][n];

        int temp = arr[0].length;
        int half = n / 2;

        for (int i = 0; i < half; i++) {

            for (int j = 0; j < temp; j++) {

                arr[i][j + i] = 1;
            }
            temp -= 2;
        }

        temp = arr[0].length;
        int temp2 = 0;
        for (int i = arr.length - 1; i >= half; i--) {

            for (int j = 0; j < temp; j++) {

                arr[i][j + temp2] = 1;
            }

            temp -= 2;
            temp2++;
        }

        for (int[] a : arr) {
            System.out.println();
            for (int i : a) {
                System.out.print(i + " ");
            }
        }
    }

    private static void task27() {
        System.out.println("\n\n27. В числовой матрице поменять местами два столбца любых столбца, т. е. все элементы одного столбца поставить на\n" +
                "соответствующие им позиции другого, а его элементы второго переместить в первый. Номера столбцов вводит\n" +
                "пользователь с клавиатуры.");

        int n = 6;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        Scanner scanner = new Scanner(System.in);
        System.out.print("\nEnter first column: _");
        int firstColumn = Integer.parseInt(scanner.nextLine());
        System.out.print("\nEnter second column: _");
        int secondColumn = Integer.parseInt(scanner.nextLine());

        //Saving first column in the temp
        int[] temp = new int[n];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i][firstColumn];
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i][firstColumn] = arr[i][secondColumn];
            arr[i][secondColumn] = temp[i];
        }

        System.out.println();
        for (int[] array : arr) {
            System.out.println();
            for (int a : array) {
                System.out.print(a + " ");
            }
        }
    }

    private static void task32() {
        System.out.println("\n\n32. Отсортировать строки матрицы по возрастанию и убыванию значений элементов.");
        int n = 6;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        //Sorting in increasing order
        System.out.print("\n\nSorting in increasing order by rows:");
        for (int i = 0; i < arr.length; i++) {
            boolean sorted = false;

            while (!sorted) {
                sorted = true;

                for (int j = 0; j < arr[i].length - 1; j++) {
                    if (arr[i][j] > arr[i][j + 1]) {
                        int temp = arr[i][j];
                        arr[i][j] = arr[i][j + 1];
                        arr[i][j + 1] = temp;
                        sorted = false;
                    }
                }
            }
        }

        for (int[] array : arr) {
            System.out.println();

            for (int a : array) {
                System.out.print(a + " ");
            }
        }

        //Sorting in decreasing order
        System.out.print("\n\nSorting in decreasing order by rows:");
        for (int i = 0; i < arr.length; i++) {
            boolean sorted = false;

            while (!sorted) {
                sorted = true;

                for (int j = 0; j < arr[i].length - 1; j++) {
                    if (arr[i][j] < arr[i][j + 1]) {
                        int temp = arr[i][j];
                        arr[i][j] = arr[i][j + 1];
                        arr[i][j + 1] = temp;
                        sorted = false;
                    }
                }
            }
        }

        for (int[] array : arr) {
            System.out.println();

            for (int a : array) {
                System.out.print(a + " ");
            }
        }
    }

    private static void task33() {
        System.out.println("\n\n33. Отсотрировать стобцы матрицы по возрастанию и убыванию значений эементов.");
        int n = 6;
        int[][] arr = new int[n][n];
        for (int i = 0; i < arr.length; i++) {
            System.out.println();

            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = (int) (Math.random() * 10);
                System.out.print(arr[i][j] + " ");
            }
        }

        //Sorting in increasing order
        System.out.print("\n\nSorting by increasing order by columns:");
        for (int j = 0; j < arr[n-1].length; j++) {
            boolean sorted = false;

            while (!sorted) {
                sorted = true;

                for (int i = 0; i < arr.length - 1; i++) {
                    if (arr[i][j] > arr[i+1][j]) {
                        int temp = arr[i][j];
                        arr[i][j] = arr[i+1][j];
                        arr[i+1][j] = temp;
                        sorted = false;
                    }
                }
            }
        }

        for (int[] array : arr) {
            System.out.println();
            for (int a : array) {
                System.out.print(a + " ");
            }
        }

        System.out.print("\n\nSorting by decreasing order by columns:");
        for (int j = 0; j < arr[n-1].length; j++) {
            boolean sorted = false;

            while (!sorted) {
                sorted = true;

                for (int i = 0; i < arr.length - 1; i++) {
                    if (arr[i][j] < arr[i+1][j]) {
                        int temp = arr[i][j];
                        arr[i][j] = arr[i+1][j];
                        arr[i+1][j] = temp;
                        sorted = false;
                    }
                }
            }
        }

        for (int[] array : arr) {
            System.out.println();
            for (int a : array) {
                System.out.print(a + " ");
            }
        }
    }
}

