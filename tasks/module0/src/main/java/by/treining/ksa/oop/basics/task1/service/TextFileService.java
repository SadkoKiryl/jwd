package by.treining.ksa.oop.basics.task1.service;

import by.treining.ksa.oop.basics.task1.model.Directory;
import by.treining.ksa.oop.basics.task1.model.File;

import java.util.ArrayList;
import java.util.List;

public interface TextFileService extends FileService {
    public void printFileBody(Directory directory, String fileName);
    public void addFileBody(Directory directory, String fileName, List<String> body);
}
