package by.training.sadko;

public class AppConstants {

    public static final String PARAM_COMMAND = "_command";
    public static final String PARAM_PARSER = "_parser";
    public static final String PARAM_PATH_TO_FILE = "_path";
    public static final String COMMAND_RESULT = "_result";
    public static final String DATA_LIST = "_list";
    public static final String DEFAULT_FILE_NAME = "fileName";

    public static final String TAG_NEWSPAPER = "Newspaper";
    public static final String TAG_MAGAZINE = "Magazine";
    public static final String TAG_BOOKLET = "Booklet";

    public static final String FIELD_ID = "id";
    public static final String FIELD_CONTENT_TYPE = "content";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_MONTHLY = "monthly";
    public static final String FIELD_COLORED = "colored";
    public static final String FIELD_VOLUME = "volume";
    public static final String FIELD_SUBSCRIPTION_INDEX = "subscriptionIndex";
    public static final String FIELD_GLOSSY = "glossy";

    private AppConstants() {
    }
}
