package by.treining.ksa.oop.association.task4.model;

import java.util.List;

public class Customer {
    private String name;
    private List<BankAccount> bankAccountList;


    public Customer(String name, List<BankAccount> bankAccountList) {
        this.name = name;
        this.bankAccountList = bankAccountList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BankAccount> getBankAccountList() {
        return bankAccountList;
    }
}
