package by.training.sadko;

import by.training.sadko.builder.BookletBuilder;
import by.training.sadko.builder.MagazineBuilder;
import by.training.sadko.builder.NewspaperBuilder;
import by.training.sadko.builder.PublicationBuilder;
import by.training.sadko.command.*;
import by.training.sadko.dal.EntitiesDAO;
import by.training.sadko.dal.SimplePublicationDAO;
import by.training.sadko.entity.Booklet;
import by.training.sadko.entity.Publication;
import by.training.sadko.entity.PublicationType;
import by.training.sadko.parser.ParserFactory;
import by.training.sadko.parser.PublicationParserFactory;
import by.training.sadko.service.EntitiesService;
import by.training.sadko.service.SimplePublicationService;
import by.training.sadko.xmlbuilder.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import static by.training.sadko.entity.PublicationType.*;

public class ApplicationContext {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);
    private static volatile ApplicationContext instance;
    private final Map<Class<?>, Object> beans = new HashMap<>();

    private ApplicationContext() {
    }

    public static ApplicationContext getInstance() {
        ApplicationContext localInstance = instance;
        if (localInstance == null) {
            synchronized (ApplicationContext.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ApplicationContext();
                }
            }
        }
        return localInstance;
    }

    public void initialize() {
        //init DAO
        EntitiesDAO<Publication> publicationDAO = SimplePublicationDAO.getInstance();
        LOGGER.info("DAO initialized");
        //init Service
        EntitiesService<Publication> publicationService = new SimplePublicationService(publicationDAO);
        LOGGER.info("Service initialized");
        //init controller
        AppCommandProvider commandProvider = new SimpleAppCommandProvider();

        AppCommand showPublicationsCommand = new ShowEntitiesCommand<>(publicationService);

        Map<PublicationType, PublicationBuilder> builderMap = new EnumMap<>(PublicationType.class);
        builderMap.put(MAGAZINE, new MagazineBuilder());
        builderMap.put(BOOKLET, new BookletBuilder());
        builderMap.put(NEWSPAPER, new NewspaperBuilder());
        ParserFactory<Publication> parserFactory = new PublicationParserFactory(builderMap);
        AppCommand uploadFileCommand = new UploadFileCommand<>(publicationService, parserFactory);

        Map<String, EntityStringBuilder<Publication>> stringBuilderMap = new HashMap<>();
        stringBuilderMap.put(MAGAZINE.getValue(), new MagazineXMLBuilder());
        stringBuilderMap.put(BOOKLET.getValue(), new BookletXMLBuilder());
        stringBuilderMap.put(NEWSPAPER.getValue(), new NewspaperXMLBuilder());
        FileBuilder<Publication> fileBuilder = new PublicationXMLBuilder(stringBuilderMap);
        AppCommand downloadFileCommand = new DownLoadFileCommand<>(publicationService, fileBuilder);

        AppCommand notFoundCommand = new NotFoundCommand();

        commandProvider.register(CommandName.SHOW_COMMAND, showPublicationsCommand);
        commandProvider.register(CommandName.UPLOAD_FILE_COMMAND, uploadFileCommand);
        commandProvider.register(CommandName.DOWNLOAD_TO_FILE_COMMAND, downloadFileCommand);
        commandProvider.register(CommandName.NOT_FOUND_COMMAND, notFoundCommand);
        LOGGER.info("Command provider initialized");

        beans.put(publicationService.getClass(), publicationService);
        beans.put(commandProvider.getClass(), commandProvider);
        LOGGER.info("beans initialized");
    }

    public void destroy() {
        beans.clear();
        LOGGER.info("Beans destroyed");
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }

}
