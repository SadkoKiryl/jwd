package by.treining.ksa.oop.simple.task6;

/*
6. Составьте описание класса для представления времени. Предусмотрте возможности установки времени и
изменения его отдельных полей (час, минута, секунда) с проверкой допустимости вводимых значений. В случае
недопустимых значений полей поле устанавливается в значение 0. Создать методы изменения времени на заданное
количество часов, минут и секунд.
*/
public class Clock {
    private int hours;
    private int minutes;
    private int seconds;

    public Clock() {
    }

    public void setTime(int hour, int minute, int second) {
        setHours(hour);
        setMinutes(minute);
        setSeconds(second);
    }

    public void setHours(int hours) {
        if (hours < 24) {
            this.hours = hours;
        } else {
            this.hours = 0;
        }
    }

    public void setMinutes(int minutes) {
        if (minutes < 60) {
            this.minutes = minutes;
        } else {
            this.minutes = 0;
        }
    }

    public void setSeconds(int seconds) {
        if (seconds < 60) {
            this.seconds = seconds;
        } else {
            this.seconds = 0;
        }
    }

    public void timeChanging(int hours, int minutes, int seconds) {
        hourChanging(hours);
        minuteChanging(minutes);
        secondChanging(seconds);
    }

    public void hourChanging(int hour) {
        int changedHour = this.hours + hour;
        if (changedHour < 24) {
            this.hours = changedHour;
        } else {
            this.hours = 0;
        }
    }

    public void minuteChanging(int minutes) {
        int changedMinutes = this.minutes + minutes;
        if (changedMinutes < 60) {
            this.minutes = changedMinutes;
        } else {
            this.minutes = 0;
        }
    }

    public void secondChanging(int seconds) {
        int changedSeconds = this.seconds + seconds;
        if (changedSeconds < 60) {
            this.seconds = changedSeconds;
        } else {
            this.seconds = 0;
        }
    }

    @Override
    public String toString() {
        return "Clock{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }
}
