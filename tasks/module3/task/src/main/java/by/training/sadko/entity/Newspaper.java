package by.training.sadko.entity;


public class Newspaper extends Publication {

    private int subscriptionIndex;

    public Newspaper() {
        super.type = PublicationType.NEWSPAPER.getValue();
    }

    public int getSubscriptionIndex() {
        return subscriptionIndex;
    }

    public void setSubscriptionIndex(int subscriptionIndex) {
        this.subscriptionIndex = subscriptionIndex;
    }

    @Override
    public String toString() {
        return "Newspaper{" +
                "id='" + id + '\'' +
                ", publicationType=" + type +
                ", contentType=" + contentType +
                ", title='" + title + '\'' +
                ", isMonthly=" + isMonthly +
                ", isColored=" + isColored +
                ", volume=" + volume +
                ", subscriptionIndex=" + subscriptionIndex +
                "}";
    }
}
