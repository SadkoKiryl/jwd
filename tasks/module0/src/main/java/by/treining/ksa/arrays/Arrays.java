package by.treining.ksa.arrays;

import java.util.ArrayList;

public class Arrays {
    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
        task11();
        task12();
        task13();
        task14();
        task15();
    }

    private static void task1() {
        System.out.println("\n\n1. В массив A [N] занесены натуральные числа. Найти сумму тех элементов, которые кратны данному К.");
        int k = 5;
        int[] array = {5, 2, 10, 24, 35, 6, 8, 12};
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % k == 0) {
                sum += array[i];
            }
        }
        System.out.println("The sum is " + sum);
    }

    private static void task2() {
        System.out.println("\n\n2. В целочисленной последовательности есть нулевые элементы. Создать массив из номеров этих элементов.");
        int[] list = {5, 7, 0, 0, 67, 0, 35};
        ArrayList<Integer> newArray = new ArrayList<>();


        for (int i = 0; i < list.length; i++) {
            if (0 == list[i]) {
                newArray.add(i);
            }
        }

        System.out.print("The index of array with 0: ");
        for (int i : newArray) {
            System.out.print(i + " ");
        }
    }

    private static void task3() {
        System.out.println("\n\n3. Дана последовательность целых чисел а1 а2,..., аn . Выяснить, какое число встречается раньше - положительное или отрицательное.");
        int[] array = {-1, 0, 1};

        boolean flag = true; //true - положительное, false - отрицательное

        for (int i : array) {
            if (i < 0) {
                flag = false;
            }
        }

        if (flag) {
            System.out.println("The first is positive");
        } else {
            System.out.println("The first is negative");
        }
    }

    private static void task4() {
        System.out.println("\n\n4. Дана последовательность действительных чисел а1 а2 ,..., аn . Выяснить, будет ли она возрастающей.");
        int[] array = {-10, -12, -13};
        boolean flag = true;

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                flag = false;
            }
        }

        System.out.println("This sequence is increasing: " + flag);
    }

    private static void task5() {
        System.out.println("\n\n5. Дана последовательность натуральных чисел а1 , а2 ,..., ап. Создать массив из четных чисел этой последовательности. Если таких чисел нет, то вывести сообщение об этом факте.");
        int[] array = {5, 12, 343, 634, 8, 234, 123, 98, 348, 123, 854345, 0, 2344};
        ArrayList<Integer> evenNum = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                evenNum.add(array[i]);
            }
        }

        System.out.print("The array with even numbers: ");
        for (int i : evenNum) {
            System.out.print(i + ", ");
        }
    }

    private static void task6() {
        System.out.println("\n\n6. Дана последовательность чисел а1 ,а2 ,..., ап. Указать наименьшую длину числовой оси, содержащую все эти числа.");
        int[] array = {23, 34, 64, 85, 78, 45};
        int max = array[0];
        int min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }
        int length = max - min;
        System.out.println("The length is " + length);

    }

    private static void task7() {
        System.out.println("\n\n7. Дана последовательность действительных чисел а1 ,а2 ,..., ап. Заменить все ее члены, большие данного Z, этим числом. Подсчитать количество замен.");
        int count = 0;
        int z = 0;
        int[] an = {-3, -12, -4, 0, 12, 23};
        for (int i = 0; i < an.length; i++) {
            if (z < an[i]) {
                an[i] = z;
                count++;
            }
        }
        System.out.println("Z = " + z);
        System.out.print("The sequence is : ");
        for (int i : an) {
            System.out.print(i + " ");
        }
        System.out.println("\nNumber of replacements is " + count);
    }

    private static void task8() {
        System.out.println("\n\n8. Дан массив действительных чисел, размерность которого N. Подсчитать, сколько в нем отрицательных, положительных и нулевых элементов.");
        int[] array = {-5, 0, 9, -7, -3, 9, 23, 5};
        int pos = 0;
        int neg = 0;
        int zero = 0;

        for (int i : array) {
            if (i > 0) {
                pos++;
            }
            if (i < 0) {
                neg++;
            }
            if (i == 0) {
                zero++;
            }
        }

        System.out.println("positive - " + pos + "\nnegative - " + neg + "\nzero - " + zero);
    }

    private static void task9() {
        System.out.println("\n\n9. Даны действительные числа а1 ,а2 ,..., аn . Поменять местами наибольший и наименьший элементы.");
        int[] array = {3, 4, 12, 0, 124, 125, 1};
        int max = array[0];
        int indexMax = 0;
        int min = array[0];
        int indexMin = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                indexMax = i;
            }
            if (array[i] < min) {
                min = array[i];
                indexMin = i;
            }
        }

        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println("");
        array[indexMax] = min;
        array[indexMin] = max;
        for (int i : array) {
            System.out.print(i + " ");
        }
    }

    private static void task10() {
        System.out.println("\n\n10. Даны целые числа а1 ,а2 ,..., аn . Вывести на печать только те числа, для которых аi > i.");
        int[] an = {6, 5, 10, 24, -5, 0, 12};

        for (int i : an) {
            System.out.print(i + " ");
        }

        System.out.println();

        for (int i = 0; i < an.length; i++) {

            if (an[i] > i) {
                System.out.print(an[i] + " ");
            }
        }
    }

    private static void task11() {
        System.out.println("\n\n11. Даны натуральные числа а1 ,а2 ,..., аn . Указать те из них, у которых остаток от деления на М равен L (0 < L < М-1).");
        int m = 24;

        int[] an = {10, -3, 80, 0, -95, 12, 3, 0, 23};

        for (int i : an) {
            System.out.print(i + " ");
        }

        System.out.println("M = " + m);

        for (int i = 0; i < an.length; i++) {
            if (0 < an[i] % m && an[i] % m < (m - 1)) {
                System.out.print(an[i] + " ");
            }
        }
    }

    private static void task12() {
        System.out.println("\n\n12. Задана последовательность N вещественных чисел. Вычислить сумму чисел, порядковые номера которых являются простыми числами.");
        double[] an = {0.12, -2.75, -7.24, 10.19, 24.5, 187, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19};
        int sum = 0;

        System.out.println("The indexes are prime: ");
        for (int i = 2; i < an.length; i++) {
            boolean isPrime = true;

            for (int j = 2; j < i; j++) {

                if (i % j == 0 && i != 2) {
                    isPrime = false;
                }
            }

            if (isPrime) {
                sum += an[i];
                System.out.print(i + " ");
            }
        }

        System.out.println("\nSum is " + sum);
    }

    private static void task13() {
        System.out.println("\n\n13. Определить количество элементов последовательности натуральных чисел, кратных числу М и заключенных в промежутке от L до N.\n");
        int m = 3;
        int k = 3;
        int n = 12;

        int[] arr = {12, 24, 7, 9, 65, 15, -6, 8, -27, 3};
        int count = 0;

        System.out.print("The array is: ");
        for (int i : arr) {
            System.out.print(i + " ");
        }

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % m == 0 && arr[i] >= k && arr[i] <= n) {
                count++;
            }
        }
        System.out.println("\nThe count is " + count);
    }

    private static void task14() {
        System.out.println("\n\nДан одномерный массив A[N]. Найти: max(четных эллементов) + min(нечетных эллементов)");
        int[] arr = {12, 24, 7, 9, 65, 15, -6, 8, -27, 3};

        System.out.print("The array is: ");
        for (int i : arr) {
            System.out.print(i + " ");
        }

        int max = arr[1];
        int min = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0) {
                if (min > arr[i]) {
                    min = arr[i];
                }
            } else {
                if (arr[i] > max) {
                    max = arr[i];
                }
            }
        }
        int result = max + min;
        System.out.println("\nThe result is " + result);
    }

    private static void task15() {
        System.out.println("\n\n15. Дана последовательность действительных чисел. Указать те ее элементы, которые принадлежат отрезку [с, d].");
        int c = -10;
        int d = 10;
        int[] arr = {12, -24, 7, 9, 65, 15, -6, 8, -27, 3, 0, 1, 10};

        System.out.print("The array is: ");
        for (int i : arr) {
            System.out.print(i + " ");
        }

        System.out.print("\nElements of the segment [" + c + ", " + d + "] : ");
        for (int i : arr) {
            if (i >= c && d >= i) {
                System.out.print(i + " ");
            }
        }
    }


}
