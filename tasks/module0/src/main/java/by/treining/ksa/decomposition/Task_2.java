package by.treining.ksa.decomposition;

/*
2. Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего кратного двух
натуральных чисел:
*/

public class Task_2 {

    public static void main(String[] args) {
        int num1 = 328;
        int num2 = 5;

        int greatestCommonDivisor = greatestCommonDivisor(num1, num2);
        int leastCommonMultiple = leastCommonMultiple(num1, num2);

        System.out.println("\nThe greatestCommonDivisor of numbers " + num1 + ", " + num2 + " is: " + greatestCommonDivisor);
        System.out.println("\nThe leastCommonMultiple of numbers " + num1 + ", " + num2 + " is: " + leastCommonMultiple);

    }

    private static int greatestCommonDivisor(int num1, int num2) {
        return num2 == 0 ? num1 : greatestCommonDivisor(num2, num1 % num2);
    }

    private static int leastCommonMultiple(int num1, int num2) {
        return num1 * num2 / greatestCommonDivisor(num1, num2);
    }
}
