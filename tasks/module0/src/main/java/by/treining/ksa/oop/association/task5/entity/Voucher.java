package by.treining.ksa.oop.association.task5.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class Voucher {

    private static int count = 0;

    private int id;
    private Direction direction;
    private VoucherType voucherType;
    private Transport transportType;
    private Food foodType;
    private AmountOfDays amountOfDays;
    private BigDecimal totalPrice;

    public Voucher(VoucherType voucherType, Direction direction, Transport transportType, Food foodType,
                   AmountOfDays amountOfDays) {

        this.id = ++count;
        this.direction = direction;
        this.voucherType = voucherType;
        this.transportType = transportType;
        this.foodType = foodType;
        this.amountOfDays = amountOfDays;

        BigDecimal roadCost = transportType.getCostPerKilometer().multiply(BigDecimal.valueOf(direction.getDistance()));
        BigDecimal voucherCost = voucherType.getCostPerDay().multiply(BigDecimal.valueOf(amountOfDays.getValue()));
        BigDecimal foodCost = foodType.getCostPerDay().multiply(BigDecimal.valueOf(amountOfDays.getValue()));

        this.totalPrice = roadCost.add(voucherCost.add(foodCost));
    }

    public Direction getDirection() {
        return direction;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public Transport getTransportType() {
        return transportType;
    }

    public Food getFoodType() {
        return foodType;
    }

    public AmountOfDays getAmountOfDays() {
        return amountOfDays;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }


    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher = (Voucher) o;
        return id == voucher.id &&
                direction == voucher.direction &&
                voucherType == voucher.voucherType &&
                transportType == voucher.transportType &&
                foodType == voucher.foodType &&
                amountOfDays == voucher.amountOfDays &&
                Objects.equals(totalPrice, voucher.totalPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, direction, voucherType, transportType, foodType, amountOfDays, totalPrice);
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "id=" + id +
                ", direction=" + direction +
                ", voucherType=" + voucherType +
                ", transportType=" + transportType +
                ", foodType=" + foodType +
                ", amountOfDays=" + amountOfDays +
                ", totalPrice=" + totalPrice + " USD" +
                '}';
    }
}
