package by.training.sadko.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class XSDValidator implements by.training.sadko.validator.Validator<InputStream> {

    private static final Logger LOGGER = LogManager.getLogger(XSDValidator.class);

    public boolean validate(InputStream xmlInputStream) {
        File file = new File(getClass().getClassLoader().getResource("schema.xsd").getFile());
        String xsdPath = file.getAbsolutePath();

        try {
            SchemaFactory factory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsdPath));
            Validator validator = schema.newValidator();
            StreamSource source = new StreamSource(xmlInputStream);
            validator.validate(source);
            LOGGER.info("The file is valid");

        } catch (IOException e) {
            LOGGER.error("IOException while reading xml file", e);
            return false;
        } catch (SAXException e1) {
            LOGGER.error("SAXException while validating xml file", e1);
            return false;
        }
        return true;
    }
}
