package by.training.sadko.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumMap;
import java.util.Map;


public class SimpleAppCommandProvider implements AppCommandProvider {

    private static final Logger LOGGER = LogManager.getLogger(SimpleAppCommandProvider.class);
    private final Map<CommandName, AppCommand> commandMap;

    public SimpleAppCommandProvider() {
        commandMap = new EnumMap<>(CommandName.class);
    }

    public SimpleAppCommandProvider(Map<CommandName, AppCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public void register(CommandName commandName, AppCommand appCommand) {
        commandMap.put(commandName, appCommand);
    }

    @Override
    public void remove(CommandName commandName) {
        commandMap.remove(commandName);
    }

    @Override
    public AppCommand getCommand(String commandFromRequest) {
        CommandName commandName = CommandName.fromString(commandFromRequest);
        LOGGER.info("CommandName from request is \"{}\"", commandName.getStringName());
        return commandMap.get(commandName);
    }
}
