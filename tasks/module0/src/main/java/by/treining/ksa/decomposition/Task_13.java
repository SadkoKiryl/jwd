package by.treining.ksa.decomposition;

/*
13. Дано натуральное число N. Написать метод(методы) для формирования массива, элементами которого являются
        цифры числа N.
*/
public class Task_13 {

    public static void main(String[] args) {
        int n = 5238;

        createArray(n);
    }

    private static void createArray(int n) {

        String num = String.valueOf(n);

        String[] array = num.split("");
        System.out.println("The array from digits of number " + n + ":");
        for (String s : array) {
            System.out.print(s + " ");
        }
    }
}
