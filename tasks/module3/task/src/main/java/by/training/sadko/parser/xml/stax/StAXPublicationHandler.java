package by.training.sadko.parser.xml.stax;

import by.training.sadko.builder.PublicationBuilder;
import by.training.sadko.entity.Publication;
import by.training.sadko.entity.PublicationType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static by.training.sadko.AppConstants.*;
import static javax.xml.stream.XMLStreamConstants.*;

public class StAXPublicationHandler extends StAXHandler<Publication> {

    private static final Logger LOGGER = LogManager.getLogger(StAXPublicationHandler.class);
    private final Map<PublicationType, PublicationBuilder> builderMap;

    public StAXPublicationHandler(Map<PublicationType, PublicationBuilder> builderMap) {
        this.entityFromXML = new ArrayList<>();
        this.builderMap = builderMap;
    }

    @Override
    public List<Publication> getListFromXML(XMLEventReader xmlEventReader) {
        PublicationBuilder publicationBuilder = null;
        Optional<PublicationBuilder> optionalBuilder;
        boolean isContentTypeElement = false;
        boolean isTitleElement = false;
        boolean isMonthlyElement = false;
        boolean isColoredElement = false;
        boolean isVolumeElement = false;
        boolean isGlossyElement = false;
        boolean isSubscriptionIndexElement = false;

        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = null;
            try {
                xmlEvent = xmlEventReader.nextEvent();
            } catch (XMLStreamException e) {
                LOGGER.error("XMLStreamException", e);
            }
            optionalBuilder = Optional.ofNullable(publicationBuilder);

            switch (xmlEvent.getEventType()) {
                case START_ELEMENT:
                    StartElement startElement = xmlEvent.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    PublicationType[] values = PublicationType.values();
                    for (PublicationType type : values) {
                        if (qName.equalsIgnoreCase(type.getValue())) {
                            publicationBuilder = builderMap.get(type);
                            publicationBuilder.setID(startElement.getAttributes().next().getValue());
                        }
                    }
                    if (qName.equalsIgnoreCase(FIELD_CONTENT_TYPE)) {
                        isContentTypeElement = true;
                    } else if (qName.equalsIgnoreCase(FIELD_TITLE)) {
                        isTitleElement = true;
                    } else if (qName.equalsIgnoreCase(FIELD_MONTHLY)) {
                        isMonthlyElement = true;
                    } else if (qName.equalsIgnoreCase(FIELD_COLORED)) {
                        isColoredElement = true;
                    } else if (qName.equalsIgnoreCase(FIELD_VOLUME)) {
                        isVolumeElement = true;
                    } else if (qName.equalsIgnoreCase(FIELD_GLOSSY)) {
                        isGlossyElement = true;
                    } else if (qName.equalsIgnoreCase(FIELD_SUBSCRIPTION_INDEX)) {
                        isSubscriptionIndexElement = true;
                    }
                    break;

                case CHARACTERS:
                    Characters characters = xmlEvent.asCharacters();
                    if (optionalBuilder.isPresent()) {
                        if (isContentTypeElement) {
                            optionalBuilder.get().setContentType(characters.getData());
                            isContentTypeElement = false;
                        } else if (isTitleElement) {
                            optionalBuilder.get().setTitle(characters.getData());
                            isTitleElement = false;
                        } else if (isMonthlyElement) {
                            optionalBuilder.get().setIsMonthly(characters.getData());
                            isMonthlyElement = false;
                        } else if (isColoredElement) {
                            optionalBuilder.get().setIsColored(characters.getData());
                            isColoredElement = false;
                        } else if (isVolumeElement) {
                            optionalBuilder.get().setVolume(characters.getData());
                            isVolumeElement = false;
                        } else if (isGlossyElement) {
                            optionalBuilder.get().setIsGlossy(characters.getData());
                            isGlossyElement = false;
                        } else if (isSubscriptionIndexElement) {
                            optionalBuilder.get().setSubscriptionIndex(characters.getData());
                            isSubscriptionIndexElement = false;
                        }
                    }
                    break;

                case END_ELEMENT:
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equalsIgnoreCase(TAG_MAGAZINE)
                            || endElement.getName().getLocalPart().equalsIgnoreCase(TAG_BOOKLET)
                            || endElement.getName().getLocalPart().equalsIgnoreCase(TAG_NEWSPAPER)) {
                        optionalBuilder.ifPresent(builder -> entityFromXML.add(builder.getEntity()));
                    }
                    break;
            }
        }
        return entityFromXML;
    }
}
