package by.treining.ksa.oop.association.task3.model;

import java.util.ArrayList;
import java.util.List;

public class Region {
    private String regionName;
    private List<City> cityList;
    private City regionCenter;
    private int regionArea;

    public Region(String regionName, List<City> cityList) {
        this.regionName = regionName;
        this.cityList = cityList;

        for (City city : cityList) {
            if (city.isRegionCenter()) {
                this.regionCenter = city;
            }

            regionArea += city.getCityArea();
        }
    }

    public Region(String regionName, City city) {
        this.regionName = regionName;
        this.cityList = new ArrayList<>();
        cityList.add(city);
        this.regionCenter = city;
        regionArea = city.getCityArea();
    }

    public City getRegionCenter() {
        return regionCenter;
    }


    public int getRegionArea() {
        return regionArea;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;

        for (City city : this.cityList) {
            if (city.isRegionCenter()) {
                this.regionCenter = city;
                break;
            }
        }

        for (City city : cityList) {
            if (city.isRegionCenter()) {
                this.regionCenter = regionCenter;
            }
            regionArea += city.getCityArea();
        }
    }
}

