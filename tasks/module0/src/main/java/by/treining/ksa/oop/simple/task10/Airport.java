package by.treining.ksa.oop.simple.task10;

import java.util.Calendar;

/*
10. Создать класс Airline, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы
и метод toString(). Создать второй класс, агрегирующий массив типа Airline, с подходящими конструкторами и
методами. Задать критерии выбора данных и вывести эти данные на консоль.
*/
/*
Airline: пункт назначения, номер рейса, тип самолета, время вылета, дни недели.
        Найти и вывести:
        a) список рейсов для заданного пункта назначения;
        b) список рейсов для заданного дня недели;
        c) список рейсов для заданного дня недели, время вылета для которых больше заданного.
*/
public class Airport {
    private Airline[] airlines;

    public Airport(Airline[] airlines) {
        this.airlines = airlines;
    }

    public void searchByDestination(String destination) {
        for (Airline airline : airlines) {
            if (destination.equals(airline.getDestination())) {
                System.out.println(airline.toString());
            }
        }
    }

    public void searchByDayOfWeek(DayOfWeek dayOfWeek) {
        for (Airline airline : airlines) {
            if (dayOfWeek.equals(airline.getDayOfWeek())) {
                System.out.println(airline.toString());
            }
        }
    }

    public void searchByDayAndDepartureTime(DayOfWeek dayOfWeek, Calendar departureTime) {
        for (Airline airline : airlines) {
            if (dayOfWeek.equals(airline.getDayOfWeek()) && airline.getDepartureTime().after(departureTime)) {
                System.out.println(airline.toString());
            }
        }
    }

    public Airline[] getAirlines() {
        return airlines;
    }

    public void setAirlines(Airline[] airlines) {
        this.airlines = airlines;
    }
}
