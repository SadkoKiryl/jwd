package by.treining.ksa.oop.basics.task1.model;

import java.util.ArrayList;
import java.util.List;

public class TextFile extends File {

    private List<String> body;

    public TextFile(String fileName, Directory directory, List<String> body) {
        super(fileName, directory);
        this.body = body;
    }

    public TextFile(String fileName, Directory directory) {
        super(fileName, directory);
        this.body = new ArrayList<>();
    }

    public List<String> getBody() {
        return body;
    }

    public void setBody(List<String> body) {
        this.body = body;
    }
}
