package by.treining.ksa.oop.association.task5.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Transport {

    BUS("bus", new BigDecimal(0.01).setScale(2, RoundingMode.HALF_UP)),
    AIRPLANE("airplane", new BigDecimal(0.05).setScale(2, RoundingMode.HALF_UP)),
    SHIP("ship", new BigDecimal(0.07).setScale(2, RoundingMode.HALF_UP));

    private String transportType;
    private BigDecimal costPerKilometer;

    Transport(String transportType, BigDecimal costPerKilometer) {
        this.transportType = transportType;
        this.costPerKilometer = costPerKilometer;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public BigDecimal getCostPerKilometer() {
        return costPerKilometer;
    }

    public void setCostPerKilometer(BigDecimal costPerKilometer) {
        this.costPerKilometer = costPerKilometer;
    }
}
