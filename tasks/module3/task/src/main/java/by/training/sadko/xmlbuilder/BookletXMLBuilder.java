package by.training.sadko.xmlbuilder;

import by.training.sadko.entity.Booklet;
import by.training.sadko.entity.Publication;

public class BookletXMLBuilder implements EntityStringBuilder<Publication> {

    @Override
    public String build(Publication publication) {
        Booklet booklet = (Booklet) publication;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<Booklet id=\"").append(booklet.getId()).append("\">\n")
                .append("<title>").append(booklet.getTitle()).append("</title>\n")
                .append("<monthly>").append(booklet.isMonthly()).append("</monthly>\n")
                .append("<colored>").append(booklet.isColored()).append("</colored>\n")
                .append("<content>").append(booklet.getContentType()).append("</content>\n")
                .append("<volume>").append(booklet.getVolume()).append("</volume>\n")
                .append("<type>").append(booklet.getType()).append("</type>\n")
                .append("<glossy>").append(booklet.isGlossy()).append("</glossy>\n")
                .append("</Booklet>\n");

        return stringBuffer.toString();
    }
}
