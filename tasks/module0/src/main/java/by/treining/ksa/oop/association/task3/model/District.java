package by.treining.ksa.oop.association.task3.model;

public class District {
    private String districtName;
    private int districtArea;

    public District(String districtName, int districtArea) {
        this.districtName = districtName;
        this.districtArea = districtArea;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public int getDistrictArea() {
        return districtArea;
    }

    public void setDistrictArea(int districtArea) {
        this.districtArea = districtArea;
    }
}
