package by.training.sadko.listener;

import by.training.sadko.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

//@WebListener
public class AppListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(AppListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext.getInstance().initialize();
        String contextEvent = sce.toString();
        LOGGER.info("Context initialized {}", contextEvent);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ApplicationContext.getInstance().destroy();
        String contextEvent = sce.toString();
        LOGGER.info("Context destroyed {}", contextEvent);
    }
}
