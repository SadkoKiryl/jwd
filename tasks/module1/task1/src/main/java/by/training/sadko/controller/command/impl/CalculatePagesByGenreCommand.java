package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;
import by.training.sadko.service.LiteratureService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.AppConstants.PARAM_GENRE;

public class CalculatePagesByGenreCommand implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(CalculatePagesByGenreCommand.class.getName());
    private final LiteratureService service;

    public CalculatePagesByGenreCommand(LiteratureService literatureService) {
        this.service = literatureService;
    }

    @Override
    public String execute(Map<String, String> requestMap) {
        String genreName = requestMap.get(PARAM_GENRE);
        Genre genre = Genre.fromString(genreName);
        long sumPagesByGenre = service.calculatingPagesByGenre(genre);
        LOGGER.info("Sum pages by");
        return assemblyResponseString(sumPagesByGenre, genre);
    }

    private String assemblyResponseString(long sumPages, Genre genre) {
        StringBuilder responseString = new StringBuilder();
        if (genre != null) {
            LOGGER.info("The Sum of pages by {} was calculated", genre);
            List<Literature> literatureByGenre = service.selectionLiteratureByGenre(genre);
            for (Literature tempLiterature : literatureByGenre) {
                responseString
                        .append("<li>")
                        .append(tempLiterature.toString())
                        .append("</li>");
            }
            responseString
                    .append("<br/><p align=\"center\"><b>The amount of pages of literature in the ")
                    .append(genre.getName())
                    .append(" are ")
                    .append(sumPages)
                    .append("</b></p>");

        } else {
            LOGGER.info("This genre {} is not found", genre);
            responseString
                    .append("<p align=\"center\"><b>This genre is not found in the library, try again</b></p>")
                    .append("<form action=\"/home\" align=\"center\">\n")
                    .append("  <input type=\"text\"  name=" + PARAM_GENRE)
                    .append(" value=\"\">\n")
                    .append("  <input type=\"hidden\"  name=")
                    .append(COMMAND_NAME)
                    .append(" value=\"calc\">\n")
                    .append("  <br>\n\n")
                    .append("  <input type=\"submit\" value=\"Calculate\">\n")
                    .append("</form> ");
        }
        return responseString.toString();
    }
}
