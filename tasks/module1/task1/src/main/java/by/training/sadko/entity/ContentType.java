package by.training.sadko.entity;

public enum ContentType {

    CHAPTER("chapter"),
    ARTICLE("article"),
    COMPOSITION("composition");

    private final String name;

    ContentType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
