package by.training.sadko;

public class AppConstants {

    public static final String URL = "localhost";
    public static final int PORT = 8081;
    public static final String COMMAND_NAME = "command";
    public static final String PATH = "path";
    public static final String SORTING_OPTIONS = "options";
    public static final String PARAM_ID = "id";
    public static final String PARAM_GENRE = "genre";


    private AppConstants() {
    }
}
