package by.training.sadko.builder;

import by.training.sadko.entity.ContentType;
import by.training.sadko.entity.Magazine;

public class MagazineBuilder implements PublicationBuilder {

    private Magazine magazine;

    public MagazineBuilder() {
        magazine = new Magazine();
    }

    @Override
    public void setID(String value) {
        magazine.setId(value);
    }

    @Override
    public void setContentType(String contentType) {
        magazine.setContentType(ContentType.fromString(contentType));
    }

    @Override
    public void setTitle(String value) {
        magazine.setTitle(value);
    }

    @Override
    public void setIsMonthly(String isMonthly) {
        magazine.setMonthly(Boolean.parseBoolean(isMonthly));
    }

    @Override
    public void setIsColored(String isColored) {
        magazine.setColored(Boolean.parseBoolean(isColored));
    }

    @Override
    public void setVolume(String volume) {
        magazine.setVolume(Integer.parseInt(volume));
    }

    @Override
    public void setSubscriptionIndex(String subscriptionIndex) {
        magazine.setSubscribingIndex(Integer.parseInt(subscriptionIndex));
    }

    @Override
    public void setIsGlossy(String isGlossy) {
        magazine.setGlossy(Boolean.parseBoolean(isGlossy));
    }

    @Override
    public Magazine getEntity() {
        Magazine copy = magazine;
        magazine = new Magazine();
        return copy;
    }
}
