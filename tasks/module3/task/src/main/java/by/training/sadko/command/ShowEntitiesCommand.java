package by.training.sadko.command;

import by.training.sadko.service.EntitiesService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowEntitiesCommand<T> implements AppCommand {

    private static final Logger LOGGER = LogManager.getLogger(ShowEntitiesCommand.class);
    private final EntitiesService<T> service;

    public ShowEntitiesCommand(EntitiesService<T> service) {
        this.service = service;
    }

    @Override
    public String execute(HttpServletRequest req, HttpServletResponse resp) {
        List<T> all = service.getEntitiesList();
        if (!all.isEmpty()) {
            req.setAttribute("_list", all);
        } else {
            req.setAttribute("list_result", "Repository is empty");
            LOGGER.info("Repository is empty");
        }
        return "list_view";
    }
}
