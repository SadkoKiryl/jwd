package by.training.sadko.builder;

import by.training.sadko.entity.Booklet;
import by.training.sadko.entity.ContentType;
import by.training.sadko.entity.Publication;

public class BookletBuilder implements PublicationBuilder {

    private Booklet booklet;

    public BookletBuilder() {
        booklet = new Booklet();
    }

    @Override
    public void setID(String value) {
        booklet.setId(value);
    }

    @Override
    public void setContentType(String contentType) {
        booklet.setContentType(ContentType.fromString(contentType));
    }

    @Override
    public void setTitle(String value) {
        booklet.setTitle(value);
    }

    @Override
    public void setIsMonthly(String isMonthly) {
        booklet.setMonthly(Boolean.parseBoolean(isMonthly));
    }

    @Override
    public void setIsColored(String isColored) {
        booklet.setColored(Boolean.parseBoolean(isColored));
    }

    @Override
    public void setVolume(String volume) {
        booklet.setVolume(Integer.parseInt(volume));
    }

    @Override
    public void setSubscriptionIndex(String subscriptionIndex) {
        //not exists
    }

    @Override
    public void setIsGlossy(String isGlossy) {
        booklet.setGlossy(Boolean.parseBoolean(isGlossy));
    }

    @Override
    public Publication getEntity() {
        Publication copy = booklet;
        booklet = new Booklet();
        return copy;
    }
}
