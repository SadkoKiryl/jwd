package by.training.sadko.command;

public class AppCommandException extends Exception {

    public AppCommandException(String message) {
        super(message);
    }

    public AppCommandException(Throwable cause) {
        super(cause);
    }
}
