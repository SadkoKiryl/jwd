package by.training.sadko.command.xml.dom;

import by.training.sadko.parser.xml.dom.DOMPublicationHandler;
import by.training.sadko.parser.xml.dom.DomParser;
import by.training.sadko.entity.Publication;
import by.training.sadko.entity.PublicationType;
import by.training.sadko.builder.BookletBuilder;
import by.training.sadko.builder.MagazineBuilder;
import by.training.sadko.builder.NewspaperBuilder;
import by.training.sadko.builder.PublicationBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DOMParserTest {

    private DomParser<Publication> parser;

    @Before
    public void setUp() {
        Map<PublicationType, PublicationBuilder> builderMap = new HashMap<>();
        builderMap.put(PublicationType.MAGAZINE, new MagazineBuilder());
        builderMap.put(PublicationType.BOOKLET, new BookletBuilder());
        builderMap.put(PublicationType.NEWSPAPER, new NewspaperBuilder());
        DOMPublicationHandler publicationHandler = new DOMPublicationHandler(builderMap);
        parser = new DomParser<>(publicationHandler);
    }

    @Test
    public void parsingShouldReturnListWithElements() throws FileNotFoundException {
        List<Publication> publicationList;
        publicationList = parser.parse(new FileInputStream("src\\test\\resources\\example.xml"));
        Assert.assertEquals(16, publicationList.size());
    }
}
