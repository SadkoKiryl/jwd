package validator;

import by.training.sadko.controller.command.validator.PathFileValidator;
import by.training.sadko.controller.command.validator.Validator;
import org.junit.Assert;
import org.junit.Test;

public class PathFileValidatorTest {
    @Test
    public void textFileShouldBeValid() {
        Validator<String> pathValidator = new PathFileValidator();
        String pathToTxt = "src/test/resources/list.txt";
        String pathToCsv = "src\\test\\resources\\list.csv";
        Assert.assertTrue(pathValidator.isValid(pathToTxt));
        Assert.assertTrue(pathValidator.isValid(pathToCsv));
    }

    @Test
    public void htmlFileShouldBeInvalid() {
        Validator<String> pathValidator = new PathFileValidator();
        String pathToHtml = "src/test/resources/list.html";
        Assert.assertFalse(pathValidator.isValid(pathToHtml));
    }
}
