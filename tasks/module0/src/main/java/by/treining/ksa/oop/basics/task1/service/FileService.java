package by.treining.ksa.oop.basics.task1.service;

import by.treining.ksa.oop.basics.task1.model.Directory;
import by.treining.ksa.oop.basics.task1.model.File;

public interface FileService {
    public void create(Directory directory, String fileName);
    public void rename(Directory directory, String fileName, String newFileName);
    public void dell(Directory directory, String fileName);
}
