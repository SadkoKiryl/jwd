package by.treining.ksa.oop.simple.task10;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        Airline airline1 = new Airline("Moscow", 111, AirPlane.CIVILIAN_AIRCRAFT,
                new GregorianCalendar(2020, Calendar.MARCH, 10, 16, 20),
                DayOfWeek.MONDAY);

        Airline airline2 = new Airline("London", 112, AirPlane.CIVILIAN_AIRCRAFT,
                new GregorianCalendar(2020, Calendar.MARCH, 11, 21, 25),
                DayOfWeek.THURSDAY);

        Airline airline3 = new Airline("Berlin", 113, AirPlane.WAR_PLANE,
                new GregorianCalendar(2020, Calendar.MARCH, 11, 8, 50),
                DayOfWeek.THURSDAY);

        Airline airline4 = new Airline("Piking", 117, AirPlane.CARGO_AIRPLANE,
                new GregorianCalendar(2020, Calendar.MARCH, 13, 12, 24),
                DayOfWeek.FRIDAY);

        Airline airline5 = new Airline("Brest", 221, AirPlane.WAR_PLANE,
                new GregorianCalendar(2020, Calendar.MARCH, 13, 3, 38),
                DayOfWeek.FRIDAY);

        Airline[] airlines = {airline1, airline2, airline3, airline4, airline5};

        Airport airport = new Airport(airlines);

        airport.searchByDestination("Berlin");
        System.out.println("-----------------");

        airport.searchByDayOfWeek(DayOfWeek.FRIDAY);
        System.out.println("-----------------");

        airport.searchByDayAndDepartureTime(DayOfWeek.THURSDAY,
                new GregorianCalendar(2020, Calendar.MARCH, 11, 18, 00));
    }
}
