package service;

import by.training.sadko.dao.InMemoryLiteratureDao;
import by.training.sadko.dao.LiteratureDao;
import by.training.sadko.entity.Genre;
import by.training.sadko.entity.Literature;
import by.training.sadko.service.SimpleLiteratureService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class SimpleLiteratureServiceTest {

    private static SimpleLiteratureService service;
    private static Genre[] genres;

    @BeforeClass
    public static void setUp() {
        LiteratureDao literatureDao = new InMemoryLiteratureDao();
        genres = Genre.values();
        service = new SimpleLiteratureService(literatureDao);
    }

    @Test
    public void sumOfPagesShouldBePositive() {
        for (Genre genre : genres) {
            long sumOfPages = service.calculatingPagesByGenre(genre);
            Assert.assertTrue(sumOfPages >= 0);
        }
    }

    @Test
    public void literatureSelectedByGenreShouldHaveSingleGenre() {
        for (Genre genre : genres) {
            List<Literature> literatureList = service.selectionLiteratureByGenre(genre);

            for (Literature literature : literatureList) {
                Genre tempGenre = literatureList.get(0).getGenre();
                Assert.assertEquals(tempGenre, literature.getGenre());
            }
        }
    }

    @Test
    public void sameLiteratureShouldNotAddedToRepository() {
        int sizeOfRepository = service.getAllLiterature().size();
        service.addLiterature(service.getAllLiterature());
        int sizeAfterAddition = service.getAllLiterature().size();
        Assert.assertEquals(sizeAfterAddition, sizeOfRepository);
    }
}


