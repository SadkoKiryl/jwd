package by.treining.ksa.basics;

import java.util.Scanner;

import static java.lang.Math.*;

public class SolutionBranching {

    public static void main(String[] args) {

        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
        task11();
        task12();
        task13();
        task14();
        task15();
        task16();
        task17();
        task18();
        task19();
        task20();
        task21();
        task22();
        task23();
        task24();
        task25();
        task26();
        task27();
        task28();
        task29();
        task30();
        task32();

    }

    private static void task1() {

        System.out.println("\n1. Составить программу сравнения двух чисел 1 и 2. Если 1 меньше 2 – вывести на экран цифру 7, в противном случае – цифру 8.n");
        short a = 1;
        short b = 2;
        if (2 > 1) {
            System.out.println(7);
        } else {
            System.out.println(8);
        }
    }

    private static void task2() {

        System.out.println("\n\n2. Составить программу сравнения двух чисел 1 и 2. Если 1 меньше 2 – вывести на экран слово «yes», в противном случае – слово «no»");
        short a = 1;
        short b = 2;
        if (2 > 1) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    private static void task3() {

        System.out.println("\n\n3. Составить программу сравнения введенного числа а и цифры 3. Вывести на экран слово «yes», если число а меньше 3; если больше, то вывести слово «no».");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number:");
        if (3 > scanner.nextInt()) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    private static void task4() {

        System.out.println("\n\n4. Составить программу: равны ли два числа а и b?");
        int a = 4;
        int b = 12;
        if (a == b) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }

    private static void task5() {

        System.out.println("\n\n5. Составить программу: определения наименьшего из двух чисел а и b.");
        int a = 23;
        int b = 12;
        if (a >= b) {
            System.out.println("The min is b");
        } else {
            System.out.println("The min is a");
        }
    }

    private static void task6() {

        System.out.println("\n\n6. Составить программу: определения наибольшего из двух чисел а и b.");
        int a = 23;
        int b = 12;
        if (a >= b) {
            System.out.println("The min is a");
        } else {
            System.out.println("The min is b");
        }
    }

    private static void task7() {

        System.out.println("\n\n7. Составить программу нахождения модуля выражения a*x*x + b*x + c при заданных значениях a, b, c и х");
        int a = 12;
        int b = 23;
        int c = -5;
        int x = 9;
        int result = a * x * x + b * x + c;
        if (result >= 0) {
            System.out.println(result);
        } else {
            System.out.println(-result);
        }
    }

    private static void task8() {

        System.out.println("\n\n8. Составить программу нахождения наименьшего из квадратов двух чисел а и b.");
        int a = -3;
        int b = 2;
        a = (int) pow(a, 2);
        b = (int) pow(b, 2);
        if (a < b) {
            System.out.println("The min is a");
        } else {
            System.out.println("The min s b");
        }
    }

    private static void task9() {

        System.out.println("\n\n9. Составить программу, которая определит по трем введенным сторонам, является ли данный треугольник равносторонним.");
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter three sides of the triangle:");
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();
        if (a + b > c && a + c > b && b + c > a) {
            System.out.println("The triangle exist.");
            if (a == b && b == c) {
                System.out.println("The triangle is equilateral");
            } else {
                System.out.println("The triangle is not equilateral");
            }
        } else {
            System.out.println("The triangle does not exist!");
        }
    }

    private static void task10() {

        System.out.println("\n\n10. Составить программу, которая определит площадь какого круга меньше.");
        int areaFirstCircle = 12;
        int areaSecondCircle = 24;
        if (areaFirstCircle < areaSecondCircle) {
            System.out.println("The area of the first circle is less");
        } else {
            System.out.println("The area of the second circle is less");
        }
    }

    private static void task11() {

        System.out.println("\n\n11. Составить программу, которая определит площадь какого треугольника больше.");
        int areaFirstTriangle = 12;
        int areaSecondTriangle = 24;
        if (areaFirstTriangle < areaSecondTriangle) {
            System.out.println("The area of the first triangle is less");
        } else {
            System.out.println("The area of the second triangle is less");
        }
    }

    private static void task12() {

        System.out.println("\n\n12. Даны три действительных числа. Возвести в квадрат те из них, значения которых неотрицательны, и в четвертую степень — отрицательные.");
        double number1 = 5.2;
        double number2 = 4.6;
        double number3 = -1;

        if (number1 >= 0) {
            number1 = pow(number1, 2);
        } else {
            number1 = pow(number1, 4);
        }

        if (number2 >= 0) {
            number2 = pow(number2, 2);
        } else {
            number2 = pow(number2, 4);
        }

        if (number3 >= 0) {
            number3 = pow(number3, 2);
        } else {
            number3 = Math.pow(number3, 4);
        }
        System.out.println("Result number1 = " + number1 + ", number2 = " + number2 + ", number3" + number3);
    }

    private static void task13() {

        System.out.println("\n\n13. Даны две точки А(х1, у1) и В(х2, у2). Составить алгоритм, определяющий, которая из точек находится ближе к началу координат.");
        double x1 = 5;
        double y1 = 5;
        double x2 = -4;
        double y2 = -7;

        double length1 = Math.sqrt(x1 * x1 + y1 * y1);
        double length2 = Math.sqrt(x2 * x2 + y2 * y2);

        if (length1 < length2) {
            System.out.println("Point (" + x1 + ", " + y1 + ") closer to origin than point (" + x2 + ", " + y2 + ")");
        } else if (length1 == length2) {
            System.out.println("Point (" + x1 + ", " + y1 + ") at the same distance from the origin as the point (" + x2 + ", " + y2 + ")");
        } else {
            System.out.println("Point (" + x2 + ", " + y2 + ") closer to origin than point (" + x1 + ", " + y1 + ")");
        }
    }

    private static void task14() {

        System.out.println("\n\n14. Даны два угла треугольника (в градусах). Определить, существует ли такой треугольник, и если да, то будет ли он прямоугольным.");
        int angle1 = 60;
        int angle2 = 30;
        int angle3 = 180 - angle1 - angle2;

        if (angle1 > 0 && angle2 > 0 && angle3 > 0) {
            System.out.println("The triangle exist");
            if (angle1 == 90 || angle2 == 90 || angle3 == 90) {
                System.out.println("The triangle is right");
            }
        } else {
            System.out.println("The triangle does not exist");
        }
    }

    private static void task15() {

        System.out.println("\n\n15. Даны действительные числа х и у, не равные друг другу. Меньшее из этих двух чисел заменить половиной их суммы, а большее — их удвоенным произведением.");
        double x = 12.0;
        double y = 3.8;
        double newX = 0.0;
        double newY = 0.0;

        if (x < y) {
            newX = (x + y) / 2;
            newY = 2 * x * y;
        } else {
            newY = (x + y) / 2;
            newX = 2 * x * y;
        }
        System.out.println("Fixed values" + newX + " & " + newY);
    }

    private static void task16() {

        System.out.println("\n\n16. На плоскости ХОY задана своими координатами точка А. Указать, где она расположена (на какой оси или в каком координатном угле).");
        int x = 0;
        int y = 10;

        if (x > 0 && y > 0) {
            System.out.println("Point (" + x + ", " + y + ") located in 1 quarter");

        } else if (x < 0 && y > 0) {
            System.out.println("Точка (" + x + ", " + y + ") located in 2 quarter");

        } else if (x < 0 && y < 0) {
            System.out.println("Точка (" + x + ", " + y + ") located in 3 quarter");

        } else if (x > 0 && y < 0) {
            System.out.println("Точка (" + x + ", " + y + ") located in 4 quarter");

        } else if (x == 0 && y != 0) {
            System.out.println("Точка (" + x + ", " + y + ") is on the y axis");

        } else if (x != 0 && y == 0) {
            System.out.println("Точка (" + x + ", " + y + ") is on the x axis");

        } else if (x == 0 && y == 0) {
            System.out.println("Точка (" + x + ", " + y + ") located at null");
        }
    }

    private static void task17() {

        System.out.println("\n\n17. Даны целые числа т, п. Если числа не равны, то заменить каждое из них одним и тем же числом, равным большему из исходных, а если равны, то заменить числа нулями.");
        int m = 3;
        int n = 3;

        if (m != n) {
            if (m > n) {
                n = m;
            } else {
                m = n;
            }
        } else {
            m = 0;
            n = 0;
        }

        System.out.println("New numbers: " + m + ", " + n);
    }

    private static void task18() {

        System.out.println("\n\n18. Подсчитать количество отрицательных среди чисел а, b, с.");
        int a = 3;
        int b = 6;
        int c = -3;
        int count = 0;

        if (a < 0) {
            count++;
        }
        if (b < 0) {
            count++;
        }
        if (c < 0) {
            count++;
        }
        System.out.println("The count of negative numbers is " + count);
    }

    private static void task19() {

        System.out.println("\n\n19. Подсчитать количество положительных среди чисел а, b, с.");
        int a = 3;
        int b = 6;
        int c = -3;
        int count = 0;

        if (a > 0) {
            count++;
        }
        if (b > 0) {
            count++;
        }
        if (c > 0) {
            count++;
        }
        System.out.println("The count of positive numbers is " + count);
    }

    private static void task20() {

        System.out.println("\n\n20. Определить, делителем каких чисел а, b, с является число k.");
        int a = 3;
        int b = 6;
        int c = -4;
        int k = 2;

        if (a % k == 0) {
            System.out.println("Number " + k + " is a divisor of number " + a);
        }
        if (b % k == 0) {
            System.out.println("Number " + k + " is a divisor of number " + b);
        }
        if (c % k == 0) {
            System.out.println("Number " + k + " is a divisor of number " + c);
        }
    }

    private static void task21() {

        System.out.println("\n\n21. Программа — льстец. На экране высвечивается вопрос «Кто ты: мальчик или девочка? Введи Д или М». В зависимости от ответа на экране должен появиться текст «Мне нравятся девочки!» или «Мне нравятся мальчики!».");
        Scanner scan = new Scanner(System.in);
        System.out.println("Кто ты: мальчик или девочка? Введи Д или М");
        char answer = scan.next().charAt(0);

        switch (answer) {
            case 'Д':
                System.out.println("Мне нравятся девочки!");
                break;
            case 'М':
                System.out.println("Мне нравятся мальчики!");
                break;
            default:
                System.out.println("Неверное значение!");
        }
    }

    private static void task22() {

        System.out.println("\n\n22.Перераспределить значения переменных х и у так, чтобы в х оказалось большее из этих значений, а в y - меньшее.");
        int x = 23;
        int y = 50;
        System.out.println("x = " + x + ", y = " + y);
        if (x < y) {
            int temp = x;
            x = y;
            y = temp;
            System.out.println("The x & y have been changed: x = " + x + ", y = " + y);
        }
    }

    private static void task23() {

        System.out.println("\n\n23. Определить правильность даты, введенной с клавиатуры (число — от 1 до 31, месяц — от 1 до 12). Если введены некорректные данные, то сообщить об этом.");
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter number of the month: ");
        int numberOfMonth = scan.nextInt();
        System.out.println("Enter month: ");
        int month = scan.nextInt();

        if (numberOfMonth >= 1 && numberOfMonth <= 31 && month >= 1 && month <= 12) {
            System.out.println("Today is " + numberOfMonth + "." + month);
        } else {
            System.out.println("Ошибка ввода данных!");
        }
    }

    private static void task24() {

        System.out.println("\n\n24. Составить программу, определяющую результат гадания на ромашке — «любит—не любит», взяв за исходное данное количество лепестков п.");
        int n = 7;
        System.out.println("n = " + n);

        if (n % 2 == 0) {
            System.out.println("Не любит!");
        } else {
            System.out.println("Любит!");
        }
    }

    private static void task25() {

        System.out.println("\n\n25. Написать программу — модель анализа пожарного датчика в помещении, которая выводит сообщение «Пожароопасная ситуация », если температура в комнате превысила 60° С.");
        double temperature = 65;

        if (temperature >= 60) {
            System.out.println("Пожароопасная ситуация! Температура " + temperature + "градусов!");
        }
    }

    private static void task26() {

        System.out.println("\n\n26. Написать программу нахождения суммы большего и меньшего из трех чисел.");
        int a = 9;
        int b = 12;
        int c = 9;
        int max = a;
        int min = a;

        if (b >= a && b >= c) {
            max = b;
        } else if (c >= a && c >= b) {
            max = c;
        }

        if (b <= a && b <= c) {
            min = b;
        } else if (c <= b && c <= a) {
            min = c;
        }
        System.out.println("max = " + max + "; min = " + min);
    }

    private static void task27() {

        System.out.println("\n\n27. Найти max{min(a, b), min(c, d)}.");
        int a = 3;
        int b = 4;
        int c = 2;
        int d = -9;
        int minAB = a;
        int minCD = c;
        int max = minAB;

        if (b < a) {
            minAB = b;
        }
        if (d < c) {
            minCD = d;
        }

        if (minCD > minAB) {
            max = minCD;
        }
        System.out.println("The minimum value between a & b is " + minAB + "\nThe minimum value between c & d is " + minCD);
        System.out.println("The maximum value between min(a, b) and min(c, d) is " + max);
    }

    private static void task28() {

        System.out.println("\n\n28. Даны три числа a, b, с. Определить, какое из них равно d. Если ни одно не равно d, то найти max(d — a, d — b, d —c).");
        final int a = -2;
        final int b = 5;
        final int c = 6;
        int constantD = 10;


        switch (constantD) {
            case a:
                System.out.println("a = " + a + " = d");
                break;
            case b:
                System.out.println("b = " + b + " = d");
                break;
            case c:
                System.out.println("c = " + c + " = d");
                break;
            default:
                int max = max(constantD - a, constantD - b);
                max = max(max, constantD - c);
                System.out.println("max = " + max);
                break;
        }

    }

    private static void task29() {

        System.out.println("\n\n29. Даны три точки А(х1,у1), В(х2,у2) и С(х3,у3). Определить, будут ли они расположены на одной прямой.");
        int x1 = 5;
        int x2 = 6;
        int x3 = 7;
        int y1 = 5;
        int y2 = 5;
        int y3 = 5;

        //для выполнения условия необходимо, что бы угловой коэф. и коэф. b для точек были одинаковы.
        double k1_2 = (y2 - y1) / (x2 - x1);
        double b1_2 = y1 - (y2 - y1) / (x2 - x1) * x1;
        double k1_3 = (y3 - y1) / (x3 - x1);
        double b1_3 = y1 - (y3 - y1) / (x3 - x1) * x1;

        if (k1_2 == k1_3 && b1_2 == b1_3) {
            System.out.println("Points A(" + x1 + ", " + y1 + "), B(" + x2 + ", " + y2 + "), C(" + x3 + ", " + y3 + ") located on one straight line");
        } else {
            System.out.println("Points A(\"+x1+\", \"+ y1 +\"), B(\"+x2+\", \"+y2+\"), C(\"+x3+\", \"+y3+\") do not belong to one straight line");
        }
    }

    private static void task30() {

        System.out.println("\n\n30. Даны действительные числа а,b,с. Удвоить эти числа, если а > b > с, и заменить их абсолютными значениями, если это не так.");
        int a = 4;
        int b = 5;
        int c = 6;

        if (a > b && b > c) {
            a *= 2;
            b *= 2;
            c *= 2;
            System.out.println("Numbers were doubled");
        } else {
            a = abs(a);
            b = abs(b);
            c = abs(c);
            System.out.println("Numbers were converted to an absolute value");
        }
    }

    private static void task32() {

        System.out.println("\n\n32. Написать программу, которая по заданным трем числам определяет, является ли сумма каких-либо двух из них положительной.");
        int a = 5;
        int b = -5;
        int c = 7;

        if (a + b > 0 || a + c > 0 || b + c > 0) {
            System.out.println("The sum of the two numbers is positive.");
        } else {
            System.out.println("The sum is negative");
        }
    }
}
