package by.treining.ksa.decomposition;

import java.util.ArrayList;
import java.util.List;

/*
19. Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры. Определить
        также, сколько четных цифр в найденной сумме. Для решения задачи использовать декомпозицию.
*/
public class Task_19 {

    public static void main(String[] args) {
        int n = 3;

        List<Integer> numbers = createNumbers(n);
        for (int num : numbers) {
            System.out.print(num + " ");
        }

        int sum = calculateSum(numbers);
        System.out.println("\n\nThe sum of numbers is " + sum);

        int amountOfEvenNum = findEvenNum(sum);
        System.out.println("\nThe amount of even digits in the sum is " + amountOfEvenNum);
    }

    private static int findEvenNum(int sum) {
        String temp = String.valueOf(sum);
        String[] tempDigits = temp.split("");

        int[] digits = new int[tempDigits.length];

        int countOfEven = 0;
        for (int i = 0; i < digits.length; i++) {
            digits[i] = Integer.parseInt(tempDigits[i]);

            if (digits[i] % 2 == 0) {
                countOfEven++;
            }
        }

        return countOfEven;
    }

    private static int calculateSum(List<Integer> numbers) {
        int sum = 0;

        for (int i : numbers) {
            sum += i;
        }
        return sum;
    }

    private static List<Integer> createNumbers(int n) {

        List<Integer> requiredNumbers = new ArrayList<>();

        String tempMinNum = "1";
        String tempMaxNum = "9";
        for (int i = 1; i < n; i++) {
            tempMinNum += 1;
            tempMaxNum += 9;
        }

        int firstNumber = Integer.parseInt(tempMinNum);
        int lastNumber = Integer.parseInt(tempMaxNum);

        int currentNum = firstNumber;
        while (currentNum <= lastNumber) {
            String tempNum = String.valueOf(currentNum);
            String[] tempDigits = tempNum.split("");
            int[] digits = new int[tempDigits.length];

            boolean areOddDigits = true;
            for (int i = 0; i < digits.length; i++) {
                digits[i] = Integer.parseInt(tempDigits[i]);
                if (digits[i] % 2 == 0) {
                    areOddDigits = false;
                    break;
                }
            }

            if (areOddDigits) {
                requiredNumbers.add(currentNum);
            }

            currentNum++;
        }

        return requiredNumbers;
    }
}
