package by.training.sadko.controller.command.impl;

import by.training.sadko.controller.command.AppCommand;

import java.util.Map;

import static by.training.sadko.AppConstants.COMMAND_NAME;
import static by.training.sadko.AppConstants.PATH;

public class ShowInputFieldCommand implements AppCommand {

    @Override
    public String execute(Map<String, String> requestMap) {
        return "<form action=\"/home\" method=\"POST\" align=\"center\" accept-charset=\"utf-8\">\n" +
                "<label for=" + PATH + ">Enter file path:" + "</label><br>\n" +
                "<input type=\"text\"  name=" + PATH + " value=\"\">\n" +
                "<input type=\"hidden\"  name=" + COMMAND_NAME + " value=\"add\">\n" +
                "<br>\n\n" +
                "<input type=\"submit\" value=\"Add\">\n" +
                "</form> ";
    }
}
