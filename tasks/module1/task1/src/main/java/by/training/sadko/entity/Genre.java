package by.training.sadko.entity;

public enum Genre {

    FANTASY("fantasy"),
    SCIENTIFIC("scientific"),
    POEMS("poems"),
    FOLKLORE("folklore"),
    FAIRY_TAILS("fairy tails"),
    BUSINESS("business"),
    NOVEL("novel"),
    COMPILATION("compilation"),
    PAINTING("painting"),
    PSYCHOLOGY("psychology"),
    HISTORY("history");

    private final String name;

    Genre(String name) {
        this.name = name;
    }

    public static Genre fromString(String userData) {
        Genre[] values = Genre.values();
        for (Genre tempGenre : values) {
            if (tempGenre.name.equals(userData) || tempGenre.name().equals(userData)) {
                return tempGenre;
            }
        }
        return null;
    }


    public String getName() {
        return name;
    }


}
