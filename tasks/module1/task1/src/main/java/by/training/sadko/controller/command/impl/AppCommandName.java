package by.training.sadko.controller.command.impl;

public enum AppCommandName {

    DEFAULT("default"),
    CALCULATE_PAGES_BY_GENRE("calc"),
    DELETE_BY_ID("delete"),
    ADD_LITERATURE("add"),
    SHOW_CALCULATOR("showCalc"),
    SHOW_INPUT_FIELD("showField"),
    SHOW_SORT_FIELD("showSort"),
    SORT_LITERATURE("sort");

    private final String shortCommand;

    AppCommandName(String shortCommand) {
        this.shortCommand = shortCommand;
    }

    public static AppCommandName fromString(String commandName) {
        final AppCommandName[] values = AppCommandName.values();
        for (AppCommandName tempName : values) {
            if (tempName.name().equals(commandName) || tempName.getShortCommand().equals(commandName)) {
                return tempName;
            }
        }
        return null;
    }

    public String getShortCommand() {
        return shortCommand;
    }
}
