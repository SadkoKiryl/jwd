package by.treining.ksa.oop.simple.task2;

/*
2. Создйте класс Test2 двумя переменными. Добавьте конструктор с входными параметрами. Добавьте конструктор,
    инициализирующий члены класса по умолчанию. Добавьте set- и get- методы для полей экземпляра класса.
*/
public class Test2 {
    int num1;
    int num2;

    public Test2(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public Test2() {
        this.num1 = 1;
        this.num2 = -1;
    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }
}
