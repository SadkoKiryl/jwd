package by.treining.ksa.oop.simple.task8;


import java.util.Arrays;
import java.util.Comparator;

/*
8. Создать класс Customer, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы
и метод toString().
Создать второй класс, агрегирующий массив типа Customer, с подходящими конструкторами и
методами. Задать критерии выбора данных и вывести эти данные на консоль.
*/
/*
Класс Customer: id, фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
        Найти и вывести:
        a) список покупателей в алфавитном порядке;
        b) список покупателей, у которых номер кредитной карточки находится в заданном интервале
*/
public class CustomerAccounting {
    private Customer[] customers;

    public Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public CustomerAccounting(Customer[] customers) {
        this.customers = customers;
    }

    public void sortByAlphabet() {

        Arrays.sort(this.customers, Comparator.comparing(Customer::getLastName).
                thenComparing(Customer::getLastName).
                thenComparing(Customer::getMiddleName));

        for (Customer customer : this.customers) {
            System.out.println(customer.toString());
        }
    }

    public void verificationCreditCard(int from, int to) {
        for (Customer customer : this.customers) {
            if (customer.getCreditCardNumber() > from &&
                    customer.getCreditCardNumber() < to) {

                System.out.println(customer.toString());
            }
        }
    }
}
