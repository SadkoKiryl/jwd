package controller;

import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.controller.command.AppCommandRegistry;
import by.training.sadko.controller.command.impl.*;
import by.training.sadko.dao.InMemoryLiteratureDao;
import by.training.sadko.dao.LiteratureDao;
import by.training.sadko.service.LiteratureService;
import by.training.sadko.service.SimpleLiteratureService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static by.training.sadko.controller.command.impl.AppCommandName.*;

public class SimpleAppCommandRegistryTest {

    AppCommandRegistry registry;

    @Before
    public void setUp() {
        LiteratureDao literatureDao = new InMemoryLiteratureDao();
        LiteratureService literatureService = new SimpleLiteratureService(literatureDao);
        AppCommand defaultCommand = new DefaultCommand(literatureService);

        Map<AppCommandName, AppCommand> commandsMap = new EnumMap<>(AppCommandName.class);
        commandsMap.put(DEFAULT, defaultCommand);
        registry = new SimpleAppCommandRegistry(commandsMap);
    }

    @Test
    public void registryShouldReturnCommand() {
        AppCommand defaultCommand = registry.getCommand(DEFAULT.getShortCommand());
        AppCommand nonExistentCommand = registry.getCommand("nonexistent command");
        AppCommand nullCommand = registry.getCommand(null);

        Assert.assertEquals(defaultCommand, nonExistentCommand);
        Assert.assertEquals(defaultCommand, nullCommand);
    }
}
