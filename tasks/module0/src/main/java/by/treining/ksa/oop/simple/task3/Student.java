package by.treining.ksa.oop.simple.task3;

/*
3. Создайте класс с именем Student, содержащий поля: фамилия и инициалы, номер группы, успеваемость
(массив из пяти элементов). Создайте массив из десяти элементов такого типа. Добавьте возможность вывода фамилий
и номеров групп студентов, имеющих оценки, равные только 9 или 10.
*/
public class Student {
    private String name;
    private int group;
    private int[] progress;

    public Student(String name, int group, int[] marks) {
        this.name = name;
        this.group = group;
        this.progress = marks;
    }

    public String getName() {
        return name;
    }

    public int getGroup() {
        return group;
    }

    public int[] getMarks() {
        return progress;
    }
}
