package by.treining.ksa.oop.basics.task1.service;

import by.treining.ksa.oop.basics.task1.model.Directory;
import by.treining.ksa.oop.basics.task1.model.File;
import by.treining.ksa.oop.basics.task1.model.TextFile;

import java.util.List;

public class SimpleTextFileService implements TextFileService {

    @Override
    public void printFileBody(Directory directory, String fileName) {
        for (File file : directory.getFileList()) {

            if (file instanceof TextFile) {
                TextFile textFile = (TextFile) file;

                if (textFile.getFileName().equals(fileName)) {

                    for (String part : textFile.getBody()) {
                        System.out.print(part);
                    }
                    System.out.println("The file's body printed");
                } else {
                    System.out.println("The file not found");
                }
            }
        }
    }

    @Override
    public void addFileBody(Directory directory, String fileName, List<String> body) {
        for (File file : directory.getFileList()) {
            if (file.getFileName().equals(fileName)) {

                for (String part : body) {
                    ((TextFile) file).getBody().add(part);
                }
                System.out.println("The file's body added");

            } else {
                System.out.println("The file not found");
            }
        }

    }

    @Override
    public void create(Directory directory, String fileName) {
        File file = new TextFile(fileName, directory);

        List<File> fileList = directory.getFileList();
        fileList.add(file);
        directory.setFileList(fileList);

        System.out.println("The file created");
    }

    @Override
    public void rename(Directory directory, String fileName, String newFileName) {
        for (File file : directory.getFileList()) {

            if (file.getFileName().equals(fileName)) {
                file.setFileName(newFileName);
                System.out.println("The file's name changed");
                break;
            } else {
                System.out.println("The file not found");
            }
        }
    }

    @Override
    public void dell(Directory directory, String fileName) {
        for (File file : directory.getFileList()) {

            if (file.getFileName().equals(fileName)) {
                directory.getFileList().remove(file);
                System.out.println("The file deleted");
                break;
            } else {
                System.out.println("The file not found");
            }
        }
    }
}
