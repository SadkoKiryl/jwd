package by.treining.ksa.calculator.procedural;

import java.util.ArrayList;
import java.util.Scanner;

public class Calculator {

    static String input;
    static double a;
    static double b;
    static String symbol;
    static ArrayList<String> log = new ArrayList<>();

    public static void main(String[] args) {

        mainMenu();

    }

    /*Вычисление*/
    private static void calculate() {

        double result = 0;

        if ("+".equals(symbol)) {
            result = a + b;
        }
        if ("-".equals(symbol)) {
            result = a - b;
        }
        if ("*".equals(symbol)) {
            result = a * b;
        }
        if ("/".equals(symbol)) {
            result = a / b;
        }

        String output = a + " " + symbol + " " + b + " = " + result;
        System.out.println("\n" + output + "\n");
        log.add(output);

        mainMenu();
    }

    /*Main Menu*/
    private static void mainMenu() {

        System.out.println("1. Calculator");
        System.out.println("2. History");
        System.out.print("Choose one:_");

        Scanner scan = new Scanner(System.in);
        String choose = scan.nextLine();

        if ("1".equals(choose)) {
            calc();

        } else if ("2".equals(choose)) {
            history();

        } else {
            System.out.println("Goodbye!");
        }
    }

    /*Вывод лога расчетов*/
    private static void history() {

        if (log.isEmpty()) {
            System.out.println("\nHistory is empty!\n");
        } else {
            System.out.println();
            for (String s : log) {
                System.out.println(s);
            }
            System.out.println();
        }

        mainMenu();
    }

    private static void calc() {

        inputData(); //Ввод данных
        initializeData(); //Инициализация полей и проверка введенных данных
        calculate(); //Вычисление
    }

    /*Ввод данных*/
    private static void inputData() {

        System.out.print("\nEnter your expression with a space: ");
        Scanner scan = new Scanner(System.in);
        input = scan.nextLine();
    }

    /*Инициализация параметров выражения и проверка на корректность введенного выражения*/
    private static void initializeData() {

        String[] elementsOfExp = input.split(" ");

        if (elementsOfExp.length != 3) {
            System.out.println("Wrong input format, try again");
            inputData();
        }

        try {
            a = Double.parseDouble(elementsOfExp[0]);
            symbol = elementsOfExp[1];
            b = Double.parseDouble(elementsOfExp[2]);
        } catch (NumberFormatException e) {
            System.out.println("Wrong input format, try again :)");
            inputData();
        }

        if ("/".equals(symbol) && b == 0) {
            System.out.println("Are you kidding me?!");
            inputData();
        }
    }


}
