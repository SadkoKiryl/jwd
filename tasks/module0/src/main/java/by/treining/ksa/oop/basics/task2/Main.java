package by.treining.ksa.oop.basics.task2;

public class Main {

    public static void main(String[] args) {

        Payment payment = new Payment();

        payment.addProduct(Product.BREAD, Product.ICE_CREAM, Product.FISH);

        payment.getBill();

        payment.pay();
    }
}
