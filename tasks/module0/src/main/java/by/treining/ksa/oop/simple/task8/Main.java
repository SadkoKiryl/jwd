package by.treining.ksa.oop.simple.task8;

/*
Класс Customer: id, фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
        Найти и вывести:
        a) список покупателей в алфавитном порядке;
        b) список покупателей, у которых номер кредитной карточки находится в заданном интервале
*/
public class Main {
    public static void main(String[] args) {


        Customer customer1 = new Customer(123, "Petrov", "Vasily", 2676);
        Customer customer2 = new Customer(476, "Kolobkov", "Ivan", 2434);
        Customer customer3 = new Customer(230, "Rockefeller", "John", 2212);
        Customer customer4 = new Customer(5589, "Kirieshkin", "Petya", 3355);
        Customer customer5 = new Customer(9999, "Kolbasnikov", "Valera", 9999);
        Customer customer6 = new Customer(8765, "Alekseev", "Sasha", 2456);

        Customer[] customers = {customer1, customer2, customer3, customer4, customer5, customer6};

        CustomerAccounting customerAccounting = new CustomerAccounting(customers);
        customerAccounting.sortByAlphabet();
        System.out.println("---------------------");
        customerAccounting.verificationCreditCard(1000, 5000);
    }
}
