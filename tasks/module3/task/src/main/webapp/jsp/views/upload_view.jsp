<%@ page import="by.training.sadko.command.CommandName" %>
<%@ page import="by.training.sadko.parser.ParserName" %>
<%@ page import="static by.training.sadko.AppConstants.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<aside class="ui-menu">
    <ul class="ui-menu">
        <p class="menu-label">
            <b>Upload file menu</b>
        </p>
        <form method="post" enctype="multipart/form-data">
            <div>
                <div class="file-name">
                    <label class="file-label">
                        <input type="hidden" value="<%=CommandName.UPLOAD_FILE_COMMAND.getStringName()%>"
                               name="<%=PARAM_COMMAND%>"/>
                        <input class="file-input btn-outline-dark" type="file" name="fileName"
                               enctype="multipart/form-data"/>
                    </label>
                </div>

                <div class="select form-group col-md-1">
                    <label for="<%=PARAM_PARSER%>>">PARSER</label>
                    <select class="form-control" name="<%=PARAM_PARSER%>">
                        <option selected>StAX</option>
                        <option value="<%=ParserName.SAX.getStringName()%>">SAX</option>
                        <%--<option value="<%=ParserName.STAX.getStringName()%>>">StAX</option>--%>
                        <option value="<%=ParserName.DOM.getStringName()%>">DOM</option>
                    </select>
                </div>
            </div>
            <button class="btn btn-sm btn-outline-dark">PARSE</button>
        </form>
    </ul>

    <div class="alert alert-info" role="alert">
        <c:if test="${not empty upload_result}">
            <p class="ui-state-error-text">
                <c:out value="${upload_result}"/>
            </p>
        </c:if>
    </div>

</aside>