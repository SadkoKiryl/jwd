import by.training.sadko.controller.SimpleHttpHandler;
import by.training.sadko.controller.command.AppCommand;
import by.training.sadko.controller.command.AppCommandRegistry;
import by.training.sadko.controller.command.impl.*;
import by.training.sadko.dao.InMemoryLiteratureDao;
import by.training.sadko.dao.LiteratureDao;
import by.training.sadko.entity.Literature;
import by.training.sadko.service.LiteratureService;
import by.training.sadko.service.SimpleLiteratureService;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static by.training.sadko.AppConstants.*;
import static by.training.sadko.controller.command.impl.AppCommandName.*;
import static by.training.sadko.dao.LiteratureComparator.PAGES_SORT;

public class ServerApplicationTest {
    private static LiteratureDao repository;
    private static HttpServer server;
    private static AppCommandRegistry commandRegistry;

    @BeforeClass
    public static void setUp() throws Exception {
        InetSocketAddress localhost = new InetSocketAddress(URL, PORT);
        server = HttpServer.create(localhost, 0);

        repository = new InMemoryLiteratureDao();
        LiteratureService literatureService = new SimpleLiteratureService(repository);

        AppCommand deleteLiteratureByIdCommand = new DeleteByIdCommand(literatureService);
        AppCommand addLiteratureCommand = new AddLiteratureCommand(literatureService);
        AppCommand sortCommand = new SortCommand(literatureService);

        Map<AppCommandName, AppCommand> commandsMap = new EnumMap<>(AppCommandName.class);
        commandsMap.put(DELETE_BY_ID, deleteLiteratureByIdCommand);
        commandsMap.put(ADD_LITERATURE, addLiteratureCommand);
        commandsMap.put(SORT_LITERATURE, sortCommand);
        commandRegistry = new SimpleAppCommandRegistry(commandsMap);

        HttpHandler httpHandler = new SimpleHttpHandler(commandRegistry);
        server.createContext("/home", httpHandler);
        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();
    }

    @AfterClass
    public static void stopServer() {
        server.stop(1);
    }

    @Test
    public void commandShouldDeleteLiterature() {
        int originalSize = repository.getAllLiterature().size();
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(COMMAND_NAME, DELETE_BY_ID.getShortCommand());
        requestMap.put(PARAM_ID, "1");

        AppCommand command = commandRegistry.getCommand(DELETE_BY_ID.getShortCommand());
        command.execute(requestMap);
        int currentSize = repository.getAllLiterature().size();

        Assert.assertEquals(originalSize - 1, currentSize);
    }

    @Test
    public void commandShouldAddThreeLiteratureFromFile() {
        String path = "src/test/resources/list.txt";
        int originalSize = repository.getAllLiterature().size();
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(COMMAND_NAME, ADD_LITERATURE.getShortCommand());
        requestMap.put(PATH, path);

        AppCommand command = commandRegistry.getCommand(ADD_LITERATURE.getShortCommand());
        command.execute(requestMap);
        int currentSize = repository.getAllLiterature().size();

        Assert.assertEquals(originalSize + 3, currentSize);
    }

    @Test
    public void commandShouldSortLiteratureByPages() {
        List<Literature> list = repository.getAllLiterature();

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(COMMAND_NAME, SORT_LITERATURE.getShortCommand());
        requestMap.put(SORTING_OPTIONS, PAGES_SORT.getName());

        AppCommand command = commandRegistry.getCommand(SORT_LITERATURE.getShortCommand());
        command.execute(requestMap);

        boolean isSorted = true;
        for (int i = 0; i < list.size() - 1; i++) {
            int previousPages = list.get(i).getNumberOfPages();
            int currentPages = list.get(i + 1).getNumberOfPages();
            if (previousPages > currentPages) {
                isSorted = false;
                break;
            }
        }

        Assert.assertTrue(isSorted);
    }
}
