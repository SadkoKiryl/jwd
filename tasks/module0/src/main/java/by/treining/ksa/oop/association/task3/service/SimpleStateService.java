package by.treining.ksa.oop.association.task3.service;

import by.treining.ksa.oop.association.task3.model.City;
import by.treining.ksa.oop.association.task3.model.State;

public class SimpleStateService implements StateService {

    @Override
    public void printingCapital(State state) {
        String capital = state.getCapital().getCityName();
        System.out.println("The state's capital is " + capital);
    }

    @Override
    public void printingRegionCount(State state) {
        int regionCount = state.getRegionCount();
        System.out.println("This state has " + regionCount + " regions.");
    }

    @Override
    public void printingStateArea(State state) {
        int stateArea = state.getStateArea();
        System.out.println("\nThe area of the state is " + stateArea + " square kilometers.");
    }

    @Override
    public void printingRegionCenterList(State state) {
        System.out.println("Region centers:");

        for (City regionCenter : state.getRegionCenterList()) {
            System.out.println(regionCenter.getCityName());
        }
    }
}
