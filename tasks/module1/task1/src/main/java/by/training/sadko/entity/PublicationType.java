package by.training.sadko.entity;

public enum PublicationType {

    BOOK("book", ContentType.CHAPTER),
    MAGAZINE("magazine", ContentType.ARTICLE),
    ALBUM("album", ContentType.COMPOSITION);

    private final String name;
    private final ContentType contentType;

    PublicationType(String name, ContentType contentType) {
        this.name = name;
        this.contentType = contentType;
    }

    public static PublicationType fromString(String publication) {
        PublicationType[] publicationTypes = PublicationType.values();
        for (PublicationType tempType : publicationTypes) {
            if (tempType.name().equalsIgnoreCase(publication) || tempType.name.equalsIgnoreCase(publication)) {
                return tempType;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public ContentType getContentType() {
        return contentType;
    }
}
