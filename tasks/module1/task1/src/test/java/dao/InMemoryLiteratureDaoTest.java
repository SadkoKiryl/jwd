package dao;

import by.training.sadko.dao.InMemoryLiteratureDao;
import by.training.sadko.dao.LiteratureDao;
import org.junit.Assert;
import org.junit.Test;

public class InMemoryLiteratureDaoTest {
    @Test
    public void repositoryShouldBeInitialized() {
        LiteratureDao repository = new InMemoryLiteratureDao();
        Assert.assertNotNull(repository.getAllLiterature());
    }
}
