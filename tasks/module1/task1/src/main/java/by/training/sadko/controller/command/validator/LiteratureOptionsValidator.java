package by.training.sadko.controller.command.validator;

import by.training.sadko.entity.Genre;
import by.training.sadko.entity.PublicationType;

import java.util.Arrays;

public class LiteratureOptionsValidator implements Validator<String[]> {

    @Override
    public boolean isValid(String[] literatureOptions) {
        return isValidLength(literatureOptions) &&
                optionsContainsGenre(literatureOptions) &&
                optionsContainsPublication(literatureOptions) &&
                isNumeric(literatureOptions);
    }

    private boolean isValidLength(String[] literatureOptions) {
        return literatureOptions.length == 5;
    }

    private boolean optionsContainsGenre(String[] literatureOptions) {
        Genre genreFromString = Genre.fromString(literatureOptions[0]);
        Genre[] genres = Genre.values();
        return Arrays.asList(genres).contains(genreFromString);
    }

    private boolean optionsContainsPublication(String[] literatureOptions) {
        PublicationType publicationFromString = PublicationType.fromString(literatureOptions[3]);
        PublicationType[] publications = PublicationType.values();
        return Arrays.asList(publications).contains(publicationFromString);
    }

    private boolean isNumeric(String[] literatureOptions) {
        try {
            Integer.parseInt(literatureOptions[2]);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
