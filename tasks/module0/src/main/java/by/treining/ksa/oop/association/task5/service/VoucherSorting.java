package by.treining.ksa.oop.association.task5.service;

import by.treining.ksa.oop.association.task5.entity.Voucher;

import java.util.List;

public interface VoucherSorting {
    List<Voucher> sortByCost(List<Voucher> voucherList);

    List<Voucher> sortByAmountOfDays(List<Voucher> voucherList);
}
