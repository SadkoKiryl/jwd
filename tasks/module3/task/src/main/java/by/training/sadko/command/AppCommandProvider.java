package by.training.sadko.command;

public interface AppCommandProvider {

    void register(CommandName commandName, AppCommand appCommand);

    void remove(CommandName commandName);

    AppCommand getCommand(String commandFromRequest);
}
