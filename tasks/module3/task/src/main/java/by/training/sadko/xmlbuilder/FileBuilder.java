package by.training.sadko.xmlbuilder;

import java.util.List;

public interface FileBuilder<T> {

    String build(List<T> entitiesList);
}
