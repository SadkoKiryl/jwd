package by.training.sadko.parser;

import by.training.sadko.builder.PublicationBuilder;
import by.training.sadko.entity.Publication;
import by.training.sadko.entity.PublicationType;
import by.training.sadko.parser.xml.dom.DOMPublicationHandler;
import by.training.sadko.parser.xml.dom.DomParser;
import by.training.sadko.parser.xml.sax.SAXParser;
import by.training.sadko.parser.xml.sax.SAXPublicationHandler;
import by.training.sadko.parser.xml.stax.StAXParser;
import by.training.sadko.parser.xml.stax.StAXPublicationHandler;

import java.util.Map;

public class PublicationParserFactory implements ParserFactory<Publication> {

    private final Map<PublicationType, PublicationBuilder> builderMap;

    public PublicationParserFactory(Map<PublicationType, PublicationBuilder> builderMap) {
        this.builderMap = builderMap;
    }

    @Override
    public FileParser<Publication> getParser(String inputName) {
        ParserName parserName = ParserName.fromString(inputName);
        switch (parserName) {
            case DOM:
                return new DomParser<>(new DOMPublicationHandler(builderMap));
            case SAX:
                return new SAXParser<>(new SAXPublicationHandler(builderMap));
            default:
                return new StAXParser<>(new StAXPublicationHandler(builderMap));
        }
    }
}
