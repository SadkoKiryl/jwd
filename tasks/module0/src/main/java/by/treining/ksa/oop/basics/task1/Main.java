package by.treining.ksa.oop.basics.task1;

import by.treining.ksa.oop.basics.task1.model.Directory;
import by.treining.ksa.oop.basics.task1.model.File;
import by.treining.ksa.oop.basics.task1.model.TextFile;
import by.treining.ksa.oop.basics.task1.service.SimpleTextFileService;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        SimpleTextFileService simpleTextFileService = new SimpleTextFileService();

        List<String> fileBody = new ArrayList<>();
        fileBody.add("Hello my new text file. ");
        fileBody.add("How are you? ");
        fileBody.add("What's your name? ");

        Directory directory = new Directory("\\My new Directory\\");

        simpleTextFileService.create(directory, "MyNewTextFile");
        simpleTextFileService.addFileBody(directory, "MyNewTextFile", fileBody);
        simpleTextFileService.printFileBody(directory, "MyNewTextFile");
        simpleTextFileService.rename(directory, "MyNewTextFile", "SimpleName");
        simpleTextFileService.dell(directory, "SimpleName");
    }
}
